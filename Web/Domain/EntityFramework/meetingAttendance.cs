//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domain.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class meetingAttendance
    {
        public int meetAttID { get; set; }
        public Nullable<int> meetID { get; set; }
        public Nullable<int> repID { get; set; }
    
        public virtual meeting meeting { get; set; }
        public virtual representative representative { get; set; }
    }
}
