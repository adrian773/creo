﻿namespace Creo.ViewModels.Organisation
{
	public class OrganisationListItem
	{
		public int id { get; set; }

		public string Name { get; set; }

		public int Type { get; set; }
		
	}
}