# Base information for loading into database

organisations = [
    {
        "orgName": "Napier City Council",
        "shortName": "NCC",
        "mapName": "Napier City",
        "mapID": "031"
    },
    {
        "orgName": "Hastings District Council",
        "shortName": "HDC",
        "mapName": "Hastings District",
        "mapID": "030"
    },
    {
        "orgName": "Central Hawke's Bay District Council",
        "shortName": "CHBDC",
        "mapName": "Central Hawke's Bay District",
        "mapID": "032"
    },
    {
        "orgName": "Hawke's Bay Regional Council",
        "shortName": "HBRC",
        "mapName": "Hawke's Bay Region",
        "mapID": "06"
    },
    {
        "orgName": "Wairoa District Council",
        "shortName": "WDC",
        "mapName": "Wairoa District",
        "mapID": "029"
    },
    {
        "orgName": "Hawke's Bay District Health Board",
        "shortName": "HBDHB",
        "mapName": "",
        "mapID": ""
    }
]

representatives = [
    {
        "orgShortName": "HDC",
        "fromDate": "",
        "toDate": "",
        "reps": [
            {
                "Surname": "Hazlehurst",
                "Forename": "Sandra",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/SandraHazlehurst-HDCweb.jpg"
            },
            {
                "Surname": "Kerr",
                "Forename": "Tania",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/TaniaKerr-HDCweb.jpg"
            },
            {
                "Surname": "Barber",
                "Forename": "Bayden",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/BaydenBarber-HDCweb.jpg"
            },
            {
                "Surname": "Dixon",
                "Forename": "Malcolm",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/MalcolmDixon-HDCweb.jpg"
            },
            {
                "Surname": "Harvey",
                "Forename": "Damon",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/DamonHarvey-HDCweb.jpg"
            },
            {
                "Surname": "Heaps",
                "Forename": "Rod",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/RodHeaps-HDCweb.jpg"
            },
            {
                "Surname": "Lawson",
                "Forename": "Eileen",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/EileenLawson-HDCweb.jpg"
            },
            {
                "Surname": "Lyons",
                "Forename": "George",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/GeorgeLyons-HDCweb.jpg"
            },
            {
                "Surname": "Nixon",
                "Forename": "Simon",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/SimonNixon-HDCweb.jpg"
            },
            {
                "Surname": "O'Keefe",
                "Forename": "Henare",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/HenareOkeefe-HDCweb.jpg"
            },
            {
                "Surname": "Poulain",
                "Forename": "Jacoby",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/JacobyPoulain-HDCweb.jpg"
            },
            {
                "Surname": "Redstone",
                "Forename": "Ann",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/AnnRedstone-HDCweb.jpg"
            },
            {
                "Surname": "Schollum",
                "Forename": "Wendy",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/WendySchollum-HDCweb.jpg"
            },
            {
                "Surname": "Travers",
                "Forename": "Geraldine",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/GeraldineTravers-HDCweb.jpg"
            },
            {
                "Surname": "Watkins",
                "Forename": "Kevin",
                "ImageUrl": "https://www.hastingsdc.govt.nz/assets/Contacts/KevinWatkins-HDCweb.jpg"
            }
        ]
    },
    {
        "orgShortName": "NCC",
        "fromDate": "",
        "toDate": "",
        "reps": [
            {
                "Surname": "White",
                "Forename": "Faye",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Faye-White-Oct-2016.jpg"
            },
            {
                "Surname": "Dalton",
                "Forename": "Bill",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Mayor-Bill-Dalton-Oct-2016-3.jpg"
            },
            {
                "Surname": "Price",
                "Forename": "Keith",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Keith-Price-Oct-2016-2.jpg"
            },
            {
                "Surname": "McGrath",
                "Forename": "Richard",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Richard-McGrath-Oct-2016-2.jpg"
            },
            {
                "Surname": "Jeffery",
                "Forename": "Tony",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Tony-Jefferies-Mar-2017-2.jpg"
            },
            {
                "Surname": "Boag",
                "Forename": "Maxine",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Maxine-Boag-Oct-2016-5.jpg"
            },
            {
                "Surname": "Brosnan",
                "Forename": "Annette",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Annette-Brosnan-Oct-2016-7.jpg"
            },
            {
                "Surname": "Taylor",
                "Forename": "Graeme",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Councillor-Graeme-Taylor-Oct-2016-7.jpg"
            },
            {
                "Surname": "Wise",
                "Forename": "Kirsten",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Kirsten-Wise-Mar-2017.jpg"
            },
            {
                "Surname": "Hague",
                "Forename": "Claire",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Claire-Hague-Oct-2016-6.jpg"
            },
            {
                "Surname": "Dallimore",
                "Forename": "Larry",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Larry-Dallimore-Oct-2016-8.jpg"
            },
            {
                "Surname": "Wright",
                "Forename": "Tania",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/Councillor-Tania-Wright-Oct-2016-12.jpg"
            },
            {
                "Surname": "Tapine",
                "Forename": "Api",
                "ImageUrl": "https://www.napier.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Councillor-Apiata-Tapine-Oct-2016.jpg"
            }
        ]
    },
    {
        "orgShortName": "HBRC",
        "fromDate": "",
        "toDate": "",
        "reps": [
            {
                "Surname": "Graham",
                "Forename": "Rex",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-A77P5437.jpg"
            },
            {
                "Surname": "Barker",
                "Forename": "Rick",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-A77P5416.jpg"
            },
            {
                "Surname": "Dick",
                "Forename": "Alan",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-Alan-Dick-cropped.jpg"
            },
            {
                "Surname": "Belford",
                "Forename": "Tom",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-A77P5413.jpg"
            },
            {
                "Surname": "Wilson",
                "Forename": "Fenton",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-A77P5404.jpg"
            },
            {
                "Surname": "Hewitt",
                "Forename": "Debbie",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-A77P5432.jpg"
            },
            {
                "Surname": "Beaven",
                "Forename": "Peter",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-A77P5409.jpg"
            },
            {
                "Surname": "Kirton",
                "Forename": "Neil",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-A77P5460.jpg"
            },
            {
                "Surname": "Bailey",
                "Forename": "Paul",
                "ImageUrl": "https://www.hbrc.govt.nz/assets/Uploads/Councillors/_resampled/FillWyI4MDAiLCI4MDAiXQ-Paul-Bailey-cropped.jpg"
            }
        ]
    },
    {
        "orgShortName": "HBDHB",
        "fromDate": "",
        "toDate": "",
        "reps": [
            {"Surname": "Atkinson", "Forename": "Kevin", "ImageUrl": ""},
            {"Surname": "Arnott", "Forename": "Barbara", "ImageUrl": ""},
            {"Surname": "Flood", "Forename": "Hine", "ImageUrl": ""},
            {"Surname": "Druzianic", "Forename": "Dan", "ImageUrl": ""},
            {"Surname": "Dunkerley", "Forename": "Peter", "ImageUrl": ""},
            {"Surname": "Apatu", "Forename": "Ana", "ImageUrl": ""},
            {"Surname": "Francis", "Forename": "Helen", "ImageUrl": ""},
            {"Surname": "Kirton", "Forename": "Diana", "ImageUrl": ""},
            {"Surname": "Poulain", "Forename": "Jacoby", "ImageUrl": ""},
            {"Surname": "Skipworth", "Forename": "Heather", "ImageUrl": ""},
            {"Surname": "Tomoana", "Forename": "Ngahiwi", "ImageUrl": ""}
        ]
    },
    {
        "orgShortName": "CHBDC",
        "fromDate": "",
        "toDate": "",
        "reps": [
            {
                "Surname": "Walker",
                "Forename": "Alex",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Mayor-Alex-Walker.jpg"
            },
            {
                "Surname": "Sharp",
                "Forename": "Ian",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Ian-Sharp.jpg"
            },
            {
                "Surname": "Aitken",
                "Forename": "Tim",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Tim-Aitken.jpg"
            },
            {
                "Surname": "Annand",
                "Forename": "Kelly",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Kelly-Annand-2.jpg"
            },
            {
                "Surname": "Burne-Field",
                "Forename": "Shelley",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Shelley-Burne-Field.jpg"
            },
            {
                "Surname": "Chote",
                "Forename": "Tim",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Tim-Chote.jpg"
            },
            {
                "Surname": "Minehan",
                "Forename": "Gerard",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Gerard-Minehan.jpg"
            },
            {
                "Surname": "Muggeridge",
                "Forename": "Brent",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Brent-Muggeridge.jpg"
            },
            {
                "Surname": "Tennent",
                "Forename": "David",
                "ImageUrl": "https://www.chbdc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-David-Tennent.jpg"
            }
        ]
    },
    {
        "orgShortName": "WDC",
        "fromDate": "",
        "toDate": "",
        "reps": [
            {
                "Surname": "Little",
                "Forename": "Craig",
                "ImageUrl": "https://www.wairoadc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Craig-Little.jpg"
            },
            {
                "Surname": "Eaglesome-Karekare",
                "Forename": "Denise",
                "ImageUrl": "https://www.wairoadc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Denise-Eaglesome-Karekare.jpg"
            },
            {
                "Surname": "Lambert",
                "Forename": "Charlie",
                "ImageUrl": "https://www.wairoadc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Charlie-Lambert.jpg"
            },
            {
                "Surname": "Bird",
                "Forename": "Michael",
                "ImageUrl": "https://www.wairoadc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Mike-Bird.jpg"
            },
            {
                "Surname": "Flood",
                "Forename": "Hine",
                "ImageUrl": "https://www.wairoadc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Hine-Flood.jpg"
            },
            {
                "Surname": "Johansen",
                "Forename": "Michael",
                "ImageUrl": "https://www.wairoadc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Min-Johansen.jpg"
            },
            {
                "Surname": "Harker",
                "Forename": "Jeremy",
                "ImageUrl": "https://www.wairoadc.govt.nz/assets/Contacts/_resampled/FillWyI4MDAiLCI4MDAiXQ-Jeremy-Harker.jpg"
            }
        ]
    },
]

meetingUrl = [
    {
        "org_short": "HDC",
        "meet_sched_url": "http://hastings.infocouncil.biz/"
    },
    {
        "org_short": "NCC",
        "meet_sched_url": "http://napier.infocouncil.biz/"
    },
    {
        "org_short": "CHBDC",
        "meet_sched_url": "http://centralhawkesbay.infocouncil.biz/"
    },
    {
        "org_short": "WDC",
        "meet_sched_url": "http://wairoa.infocouncil.biz/"
    },
    {
        "org_short": "HBRC",
        "meet_sched_url": "http://hawkesbay.infocouncil.biz/"
    }
]

meetingTypeScrapeHelp = [
    {
        "org_short": "HDC",
        "meet_type": "Council",
        "startWord": "present:",
        "endWord": "attendance:"
    },
    {
        "org_short": "HDC",
        "meet_type": "Council - Extraordinary",
        "startWord": "present:",
        "endWord": "attendance:"
    },
    {
        "org_short": "HDC",
        "meet_type": "Finance and Risk Committee",
        "startWord": "present:",
        "endWord": "attendance:"
    },
    {
        "org_short": "HDC",
        "meet_type": "Works and Services Committee",
        "startWord": "present:",
        "endWord": "attendance:"
    },
    {
        "org_short": "HDC",
        "meet_type": "Strategy Planning and Partnerships Committee",
        "startWord": "present:",
        "endWord": "also present:"
    },
    {
        "org_short": "NCC",
        "meet_type": "Council",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "NCC",
        "meet_type": "Extra Council",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "NCC",
        "meet_type": "Strategy and Infrastructure Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "NCC",
        "meet_type": "Finance Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "NCC",
        "meet_type": "Regulatory Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "NCC",
        "meet_type": "Community Services Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Council",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Extra Council",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "WDC",
        "meet_type": "Council",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "WDC",
        "meet_type": "Extra Council",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Regional Council",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Extra Regional Council",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Corporate and Strategic Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Environment and Services Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Regional Planning Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HDC",
        "meet_type": "Risk and Audit Subcommittee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HDC",
        "meet_type": "Tenders Subcommittee",
        "startWord": "present:",
        "endWord": "also present:"
    },
    {
        "org_short": "HDC",
        "meet_type": "Community Grants Subcommittee",
        "startWord": "present:",
        "endWord": "also present:"
    },
    {
        "org_short": "HDC",
        "meet_type": "Temporary Road Closures Subcommittee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "NCC",
        "meet_type": "Hearings Committee (For the consideration of Tenders)",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "NCC",
        "meet_type": "Hearings Committee (Dog Hearing)",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "NCC",
        "meet_type": "Audit and Risk Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Community Development Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Environment and Regulatory Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Finance and Planning Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Risk and Audit Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "WDC",
        "meet_type": "Economic Development Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "WDC",
        "meet_type": "Finance, Audit & Risk Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "WDC",
        "meet_type": "Maori Standing Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "WDC",
        "meet_type": "Extra Finance, Audit & Risk Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "WDC",
        "meet_type": "Infrastructure Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Regional Transport Committee",
        "startWord": "present",
        "endWord": "attendance"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Maori Committee",
        "startWord": "present",
        "endWord": "attendance"
    }
]

# Meeting types that all councillors are expected to attend
meetingRepAll = [
    {
        "org_short": "HDC",
        "meet_type": "Council"
    },
    {
        "org_short": "HDC",
        "meet_type": "Finance and Risk Committee"
    },
    {
        "org_short": "HDC",
        "meet_type": "Council Supplementary"
    },
    {
        "org_short": "HDC",
        "meet_type": "Council - Extraordinary"
    },
    {
        "org_short": "HDC",
        "meet_type": "Works and Services Committee"
    },
    {
        "org_short": "HDC",
        "meet_type": "Strategy Planning and Partnerships Committee"
    },
    {
        "org_short": "NCC",
        "meet_type": "Council"
    },
    {
        "org_short": "NCC",
        "meet_type": "Extra Council"
    },
    {
        "org_short": "NCC",
        "meet_type": "Strategy and Infrastructure Committee"
    },
    {
        "org_short": "NCC",
        "meet_type": "Finance Committee"
    },
    {
        "org_short": "NCC",
        "meet_type": "Regulatory Committee"
    },
    {
        "org_short": "NCC",
        "meet_type": "Community Services Committee"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Council"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Council Supplementary"
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Extra Council"
    },
    {
        "org_short": "WDC",
        "meet_type": "Council"
    },
    {
        "org_short": "WDC",
        "meet_type": "Extra Council Supplementary"
    },
    {
        "org_short": "WDC",
        "meet_type": "Council Supplementary"
    },
    {
        "org_short": "WDC",
        "meet_type": "Extra Council"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Regional Council"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Extra Regional Council"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Regional Council Supplementary"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Corporate and Strategic Committee"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Corporate and Strategic Committee Supplementary"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Environment and Services Committee"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Regional Planning Committee"
    },
    {
        "org_short": "HBRC",
        "meet_type": "Extra Regional Planning Committee"
    }
]

meetingRepExtra = [
    {
        "org_short": "HDC",
        "meet_type": "Risk and Audit Subcommittee",
        "representatives": [
            "Kerr", "Nixon", "Travers"
        ]
    },
    {
        "org_short": "HDC",
        "meet_type": "Tenders Subcommittee",
        "representatives": [
            "Travers", "Watkins", "Lawson", "Nixon", "Redstone", "Hazlehurst"
        ]
    },
    {
        "org_short": "HDC",
        "meet_type": "Community Grants Subcommittee",
        "representatives": [
            "Dixon", "Barber", "Lawson", "Schollum", "Harvey", "Watkins", "Hazlehurst"
        ]
    },
    {
        "org_short": "HDC",
        "meet_type": "Temporary Road Closures Subcommittee",
        "representatives": [
            "Nixon", "Watkins", "Hazlehurst"
        ]
    },
    {
        "org_short": "NCC",
        "meet_type": "Hearings Committee (For the consideration of Tenders)",
        "representatives": [
            "Jeffery", "Taylor", "Brosnan", "White", "Wise", "Wright"
        ]
    },
    {
        "org_short": "NCC",
        "meet_type": "Hearings Committee (Dog Hearing)",
        "representatives": [
            "Jeffery", "Taylor", "Brosnan", "White", "Wise", "Wright"
        ]
    },
    {
        "org_short": "NCC",
        "meet_type": "Audit and Risk Committee",
        "representatives": [
            "Wise", "Hague"
        ]
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Community Development Committee",
        "representatives": [
            "Walker", "Sharp", "Aitken", "Annand", "Burne-Field", "Minehan"
        ]
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Environment and Regulatory Committee",
        "representatives": [
            "Walker", "Sharp", "Chote", "Muggeridge", "Tennent"
        ]
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Finance and Planning Committee",
        "representatives": [
            "Walker", "Sharp", "Aitken", "Annand", "Burne-Field", "Chote", "Minehan", "Muggeridge", "Tennent"
        ]
    },
    {
        "org_short": "CHBDC",
        "meet_type": "Risk and Audit Committee",
        "representatives": [
            "Walker", "Aitken", "Minehan", "Muggeridge", "Tennent"
        ]
    },
    {
        "org_short": "WDC",
        "meet_type": "Economic Development Committee",
        "representatives": [
            "Little", "Eaglesome-Karekare", "Johansen"
        ]
    },
    {
        "org_short": "WDC",
        "meet_type": "Finance, Audit & Risk Committee",
        "representatives": [
            "Little", "Eaglesome-Karekare", "Harker", "Flood"
        ]
    },
    {
        "org_short": "WDC",
        "meet_type": "Maori Standing Committee",
        "representatives": [
            "Little", "Lambert", "Harker"
        ]
    },
    {
        "org_short": "WDC",
        "meet_type": "Extra Finance, Audit & Risk Committee",
        "representatives": [
            "Little", "Eaglesome-Karekare", "Harker", "Flood"
        ]
    },
    {
        "org_short": "WDC",
        "meet_type": "Infrastructure Committee",
        "representatives": [
            "Little", "Johansen", "Lambert"
        ]
    },
    {
        "org_short": "HBRC",
        "meet_type": "Regional Transport Committee",
        "representatives": [
            "Dick", "Wilson"
        ]
    },
    {
        "org_short": "HBRC",
        "meet_type": "Maori Committee",
        "representatives": [
            "Barker", "Graham", "Wilson"
        ]
    }
]
