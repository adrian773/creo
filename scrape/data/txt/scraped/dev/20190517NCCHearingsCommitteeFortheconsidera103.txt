Minutes of Hearings Committee (For the Consideration of Tenders) - 17
May 2019
Hearings Committee (For the
Consideration of Tenders)
Open Minutes
Meeting Date:
Friday
17 May 2019
Time:
1.30pm
– 1.32pm
Venue
Council
Chamber
Hawke's Bay Regional Council
159 Dalton Street
Napier
Present
Councillor
Jeffery (In the Chair), Councillors Wise and Wright
In
Attendance
Director
Infrastructure Services, Project Management Engineer
Administration
Governance
Team
Hearings Committee
(For the Consideration of Tenders)
- 17 May 2019 - Open
Minutes
Apologies
Nil
Conflicts of
interest
Nil
Public forum
Nil
Announcements by
the Chairperson
Nil
Announcements by
the management
Nil
Confirmation of minutes
Councillors Jeffery / Wise
That the
Minutes of the meeting held on 14 March 2019 were taken as a true and
accurate record of the meeting.
Carried
Hearings Committee (For the Consideration of Tenders) - 17 May
2019 - Open Minutes
PUBLIC EXCLUDED
ITEMS
Councillors Wright / Jeffery
That the public be excluded
from the following parts of the proceedings of this meeting, namely:
1.         Contract
1221 Park Island HBRU Site
Carried
The general subject of each
matter to be considered while the public was excluded, the reasons for passing
this resolution in relation to each matter, and the specific grounds under
Section 48(1) of the Local Government Official Information and Meetings Act
1987 for the passing of this resolution were as follows:
General subject of each
matter to be considered.
Reason for passing this
resolution in relation to each matter.
Ground(s) under section
48(1) to the passing of this resolution.
1.  Contract 1221 Park
Island HBRU Site
7(2)(h) Enable the local authority to carry out,
without prejudice or disadvantage, commercial activities
7(2)(i) Enable the local authority to carry on,
without prejudice or disadvantage, negotiations (including commercial and
industrial negotiations)
48(1)A That the public conduct
of the whole or the relevant part of the proceedings of the meeting would be
likely to result in the disclosure of information for which good reason for
withholding would exist:
(i) Where the local authority is named or specified in Schedule 1 of this
Act, under Section 6 or 7  (except 7(2)(f)(i)) of the Local Government Official
Information and Meetings Act 1987.
The meeting moved into committee
at 1.32pm
Approved and adopted as a true and accurate record of the
meeting.
Chairperson .............................................................................................................................
Date of approval ......................................................................................................................