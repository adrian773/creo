Minutes of Risk and Audit Subcommittee Meeting - 2 July 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Risk and Audit Subcommittee
Meeting
Date:
Tuesday, 2 July 2019
CG-14-25-00099                                                                         1
Minutes
of a Meeting of the Risk and Audit Subcommittee held on
Tuesday,
2 July 2019 at 1.00pm
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  1
4.         Chairman's
Strategic Risk and Audit Overview   2
5.         Risk
Assurance Review   2
6.         HDC
Draft Risk Appetite Statement 2
7.         Updated
Bow Tie Risk Analysis for Civil Defence Emergency Strategic Risk  3
8.         Treasury
Activity and Funding   3
9.         2018/19
Annual Report Update  3
10.       Internal Audit 3
11.       General Update
Report and Status of Actions  4
12.       Additional
Business Items  4
13.       Extraordinary
Business Items  4
14.       Recommendation to
Exclude the Public from Items 15, 16 and 17  4
CG-14-25-00099                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE Risk
and Audit Subcommittee
HELD
IN THE Council
Chamber, Ground Floor, Civic Administration Building, Lyndon Road East,
Hastings
ON
Tuesday, 2 July
2019 AT 1.00pm
Present:
Mr J
Nichols (Chair)
Mayor
Hazlehurst
Deputy
Mayor Kerr (Deputy Chair)
Councillors
Nixon and Travers
IN ATTENDANCE:
Chief Executive: (Mr N Bickle)
Chief Financial
Officer (Mr B Allan)
Manager,
Strategic Finance (Mr B Chamberlain)
Financial
Controller (Mr A Wilson)
Management
Accountant (Mr J Tieman)
Group
Manager: Asset Management (Mr C Thew)
Group
Manager: Human Resources (Mrs B Bayliss)
Health
and Safety Manager (Ms J Kuzman)
Risk
and Corporate Services Manager (Mr R Smith)
Risk
Assurance Advisor (Mr D Ferguson)
District
Customer Services Manager (Mr G Brittin)
Building
Consents Project Officer (Mr G van Veen)
Group Manager: Planning & Regulatory Services
(Mr J O’Shaughnessy)
Community Facilities Manager (Ms W Beeke)
Project Manager – Corporate
Security (Mr J Doyle)
Manager:
Democracy and Governance Services (Mrs J Evans)
Committee
Secretary (Mrs C Hilton)
Project Manager
– Corporate Security, Mr J Doyle, introduced himself to the Risk and
Audit Subcommittee and outlined his role.
1.         Apologies
There were no apologies.
2.         Conflicts
of Interest
There were no
declarations of conflicts of interest.
3.         Confirmation
of Minutes
Councillor Travers/Councillor Nixon
That the
minutes of the Risk and Audit Subcommittee Meeting held Monday 6
May 2019 be confirmed as a true and correct record and be adopted.
CARRIED
4.
Chairman's Strategic Risk and Audit Overview
(Document 19/622)
The Chair, Mr J Nichols, spoke to a
power point slide “The 1 slide Strategic Discussion” (CG-14-25-00101).
Mr Nichols/Councillor Kerr
A)   That
the report of the Chief Financial Officer titled “Chairman's
Strategic Risk and Audit Overview” dated 2/07/2019
be received.
CARRIED
5.
Risk Assurance Review
(Document 19/412)
Councillor Kerr/Mayor Hazlehurst
A)    That
the report of the Risk and Corporate Services Manager titled “Risk
Assurance Review” dated 2/07/2019 be received.
B)    That
a follow up summary report be presented at the next Risk and Audit
Subcommittee meeting showing recommendations and actions in regard to risk.
CARRIED
6.
HDC Draft Risk Appetite Statement
(Document 19/614)
The Risk and Corporate Services
Manager spoke to a power point presentation “HDC Risk Appetite
– Our Service Advantage” (PRJ17-110-0089).
Councillor Nixon/Councillor Kerr
A)        That
the report of the Risk and Corporate Services Manager titled “HDC
Draft Risk Appetite Statement” dated 2/07/2019 be
received.
B)        That the Draft Risk Appetite Statement be modified and discussed
at a council workshop before being referred to the Council for adoption.
C)        That
the modified Draft Risk Appetite Statement be circulated to the Risk and
Audit Subcommittee members before a council workshop is held.
CARRIED
7.
Updated Bow Tie Risk Analysis for Civil Defence
Emergency Strategic Risk
(Document 19/534)
Councillor Travers/Mr Nichols
A)        That
the report of the Risk and Corporate Services Manager titled “Updated
Bow Tie Risk Analysis for Civil Defence Emergency Strategic Risk”
dated 2/07/2019 be received.
B)        That the revised risk analysis for Civil Defence Emergency is
reported to Council.
With the
reasons for this decision being that the objective of the decision will
contribute to enabling democratic local
decision-making and action by (and on behalf of) communities, and to promote
the social, economic, environmental, and cultural well-being of communities
in the present and for the future by:
i)          Providing
comprehensive understanding of strategic risk.
CARRIED
8.
Treasury Activity and Funding
(Document
19/568)
Councillor Nixon/Councillor
Kerr
That the report of the Manager Strategic Finance titled Treasury Activity and Funding
dated 2/07/2019 be
received.
CARRIED
9.
2018/19 Annual Report Update
(Document
19/593)
Mayor Hazlehurst/Councillor Travers
A)        That
the report of the Chief Financial Officer titled “2018/19
Annual Report Update” dated 2/07/2019 be received.
CARRIED
10.
Internal Audit
(Document 19/610)
Mayor Hazlehurst/Councillor Travers
A)        That
the report of the Chief Financial Officer titled “Internal
Audit” dated 2/07/2019 be received.
B)        That the 2019/20 Draft Internal Audit Plan from Crowe Horwath be
received.
CARRIED
11.
General Update Report and Status of Actions
(Document 19/567)
Mr Nichols/Councillor Nixon
A)      That the report of the Manager
Strategic Finance titled “General Update Report and Status of Actions” dated 2/07/2019 be received.
CARRIED
12.       Additional Business Items
There were no additional business items.
13.       Extraordinary
Business Items
There were no extraordinary business items.
14.
Recommendation to
Exclude the Public from Items 15, 16 and 17
SECTION 48,
LOCAL GOVERNMENT OFFICIAL INFORMATION AND MEETINGS ACT 1987
Councillor Nixon/Mayor Hazlehurst
THAT the public now be
excluded from the following parts of the meeting, namely;
15        Building
Control Liability Risk Update
16        Update
on Delegated Financial Authorities review and implementation
17        2019/20
Insurance Renewal Programme
The general subject of the matter to be considered
while the public is excluded, the reason for passing this Resolution in
relation to the matter and the specific grounds under Section 48 (1) of the
Local Government Official Information and Meetings Act 1987 for the passing
of this Resolution is as follows:
GENERAL SUBJECT OF EACH MATTER TO BE CONSIDERED
REASON FOR PASSING THIS RESOLUTION IN
RELATION TO EACH MATTER, AND PARTICULAR INTERESTS PROTECTED
GROUND(S) UNDER SECTION 48(1) FOR THE PASSING OF
EACH RESOLUTION
15    Building Control
Liability Risk Update
Section
7 (2) (g)
The
withholding of the information is necessary to maintain legal professional
privilege.
Legal
advice received is legally privileged.
Section
48(1)(a)(i)
Where the
Local Authority is named or specified in the First Schedule to this Act
under Section 6 or 7 (except Section 7(2)(f)(i)) of this Act.
16    Update on Delegated
Financial Authorities review and implementation
Section
7 (2) (a)
The
withholding of the information is necessary to protect the privacy of
natural persons, including that of a deceased person.
To protect
named persons.
Section
48(1)(a)(i)
Where the
Local Authority is named or specified in the First Schedule to this Act
under Section 6 or 7 (except Section 7(2)(f)(i)) of this Act.
17    2019/20 Insurance
Renewal Programme
Section
7 (2) (h)
The
withholding of the information is necessary to enable the local authority
to carry out, without prejudice or disadvantage, commercial activities.
Commercial
Sensitivity.
Section
48(1)(a)(i)
Where the
Local Authority is named or specified in the First Schedule to this Act
under Section 6 or 7 (except Section 7(2)(f)(i)) of this Act.
CARRIED
________________________
The meeting closed at 3.44pm
Confirmed:
Chairman:
Date: