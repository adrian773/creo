Minutes of Risk and Audit Subcommittee Meeting - 6 May 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Risk and Audit Subcommittee
Meeting
Date:
Monday, 6 May 2019
CG-14-25-00084                                                                         1
Minutes
of a Meeting of the Risk and Audit Subcommittee held on
Monday,
6 May 2019 at 10.00am
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  1
4.         Treasury
Activity and Funding   2
5.         Health
and Safety Risk Management Update  2
6.         Cape
Kidnappers Interim Operations Manual 2
7.         General
Update Report and Status of Actions  2
9.         Internal
Audit Report 3
8.         MBIE
Review of Tauranga City Council - Bella Vista Development 3
10.       Additional Business
Items  3
11.       Extraordinary
Business Items  3
CG-14-25-00084                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE Risk
and Audit Subcommittee
HELD
IN THE Landmarks
Room, Ground Floor, Civic Administration Building, Lyndon Road East, Hastings ON
Monday, 6 May 2019 AT 10.00am
Present:
Mr J
Nichols (Chair)
Mayor
Hazlehurst
Deputy
Mayor Kerr (Deputy Chair)
Councillors
Nixon and Travers
IN ATTENDANCE:
Manager,
Strategic Finance (Mr B Chamberlain)
Financial
Controller (Mr A Wilson)
Group Manager: Economic Growth and Organisation Improvement (Mr C
Cameron)
Group Manager: Regulatory Services (Mr J O’Shaughnessy)
Group
Manager: Human Resources (Mrs B Bayliss)
Health
and Safety Co-ordinator (Mr P McClusky)
Risk
and Corporate Services Manager (Mr R Smith)
Risk
Assurance Advisor (Mr D Ferguson)
District
Customer Services Manager (Mr G Brittin)
Project
Manager (Mr D Bishop)
Building
Consents Manager (Mr M Hart)
Procurement
Specialist (Mrs A Hirst)
Committee
Secretary (Mrs C Hilton)
ALSO
PRESENT:              Mr S Lucy, Audit New Zealand – (present for Item 7)
Ms J Noiseux – (present for Item 7)
Mr N Speir, Partner, Rice Speir – (present for Item 8)
Ms L Bielby, Rice Speir – (present for Item 8)
1.         Apologies
There were no apologies
from members of the Risk and Audit Subcommittee.
2.         Conflicts
of Interest
There were no
declarations of conflicts of interest.
3.         Confirmation
of Minutes
Councillor Travers/Mr Nichols
That the
minutes of the Risk and Audit Subcommittee Meeting held Monday 18
February 2019 be confirmed as a true and correct record and be adopted.
CARRIED
4.
Treasury Activity and Funding
(Document 19/367)
Councillor Nixon/Mayor
Hazlehurst
A)        That
the report of the Manager Strategic Finance
titled “Treasury Activity and Funding” dated 6/05/2019 be received.
CARRIED
5.
Health and Safety Risk Management Update
(Document 19/373)
Mayor Hazlehurst withdrew from the meeting at 10.40am.
Councillor Kerr/Councillor Travers
A)        That
the report of the Health and Safety Manager titled “Health
and Safety Risk Management Update” dated 6/05/2019
be received.
CARRIED
6.
Cape Kidnappers Interim Operations Manual
(Document 19/378) (Attachment
circulated in separate agenda
document)
Councillor Kerr/Mr Nichols
A)        That
the report of the Group Manager: Asset Management titled “Cape
Kidnappers Interim Operations Manual” dated 6/05/2019
be received.
B)        That the Risk and Audit Subcommittee endorse the interim
operations manual for the management of the beach road from Clifton to Cape
Kidnappers.
CARRIED
7.
General Update Report and Status of Actions
(Document 19/409)
Mr S Lucy, Audit New Zealand, spoke to
the Audit Plan report attachment for the year ending 30 June 2019.
Councillor Kerr/Councillor Travers
A)     That the report of the Manager
Strategic Finance titled “General Update Report and Status of Actions” dated 6/05/2019 be received.
B)     That the Subcommittee note that the Delegated Financial Policy and
the associated Delegated Financial Authorities have been updated and adopted
by the Chief Executive and the Lead Team.
C)     That
the Delegated Financial Policy be reviewed by the Risk and Audit Subcommittee
prior to it being forwarded to Council for ratification.
CARRIED
With the agreement
of the Risk and Audit Subcommittee members, Item 9 was taken out of order and
addressed at this point.
9.
Internal Audit Report
(Document 19/438)  (Attachment for this item was circulated
separately prior to meeting)
Councillor Travers/Mr Nichols
A)     That
the report of the Financial Controller titled “Internal
Audit Report” dated 6/05/2019 be received.
CARRIED
8.
MBIE Review of Tauranga City Council - Bella Vista
Development
(Document 19/424) (Attachment
circulated in separate agenda
document)
Mr N Speir, Partner, Rice Speir gave a
power point presentation (CG-14-25-00086) in regard to the report on
the Bella Vista development that had been circulated as an attachment to the
agenda.  He responded to questions from the Risk and Audit
Subcommittee.
Mayor Hazlehurst rejoined the meeting
at 12.15pm.
Mr Nichols/Councillor Travers
A)     That
the report of the Group Manager: Planning & Regulatory titled “MBIE
Review of Tauranga City Council - Bella Vista Development” dated
6/05/2019 be received.
CARRIED
10.       Additional Business Items
There were no additional business items.
11.       Extraordinary
Business Items
There were no extraordinary business items.
The meeting closed at 12.25pm
Confirmed:
Chairman:
Date: