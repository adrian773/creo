Minutes of Māori Consultative Committee - 21 May 2019
Māori Consultative Committee
Open Minutes
Meeting Date:
Tuesday
21 May 2019
Time:
3.00pm
– 3.22pm
Venue
Council
Chamber
Hawke’s Bay Regional Council
159 Dalton Street
Napier
Present
Piri
Prentice (In the Chair), Acting Mayor Faye White, Councillor Apiata Tapine, and
Tiwana Aranui
In
Attendance
Chief
Executive, Director Community Services, Manager Communications and Marketing
Administration
Governance
Team
Māori
Consultative Committee - 21 May 2019 -
Open Minutes
Karakia
Cr Tapine opened the meeting with a karakia. The Chair also
spoke of those recently passed, and sending continuing best wishes to Mayor
Dalton for a speedy recovery and to Te Whetu in Wellington.
Apologies
The  apologies from Te Whetu Henare-Winitana and
Charles Ropitini were accepted by the meeting.
Conflicts of
interest
Nil
Public forum
Nil
Announcements by
the Chairperson
The Iwi Leaders Forum is to be held in Ahuriri in August
this year which is an honour for the city. It is anticipated that the Acting
Mayor and CE will be invited.
The blessing of the Napier Girls High School carvings is to
take place on 21 July 2019.
Announcements by
the management
The Provincial Growth Fund announcement for Hawke’s
Bay will take place on 10 June 2019 – it is anticipated that the funding
package may encompass approximately $80M of investment.
The Napier-Wairoa railway line is due to open on 14 June.
Confirmation of minutes
Councillor Tapine / T Aranui
That the
Minutes of the meeting held on 9 April 2019 were taken as a true and accurate
record of the meeting.
Carried
Reports from
standing committees
Councillor Tapine / T Aranui
That the
Māori Consultative Recommendations arising from the discussion of the
Committee reports be submitted to the Council meeting for consideration.
Carried
Reports from
Regulatory Committee held 30 April 2019
1.    Earthquake-Prone
Buildings - Identification of Priority Buildings - Consultation
Type of Report:
Legal
Legal Reference:
Building Act
2004
Document ID:
726241
Reporting Officer/s &
Unit:
Malcolm Smith, Manager
Building Consents
1.1   Purpose
of Report
To advise Council of the
requirements under the Building Act 2004 in relation to the identification of
priority buildings under the earthquake-prone building legislation, and to seek
approval to release the draft Statement of Proposal for public submissions
prior to adoption by Council.
At the Māori Consultative Committee meeting
It was advised that
there is a legislative requirement to have a Policy in place, and Council is
consulting on the Statement of Proposal for this Policy. There are
implication for building owners, as once a building has been identified as
earthquake prone they have seven years within which to strengthen or demolish
it.
The criteria for the buildings considered ‘high
priority’ are outlined in the Statement of Proposal (refer p9 of the
agenda). Maraenui is not currently considered a high risk area as there are
not believed to be high numbers of unreinforced masonry buildings. However the
public are able to submit if they believe that there are other areas that
should be included as higher risk/ priority.
Māori
Consultative Committee's recommendation
Acting
Mayor White/ T Aranui
That the Council resolve that the Committee’s recommendation
be adopted.
Carried
Officer’s Recommendation
The Regulatory Committee:
a.     Approve
the release of the draft Statement of Proposal for public submissions.
2.    Street
Naming - 250 Guppy Road Taradale Napier
Type of Report:
Procedural
Legal Reference:
N/A
Document ID:
723801
Reporting Officer/s &
Unit:
Paul O'Shaughnessy, Team
Leader Resource Consents
2.1   Purpose
of Report
The purpose of this report is to
obtain Councils approval for one new street name to replace a previously approved
street name within the recently approved residential subdivisions at 250 Guppy
Road. The street in question has already been subject to a previous street name
approval by Council (Chue Court), however a mistake by the developer has led to
a request for a re-naming to Gee Place.
At the Māori Consultative Committee meeting DECISION OF COUNCIL
It was noted that this
was a Decision of Council at the Committee meeting as there was a tight
timeline with regards to the issuing of titles; the name change merely
reflected the actual preferred and regularly used surname of the family who
the street name recognises.
Māori
Consultative Committee's recommendation
Acting
Mayor White / T Aranui
That the Council resolve that the Committee’s
recommendation be adopted.
Carried
Council Resolution
Councillors Brosnan / Hague
That Council:
a.     Approve
one new street name at 250 Guppy Road as follows:
·      Gee
Place-250 Guppy Road
Carried
Reports from
Finance Committee held 14 May 2019
1.    Financial
Forecast to 30 June 2019
Type of Report:
Legal and
Operational
Legal Reference:
Local
Government Act 2002
Document ID:
731958
Reporting Officer/s &
Unit:
Caroline Thomson, Chief
Financial Officer
1.1   Purpose
of Report
To report to Council the
financial forecast to the 30 June 2019 for the whole of Council.
At the Māori Consultative Committee meeting
The Chief Executive
provided an overview of the report, noting that the main impacts on the
forecast have come from timing in relation to the Parklands development;
sales were affected by a significant District Plan change and there have been
regulatory settlements paid from the fund.
In response to a separate question it was advised that
the water modelling results are expected later this calendar year.
Māori
Consultative Committee's recommendation
Councillor
Tapine / Acting Mayor White
That the Council resolve that the Committee’s
recommendation be adopted.
Carried
Committee's recommendation
Councillors Wright / Price
That Council:
a.     Receive
the financial forecast to 30 June 2019.
b.     Approve
the 2018/19 financial forecast to 30 June 2019 including the proposed carry
forward commitments to 2019/20.
Carried
2.    Policies
- CCTV and Disposal of Surplus Assets
Type of Report:
Operational
Legal Reference:
N/A
Document ID:
727324
Reporting Officer/s &
Unit:
Duncan Barr, Manager
Information Services
Bryan Faulknor, Manager
Property
2.1   Purpose of Report
To present the CCTV and Disposal
of Surplus Assets Policy to Council to be approved.
At the Māori Consultative Committee meeting
There was no discussion
on this item.
Māori
Consultative Committee's recommendation
Councillor
Tapine / Acting Mayor White
That the Council resolve that the Committee’s
recommendation be adopted.
Carried
Committee's recommendation
Councillors Price / Dallimore
That Council:
a.     Approve
the updates made to the CCTV Policy for finalisation and publication.
b.     Approve
the updates made to the Disposal of Surplus Assets Policy for finalisation
and publication.
Carried
Reports from Community Services Committee held 14 May
2019
1.    Iron Maori
Event - Hardinge Road and Marine Parade Proposed Road Closures
Type of Report:
Operational
Legal Reference:
N/A
Document ID:
730705
Reporting Officer/s &
Unit:
Sera Chambers, Transportation
Team Administrator
Kevin Murphy, Event Manager
1.1   Purpose
of Report
To seek approval for the proposed
course change and proposed road closures for the Iron Māori event to be
held in 2019 and future years.
At the Māori Consultative Committee meeting DECISION OF COUNCIL
It was noted that the
event route for Iron Māori has been similar for a number of years, and
the event organisers are refreshing the course, both as a follow up to
learnings from the 2018 and to provide competitors with a revitalised
experience. It is expected that there will be approximately 2000 competitors
across the four events of Iron Māori, accompanied by whanau and support
crews. The event contributes approximately $900k in economic benefit to
Napier and the region. As well as directly quantifiable economic benefits
there are significant social and wellbeing implications from the fitness
participation and social cohesion which contribute to the positive impact of
the event.
Māori
Consultative Committee's recommendation
T
Aranui / Councillor Tapine
That the Council resolve that the Committee’s recommendation
be adopted.
Carried
Council Resolution
Councillors Price / Boag
That
Council:
a.     Note the
proposed course change and road closures for the Iron Māori event and
other events.
b.     Approve
the road closures noting that the event traffic management plan may be
modified to account for feedback obtained through the targeted consultation
by the event management.
Carried
2.    Creative
Communities Funding March 2019
Type of Report:
Operational
Legal Reference:
N/A
Document ID:
728363
Reporting Officer/s &
Unit:
Belinda McLeod, Community
Funding Advisor
2.1   Purpose
of Report
To note the Creative Communities
funding decisions made on 28 March 2019. Council administers the scheme on
behalf of Creative NZ. Funding decisions do not require ratification from
Council.
At the Māori Consultative Committee meeting
It was noted this
funding is a fixed grant provided by central government.
Māori
Consultative Committee's recommendation
T
Aranui / Councillor Tapine
That the Council resolve that
the Committee’s recommendation be adopted.
Carried
Committee's recommendation
Councillors Wright / Boag
That Council:
a.     Note
the Creative Communities funding decisions made on 28 March 2019.
Carried
Karakia
Tiwana spoke briefly of those recently passed, and the
encouragement that tragic events like the recent fatal car collision brings to
draw together as a community. He touched briefly on Pukemokimoki marae before
drawing the meeting to a close with a karakia.
The meeting closed at 3.22pm
Approved and adopted as a true and accurate record of the
meeting.
Chairperson .............................................................................................................................
Date of approval ......................................................................................................................