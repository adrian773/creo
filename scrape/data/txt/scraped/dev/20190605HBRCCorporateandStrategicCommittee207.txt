Minutes of Corporate and Strategic Committee - 5 June 2019
Unconfirmed
MINUTES OF A
meeting of the Corporate and Strategic Committee
Date:                          Wednesday 5 June 2019
Time:                          9.00am
Venue:
Council Chamber
Hawke's Bay Regional Council
159 Dalton Street
NAPIER
Present:                     N Kirton - Chairman
P Bailey
R Barker
P Beaven
T Belford
A J Dick
R Graham
D Hewitt
M Paku
T Waaka
F Wilson
In Attendance:          J Palmer – Chief Executive
J Ellerm –
Group Manager Corporate Services
P Munro – Te
Pou Whakarae, Māori Partnerships
A Roets – Governance
Administration Assistant
L Hooper –
Principal Advisor Governance
J Lawrence –
Group Manager Office of the CE & Chair
T Skerman –
Group Manager Strategic Planning
C Dolley –
Group Manager Asset Management
M Sharpe – Project
Manager
T Kilkolly –
Principal Accountant Rates and Revenue
1.       Welcome/Apologies/Notices
Pieri Munro, Te
Pou Whakarae, opened the meeting with a karakia.
The Chair welcomed everyone to the meeting and noted the agenda
items would be taken out of order.
Councillor Alan Dick and Mr Mike Paku advised that they are
unavailable this afternoon.
Resolution
C&S11/19     That
the apologies from Councillor Fenton Wilson for lateness and Councillor Rex Graham
for early departure at 1.30pm be received.
Barker/Bailey
CARRIED
2.       Conflict
of Interest Declarations
As previously noted Cr Hewitt indicated that she is a trustee of the
Cranford Foundation Trust.
Cr Rex Graham declared an interest in item 8 – HB Tourism Funding Review and Organisational Updates as
he is investigating entering the ‘bed and breakfast’ business.
Mr
Toro Waaka declared an interest in item 8 – HB Tourism Funding Review and Organisational Updates as
Chair of Hawke’s Bay Māori Tourism.
3.       Confirmation
of Minutes of the Corporate and Strategic Committee meeting held on 6 March
2019
C&S12/19
Resolution
Minutes of the Corporate and Strategic Committee meeting
held on Wednesday, 6 March 2019, a copy having been circulated prior to
the meeting, were taken as read and confirmed as a true and correct record.
Waaka/Bailey
CARRIED
4.
Follow-ups from Previous Corporate and
Strategic Committee Meetings
James Palmer
introduced the item, advising that all bar one of the items is the subject of
an agenda item today.
In relation to
an ethical investment workshop, it was suggested that a session provided by
the fund managers can be organised in late July.
In relation to
Maori Constituencies, the advice that the next opportunity for Council to
discuss the establishment of one or more will be late 2020 was reiterated.
Cr Fenton Wilson arrived at 9.18am
C&S13/19
Resolution
That the Corporate
and Strategic Committee receives and notes the “Follow-ups from
Previous Corporate and Strategic Committee Meetings” report.
Beaven/Paku
CARRIED
5.
Call for Minor Items of Business not on
the Agenda
There were no items
raised.
8.
HB Tourism Funding Review and Organisational Updates
Tom Skerman introduced the item, highlighting:
·
Ongoing issues for funding of tourism, generally
·
Emergence of Air BnB sector
·
Introduce Hamish Saxon, Acting General Manager
of HB Tourism
·
HB Tourism board is seeking Council agreement
to Key Performance Indicators under the new funding arrangement
·  Letter
advising commercial ratepayers of changes to the economic development rate
will be distributed
Hamish Saxon spoke to the organisational update,
highlighting:
·
Applications for GM have now closed and will
be shortlisting and interviewing over next 6 weeks or so
·
Balancing tourism, how much is too much and
how to determine where the local community would like to see tourism growth
·
Paid membership does not reflect all of industry
support, as there is significant ‘in kind’ support through
participation in trade shows and providing meals, accommodation, etc for
trade delegations
·
visitor levy and GST propositions from Central
Government
·
Suggested additional KPIs for increasing membership,
increasing the number of members signing up to pay the living wage,
development of a carbon reduction Tourism strategy, and the need for some
additional detail to make the performance measures meaningful
·  Council’s
agreed expectations and strategy need to be clear so that HB Tourism can set
clear, appropriate KPIs.
C&S14/19
Resolutions
1.      That the Corporate and Strategic Committee
receives and notes the “HB Tourism Funding Review” staff
report.
2.      The Corporate and Strategic Committee
recommends that Hawke’s Bay Regional Council:
2.1     Adopts
the Key Performance Indicators proposed, subject to the Hawke’s Bay
Tourism Board of Directors ratifying them, and incorporating any amendments agreed
by the Committee today.
Graham/Wilson
CARRIED
13.
Treasury Report
Jessica Ellerm introduced the item, which
provides an update to the Committee on investment performance, borrowing and
debt funding, before introducing Rhys Weyburne and Brian Kearney, presenting
on behalf of Mercer Investments.
·
Transition of funds came in through three
transactions, first $5M on 18 January followed by two transactions totalling $15M
in February
·
funds immediately, by day’s end,
invested in accordance with Council’s investment objectives
·
Set up a bespoke investment portfolio for
Council, to meet Council’s objectives -currently 50% growth and 50%
defensive assets
·
Q1 returns were $774,000
·
Socially Responsible investment (ethical) approach
with HBRC funds excludes investments in companies that get more than –
10% of their revenue from adult entertainment, alcohol, gambling or tobacco
related business; more than 20% from extraction of thermal, coal and/or tar
sands oil; and any companies directly involved in development, production,
sale or distribution of cluster munitions, anti-personnel mines, biological,
chemical or nuclear weapons. Other part is proactively investing to make
positive social impacts.
The meeting adjourned at 10.42am and
reconvened at 11am with Cr Rick Barker not in attendance
John Carran (economist, strategist), Ben
Petro (fixed interest investments), Adrian Woodhams and Sam Howard (HB
office) spoke to the First NZ Capital first quarter report.
Early in the year impressive run for global
equities due to key reasons of investor view that market softness last year
was temporary, good data from US economy, optimism that trade tension between
China and US settled and banks committed to keeping interest rates low.
Recent market tension and volatility
increased due to ramped up trade tensions between US and China, and investors
becoming less optimistic. Slightly cautious on equities and valued slightly
higher than normal averages. Fixed income term deposit. returns expected at
around 3% and longer term view to equities market.
Decided not to be a sub-partner in the
Napier Port IPO as feel less constrained but very supportive of the
investment opportunity
Long term focussed portfolio so reporting
six monthly should be sufficient.
C&S15/19
Resolution
That the Corporate and
Strategic Committee receives and notes the “Treasury Report”.
Wilson/Bailey
CARRIED
11.
Business HB Update
Tom Skerman introduced the item and the
Business Hawke’s Bay team (Carolyn Neville (CE), Alan Pollard and David
Kriel) noting that a single ‘service level agreement’ for all
councils is being developed.
Carolyn’s presentation highlighted:
·
Attract, Build, Connect
·
“Think Hawke’s Bay – for
your business” initiative to attract businesses to Hawke’s Bay,
including case studies of successful businesses in HB
·
Regional skills attraction strategy being
developed, from which a campaign will run
·
Future food innovation and land
diversification opportunities including sheep and goat dairy, hemp crops,
sustainable fibre, pyrethrum, etc
·
Start-up 9 week development programme to
support entrepreneurs
·
18 business support agencies represented at
the Business hub
Queries and discussions covered:
·
Regional economic development strategy –
Matariki – the differences between economic development and business
development (BHB lead) which are interrelated
·
Part of the matariki work is around developing
appropriate metrics to measure service delivery around things such as new
investment and business relocations for example
·
Newsletter distributed is a showcase of what’s
happening across HB in the economic development space
·
Councillors keen for ongoing engagement with
BHB
C&S16/19
Resolution
That the Corporate and
Strategic Committee receives and notes the “Business HB Update”
report.
Bailey/Graham
CARRIED
6.
Report and Recommendations from the
Finance Audit and Risk Sub-Committee
Jessica Ellerm introduced the item, highlighting the proposed
Rating invoice, due and penalty dates changes with discussions covering:
·
engaging with the community to smooth the
transition to the new dates including socialisation of the changes ahead of
implementation
·
rates revenue currently comes in towards the
latter quarter of the financial year
·
potential to alleviate impacts on top 10% of
ratepayers by offering payment plans to spread payments across the year
Further discussions traversed:
·
living wage item and disappointing responses
to survey, proposal to give preference to suppliers who pay the living wage
and how the living wage is calculated
·
revised procurement policy and manual in
response to 2018 internal audit
C&S17/19
Resolutions
The Corporate and Strategic Committee:
1.      Receives and notes the “Report and Recommendations
from the 22 May 2019 Finance, Audit and Risk Sub-committee Meeting”
2.      Agrees that the decisions to be made are not significant under the
criteria contained in Council’s adopted Significance and Engagement
Policy, and that Council can exercise its discretion and make decisions on
this issue without conferring directly with the community or persons likely
to be affected by or have an interest in the decision.
Rating Invoice – Proposed Issue, Due and Penalty Date
Changes
3.      Recommends that Hawke’s Bay Regional Council approves the
proposed rates issue, due and penalty dates following, for implementation 1
July 2020.
3.1.      Invoices sent out – early to mid-August
3.2.      Rate assessment/invoice date – 20 September
3.3.      Payment due date – 20 September
3.4.      Penalty date – 21 September.
Proposed
schedule of 2019-20 internal audits
4.      Takes note of
the agreed 2019-20 schedule of Internal Audits within associated budget
allocations, being:
4.1.      IT Security
4.2.      Data
Analytics
4.3.      Risk
Management
4.4.      Asset
Management
4.5.      Water
Management follow-up.
Living wage
update
5.      Considers the
addition of a statement in the Procurement Policy that “Council
gives preference to suppliers who pay employees the Living Wage”
6.      If the
inclusion of a weighting for payment of the Living Wage in a contract
decision is to be considered, that the decision will be determined based on
the type and value of the contract, and should also weigh up other best
business practices.
Procurement
and Contract Management Update
7.      Agrees
support for the proposed Hawke’s Bay Regional Council Procurement Policy
May 2019 and Procurement Manual as revised to reflect the feedback provided
by FARS.
8.      Recommends that Hawke’s Bay Regional Council agrees support
for the Hawke’s Bay Regional Council Procurement Policy May 2019 and
Procurement Manual as revised, including that Policy 5.9 is amended to read
“HBRC will give preference to suppliers who have adopted the living
wage and will consider this as part of the procurement evaluation
process”.
Reports Received
9.      Notes that the following reports were provided to the Finance
Audit and Risk Sub-committee.
9.1.      Water Management Follow-up Internal Audit Report
9.2.      Living wage update
9.3.      Draft 2019-20 Annual Plan for review and feedback
9.4.      May 2019 Sub-committee Work Programme Update.
Paku/Bailey
CARRIED
The meeting
adjourned at 12.30pm and reconvened at 1.00pm with Councillor Rick Barker in
attendance
7.
HBRC 2019-20 Annual Plan
Jessica Ellerm introduced the item, advising that updates from the
Finance Audit and Risk Sub-committee are not all included in the copies
provided due to timing issues and reiterating Council’s earlier
decision not to consult. Discussions traversed:
·
Audit NZ does not audit annual plans where
there is not material change from what was proposed in the Long Term Plan
·
Financials will be updated with the outcomes
of the decisions made on carry forwards
·
Increased borrowings relate substantially to
sustainable homes and the erosion control scheme
Councillor
Rex Graham left the meeting at 1.20pm
·
Notes to clearly identify what the numbers
represent, i.e. sustainable homes, Port IPO proceeds
·
Some carry forwards relate to activities
spanning the end/beginning of the financial year
·
Figures are as at 30 April and so financial
year end results may be different when actual results known
·
Delays to work starting while policy is
agreed, e.g. erosion control scheme, resulted in unspent budgets needing to
be carried forward so activities can carry on
Councillor
Debbie Hewitt left the meeting at 1.30pm
·
Will get better at forecasting scheme uptake
however will never be precise enough to not require any carry forward of
budgets
·
Scheme Maintenance is averaged so year to year
variations are smoothed, engineering staffing has been under resourced for 3
years and is being built up
·
Funds leveraged against central government
funding could unravel the project as a whole if not carried forward
·
All of the erosion control scheme carry
forward funds are committed to landowner agreements as determined through a
rigorous engagement process
·
CAPEX does not impact directly on the rate
– planned $15m spend, deliberate decision to pause software
implementation and IT strategy to move to an integrated platform, replacing
bespoke council IT systems
·
Change title of Ngaruroro water security to
regional fresh water security
·
Drilling programme signalled in previous LTP
as well as 2018-28, at least 2 deep wells required to calibrate SkyTEM
·
Two page release scheduled in media tomorrow
as this meeting has effectively ‘publicised’ the Annual Plan
C&S18/19
Resolutions
1.      That the Corporate and Strategic Committee:
1.1.      Receives and notes the “HBRC 2019-20 Annual Plan”
staff report.
1.2.      Receives and notes the two page public notice informing the
community of key highlights of the 2019-20 Annual Plan included in this staff
report.
1.3.      Receives and notes the 2019-20 Annual Plan Carry Forward Budget
Considerations included in this staff report.
2.      The Corporate and Strategic Committee recommends that
Hawke’s Bay Regional Council:
2.1.      Adopts the 2019-20 Annual Plan, incorporating amendments and carry
forwards agreed at today’s meeting, in accordance with Section 95 and
Section 82(A)(3) of the Local Government Act 2002.
Belford/Barker
For: Beaven, Bailey, Belford, Barker,
Paku, Kirton
Against: Dick, Wilson, Waaka
CARRIED
9.
Organisational Performance Report for
Period 1 February to 30 April 2019
The item was taken as read
C&S19/19
Resolution
That the Corporate and Strategic Committee receives
and notes the “Organisational Performance
Report for Period 1 February to 30 April 2019”.
Barker/Beaven
CARRIED
10.
Strategic Plan Implementation
James Palmer
introduced the item, which provides an overall view of the major activities
and projects across the organisation to deliver Council’s strategic
goals. Some thought to be given to communicating this document with the
public to assist with the community’s understanding of what it is
Council does and is achieving, potentially in the context of the Annual Plan.
C&S20/19
Resolution
That the Corporate and Strategic Committee
receives and notes the “Strategic Plan Implementation”
staff report.
Beaven/Barker
CARRIED
12.
Discussion of Minor Items not on the
Agenda
There
were no items raised for discussion.
Pieri Munro closed the meeting with a
karakia.
Closure:
There
being no further business the Chairman declared the meeting closed at 2.25pm on
Wednesday, 5 June 2019.
Signed
as a true and correct record.
DATE: ................................................               CHAIRMAN:
...............................................