Minutes of Strategy Planning and Partnerships Committee Meeting - 16 May
2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Strategy Planning and
Partnerships Committee
Meeting
Date:
Thursday, 16 May 2019
CG-14-72-00038                                                                         1
Minutes
of a Meeting of the Strategy Planning and
Partnerships Committee held on Thursday, 16 May 2019 at 1.00pm
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  1
4.         Petition
- Kotuku and Takapu Street, Camberley  2
5.         On-Street
Parking Meters  2
6.         Variation
7 to The Proposed Hastings District Plan - Seasonal Workers Accommodation   2
7.         National
Planning Standards  3
8.         Additional
Business Items  4
9.         Extraordinary
Business Items  4
10        Recommendation
to Exclude the Public from Item 11  4
CG-14-72-00038                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE Strategy
Planning and Partnerships Committee HELD IN THE Council Chamber, Ground Floor,
Civic Administration Building, Lyndon Road East, Hastings ON Thursday,
16 May 2019 AT 1.00pm
Present:                          Councillor Lyons (Chair)
Mayor Hazlehurst
Councillors
Barber (Deputy Chair), Dixon, Harvey, Heaps, Nixon, O’Keefe, Poulain,
Redstone, Schollum, Travers and Watkins and Tracee Te Huia
ALSO Present:              Chief Executive (Mr N Bickle)
Group Manager: Planning and Regulatory Services (Mr J O’Shaughnessy)
Chief Financial
Officer (Mr B Allan)
Regulatory
Solutions Manager (Mr J Payne)
Transportation
Manager (Mr J Pannu)
Traffic
Engineering Officer (Mr L Crawford)
Business Analyst
(Mr C Thorsen)
Acting
Environmental Policy Manager (Mrs M Gaffaney)
Environmental
Planner (Policy) (Mr C Scott)
Committee
Secretary (Mrs C Hilton)
1.         Apologies
Councillor
Lyons/Councillor Watkins
That apologies for absence from
Councillors Kerr and Lawson be accepted.
CARRIED
2.         Conflicts
of Interest
There were no
declarations of conflicts of interest.
3.         Confirmation
of Minutes
Councillor Dixon/Councillor Redstone
That the
minutes of the Strategy Planning and Partnerships Committee Meeting held Thursday
14 March 2019 be confirmed as a true and correct record and be adopted.
CARRIED
The Chair
acknowledged that this was the first meeting of the Strategy Planning and
Partnerships Committee since Tracee Te Huia had been appointed to the
committee, and he welcomed her to this meeting.
4.
Petition - Kotuku and Takapu Street, Camberley
(Document 19/457) (Copy of petition [TR-11-2-19-1427]
was tabled at meeting)
Mayor
Hazlehurst/Councillor Redstone
A)        That
the report of the Committee Secretary titled “Petition - Kotuku and
Takapu Street, Camberley” be received.
B)        That
the tabled “Petition - Kotuku and Takapu Street, Camberley” be
received.
C)        That
officers prepare a report to a future Council meeting on “Petition -
Kotuku and Takapu Street, Camberley”.
D)        That
the lead petitioner Sally Maoate be invited to speak to the petition, at the
meeting referred to in “C” above.
CARRIED
5.
On-Street Parking Meters
(Document 18/813)
Alternate recommendation wording had been
circulated prior to the meeting with some background information (CG-14-72-00040).
Officers responded to questions from the committee.
Mayor Hazlehurst/Councillor Watkins
A)        That the report of the Regulatory Solutions Manager titled
“On-Street Parking Meters” dated 16/05/2019 be left to lie on the
table to allow Council officers time to inform the Hastings City Business Association
of the parking technologies that are proposed to upgrade the existing parking
meters in the Hastings CBD.
CARRIED
6.
Variation 7 to The Proposed Hastings District Plan
- Seasonal Workers Accommodation
(Document 19/411)
(Attachment included in separate agenda document)
Councillor Watkins/Councillor Redstone
A)        That
the report of the Environmental Policy Manager titled “Variation
7 to The Proposed Hastings District Plan - Seasonal Workers Accommodation”
dated 16/05/2019 be received.
B)        That Variation 7 to the Proposed Hastings District Plan and the
Section 32 Evaluation Report, be adopted for public notification pursuant to
Schedule 1 of the Resource Management Act 1991.
With the
reasons for this decision being that the objective of the decision will
contribute to the performance of regulatory functions by:
i)          Ensuring
that Council provides for the sustainable management of the natural and
physical resources of the District under the Resource Management Act, by
providing for seasonal workers accommodation in locations and at a scale
where the environmental effects are minimised.
CARRIED
7.
National Planning Standards
(Document 19/429)
(Attachment included in separate agenda document)
Councillor Lyons/Councillor Heaps
A)        That
the report of the Environmental Policy Manager titled “National
Planning Standards” dated 16/05/2019 be received.
B)        That the Committee notes and endorses the priorities outlined in
the table below:
Task
Suggested Priority
a.
Ensuring that
ePlan is entirely up to date.
1
b.
Settling
Appeals on the Proposed District Plan
2
c.
Making the
Proposed District Plan Operative
3
d.
RSE Workers
Accommodation Variation
4
e.
Implementing
National Planning Standards
5
f.
Variation to
tidy up matters in the Proposed Plan
6
g.
Landscape
Section/ Wāhi Taonga Review
7
h.
Variation to
amend the Light Industrial Zone provisions
8
i.
Implementing
the Medium Density Strategy
9
j.
Forestry Slash
(the hazard effects on waterways created by forestry prunings and
thinnings) if required as a result of joint monitoring approach with HB
Regional Council
10
C)        That the Committee
notes the obligation set out in the legislation to amend the district plan to
meet the new standards and this has influenced the establishment of new
priorities.
With the reasons for this decision being that the objective of the
decision will contribute to meeting the current and future needs of
communities for performance of regulatory functions in a way that is most
cost-effective for households and business by:
i)       Setting priorities for the policy work that is required for the
Council to meet its regulatory functions, and deliver outcomes to the
community, in a timely manner.
CARRIED
8.         Additional Business Items
There were no additional business items.
9.         Extraordinary
Business Items
There were no extraordinary business items.
10.
Recommendation to
Exclude the Public from Item 11
SECTION 48,
LOCAL GOVERNMENT OFFICIAL INFORMATION AND MEETINGS ACT 1987
Councillor Lyons/Councillor Watkins
THAT the public now be
excluded from the following parts of the meeting, namely;
11        Forestry
Slash - Increased Inspection Regime
The general subject of the matter to be considered
while the public is excluded, the reason for passing this Resolution in
relation to the matter and the specific grounds under Section 48 (1) of the
Local Government Official Information and Meetings Act 1987 for the passing
of this Resolution is as follows:
GENERAL SUBJECT OF EACH MATTER TO BE CONSIDERED
REASON FOR PASSING THIS RESOLUTION IN
RELATION TO EACH MATTER, AND PARTICULAR INTERESTS PROTECTED
GROUND(S) UNDER SECTION 48(1) FOR THE PASSING OF
EACH RESOLUTION
11         Forestry
Slash - Increased Inspection Regime
Section
7 (2) (i)
The
withholding of the information is necessary to enable the local authority
to carry on, without prejudice or disadvantage, negotiations (including
commercial and industrial negotiations).
Information
in this report is commercially sensitive.
Section
48(1)(a)(i)
Where the
Local Authority is named or specified in the First Schedule to this Act
under Section 6 or 7 (except Section 7(2)(f)(i)) of this Act.
CARRIED
The meeting closed at 2.25pm
Confirmed:
Chairman:
Date: