Minutes of Temporary Road Closures Subcommittee Meeting - 20 June 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Temporary Road Closures
Subcommittee
Meeting
Date:
Thursday, 20 June 2019
CG-14-16-00329                                                                         1
Minutes
of a Meeting of the Temporary Road Closures
Subcommittee held on
Thursday,
20 June 2019 at 8.45am
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  1
4.         Temporary
Road Closures - Glenross Road - 22 June 2019, Waikoau Road - 23 June 2019,
Otene Road - 30 June 2019 - Hawke's Bay Car Club   1
5.         Additional
Business Items  3
6.         Extraordinary
Business Items  3
CG-14-16-00329                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE Temporary
Road Closures Subcommittee HELD IN THE Green Room, Ground Floor, Civic
Administration Building, Lyndon Road East, Hastings ON
Thursday, 20 June 2019 AT 8.45am
Present:                          Councillor Nixon (Chair)
Transportation Manager – Mr J Pannu
IN ATTENDANCE:             Transportation Officer (Mrs L Burden)
Committee Secretary (Mrs A Murdoch)
1.         Apologies
Leave of Absence had previously been granted to
Councillor Watkins (Deputy Chair).
2.         Conflicts
of Interest
There were no
declarations of conflicts of interest.
3.         Confirmation
of Minutes
Mr Pannu/Councillor Nixon
That the
minutes of the Temporary Road Closures Subcommittee Meeting held Thursday
6 June 2019 be confirmed as a true and correct record and be adopted.
CARRIED
4.
Temporary Road Closures - Glenross Road - 22 June
2019, Waikoau Road - 23 June 2019, Otene Road - 30 June 2019 - Hawke's Bay
Car Club
Mr Pannu/Councillor
Nixon
A)        That
the report of the Transportation Officer titled “Temporary
Road Closures - Glenross Road - 22 June 2019, Waikoau Road - 23 June 2019,
Otene Road - 30 June 2019 - Hawke's Bay Car Club” dated 20/06/2019
be received.
B)        That, there being no submissions received in respect of the
Hawke’s Bay Car Clubs temporary road closure applications, the Council approve
the following temporary road closures:
·        Glenross Road - from outside Rapid Number 336 to the end of the road. Access to
Rapid Numbers 382, 384 and 386 will be retained. The road closure will be
between 9:00am to 5:00pm, Saturday, 22 June 2019.
·        Waikoau Road - from outside Rapid Number 43 to the intersection with Mokamoka
Road. The road closure will be between 9:00am and 5:00pm, Sunday, 23 June
2019.
·        Otene Road - between Ruahapia Road and Elwood Road. The road closure will be
between 9:00am and 5:00pm, Sunday, 30 June 2019.
C)        The Council approve the above temporary road closures subject to
the following conditions to be complied with to the satisfaction of the Group
Manager: Asset Management:
1.      These
events are conducted in accordance with the New Zealand Transport Agency Code
of Practice Temporary Traffic Management (CoPTTM).
2.      The
Traffic Management Plans including provision for appropriate signage are
approved by the Traffic Management Coordinator acting under delegated
authority.
3.      The
Traffic Management Plans must be complied with including any specific
conditions.
4.      Copies
of the relevant liability insurance policies are received.
5.      That
the cost of all advertising is met by the event organisers.
6.      Emergency
Services are contacted regarding the holding of these events with details of the
dates, locations and time frames.
7.      As
per the Traffic Management Plan provisions - all emergency services will be
accommodated and access provided through the sites as required.
8.      The
applicant is responsible under the Health and Safety at Work Act 2015 for all
health and safety risks associated with this activity and must take
reasonably practicable steps to ensure the safety of all persons during these
temporary road closures.
9.      The
applicant has in place at all times, appropriate Health and Safety measures
(to prevent harm to any persons), including (but not limited to) any measures
provided for in the submitted Health and Safety Plans including any
conditions attached.
10.    These
Hawke’s Bay Car Club events shall be conducted in accordance with the
requirements of the Motorsport New Zealand Motor Sport Manual, National
Sporting Code and Appendices.
With the
reasons for this decision being that the objective of the decision will
contribute to the good quality local infrastructure by:
i)          That the
use of the above listed street/roads for these short periods of time will not
unreasonably impede traffic.
ii)         Allowing the above temporary road closure the organiser can
undertake their event in a safe and controlled environment with all risks minimised
and managed.
CARRIED
5.         Additional Business Items
There were no additional business items.
6.         Extraordinary
Business Items
There were no extraordinary business items.
________________________
The meeting closed at 8.48am
Confirmed:
Chairman:
Date: