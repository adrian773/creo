Minutes of Council Meeting - 29 August 2019
Council Meeting Minutes                                                                                               29
August 2019
MINUTES OF Central
Hawkes Bay District Council
Council Meeting
HELD AT THE Council Chamber, 28-32
Ruataniwha Street, Waipawa
ON Thursday, 29 August 2019 AT 9.00am
PRESENT:              Mayor Alex Walker
Cr Ian Sharp (Deputy
Mayor)
Cr Kelly Annand
Cr Tim Aitken
Cr Tim Chote
Cr Gerard Minehan
Cr Brent Muggeridge
Cr David Tennent
Dr Roger Maaka
IN ATTENDANCE:
Joshua Lloyd (Group
Manager, Community Infrastructure and Development)
Bronda Smith (Group
Manager, Corporate Support and Services)
Doug Tate (Group
Manager, Customer and Community Partnerships)
Nicola Bousfield
(People and Capability Manager)
The meeting opened at 9.05am
1            Prayer
Dr Maaka
opened the meeting with a karakia.
2            Apologies
Cr
Burne-Field approved leave of absence as per June 29th Meeting
3            Declarations
of Conflicts of Interest
None
4            Standing
Orders
Resolved:  19.78
Moved:       Cr
Ian Sharp
Seconded:  Cr
Kelly Annand
THAT the following standing orders are
suspended for the duration of the meeting:
20.2 Time limits on speakers
20.5 Members may speak only once
20.6 Limits on number of speakers
And that Option C under section 21
General procedures for speaking and moving motions be used for the meeting.
Standing orders are recommended to be
suspended to enable members to engage in discussion in a free and frank
manner.
Carried
Item 7.1 was considered as the first
item prior to proceedings from committee and council meetings.
5            Confirmation
of Minutes
Resolved:  19.79
Moved:       Cr
Gerard Minehan
Seconded:  Cr
Ian Sharp
That the minutes of the Ordinary Council
Meeting held on 20 June 2019 as circulated, be confirmed as true and correct.
Carried
6            Reports
from Committees
6.1         Minutes
of the Finance and Planning Committee Meeting held on 15 August 2019
Motion
Moved:       Cr Ian Sharp
Seconded:  Cr
Kelly Annand
1.
That the minutes of the meeting of the Finance and Planning
Committee held on 15 August 2019 be
confirmed as true and correct.
6.1.1      Resolution
to apply for funding to Eastern and Central Community Trust
Resolved:  19.80
Moved:       Cr
Ian Sharp
Seconded:  Cr
Tim Chote
That having considered all
matters raised in the report:
That Finance and Planning Committee recommend to Council
to resolve to apply for funding of $250,000 to the Eastern and Central
Community Trust Community Assets and Facilities Fund for Ngā Ara Tipuna
–Waipukurau Pā site interpretation.
Carried
6.1.2      Remuneration
Authority Determination - Childcare Allowance
Resolved:  19.81
Moved:       Cr
Ian Sharp
Seconded:  Cr
Tim Aitken
That having considered all
matters raised in the report:
a)         That
Council receives the information contained in the report.
Carried
RECOMMENDATION
That having considered all
matters raised in the report:
b)         That
Council agrees to approve payment of a Childcare allowance as per the
2019-2020 determination
c)         That
Council note that if approved, the necessary provisions will be included in
the Elected Members Remuneration, Allowances and Reimbursements Policy for
Council adoption following the 2019 triennial election.
In
Favour:       Crs Alex Walker, Ian Sharp,
Kelly Annand and Tim Aitken
Against:           Crs
Tim Chote, Gerard Minehan, Brent Muggeridge and David Tennent
equal
Cr Sharp stated that many barriers exist for people wanting to
represent their communities on Council. He determined that providing a child
care allowance was an opportunity to remove one of those barriers. Cr Sharp
strongly recommended the recommendation be approved.
Cr Aitken concurred with Cr. Sharp in his reasoning for the
recommendation to be approved but insisted the recommendation return to the
table for deliberation on a capped value amount per eligible councillor.
Cr Muggeridge opposed the recommendation as it would cost the
ratepayer and he saw no reason for it. Cr Muggeridge expressed that everyone
has barriers in life and that councillors, not the ratepayer, should pay for their
childcare.
Cr Tennent voiced reservations in regards to how the fund would be
applied, saying that it is a simplistic answer to the challenge of limited
representation in councils. The childcare support could be used by anyone with
care responsibilities. However, Cr Tennent fully accepts that councils are
overrepresented by older males which he agreed is unfortunate and he would like
to see a wider spread of representation and is against the policy. Cr Tennant
had changed his mind on the matter and voted against the policy.
Cr Annand questioned whether there was a conflict of interest in the
matter as she is potentially a person who could utilise the fund going forward?
Cr Annand sought advice from the CEO.
CEO Davidson advises Cr Annand that the point of time when the
pecuniary conflict of interest would be executed is when the policy is adopted.
Because the policy will determine how the funding is allocated. This decision
is to provide officers guidance as to whether we should go ahead and prepare a
policy. Therefore it is not necessary to declare a conflict of interest at this
moment.
Cr Chote reiterated what he had said at the committee meetings. He
has not changed his mind and would be voting against the recommendation.
Mayor Walker pointed out that the guidelines from the remuneration
authority on the policy are very clear. There are childcare mechanisms that are
excluded from the policy. It is tightly described and not an “open
slather” mechanism. And based on a councillor’s salary at the table
to pay for the childcare required to do a day’s work every week, would
leave over $180 per week for the councillor to do the work at this table. Mayor
Walker provided perspective as to why the policy has been suggested and
promoted by the remuneration authority, and also promoted by the 32 people in
local government in New Zealand who are under the age of 40, who identify
childcare as a barrier.  Mayor Walker stated that she understands this is
potentially an additional amount of money that ratepayers would be asked to
contribute to. However, we also have a responsibility to seek as wide a representation
as we can to ensure the voices and opinions shared at the table represent
across the spectrum, and that people are not unfairly excluded. Therefore Mayor
Walker will be voting in support of the recommendation.
Cr Sharp addressed the cost to rate payers as mentioned by other
councillors, suggesting the ratepayers may get exceptional value for their
money because of the quality of representation they would receive, if this
barrier was removed. Cr Sharp pointed out that he has no input into his remuneration
or terms of employment or contract, and therefore it is not comparable to a
contractor who does have such input to their remuneration. Cr Sharp said that
councillors currently claim for expenses associated with representing their
community, such as travel expenses, and that childcare is in the same category
– a barrier to be removed. There are barriers for all sorts of people not
currently represented around the table, but that is not a good enough reason to
remove the opportunity for this particular barrier to be removed to allow
greater representation at the table.
Division was called in voting. There were 4 votes for and 4 against
leaving Mayor Walker, as Chair, with the casting vote.
Mayor Walker stated that tradition would say that the casting vote
should be cast to maintain the status quo. Though it pained her to have to cast
the vote against the recommendation - Mayor Walker passed her casting vote
against the recommendation so as to maintain status quo.
6.1.3
Leachate to landfill Irrigation Budget
Resolved:  19.82
Moved:       Cr
Ian Sharp
Seconded:  Cr
Tim Chote
a)    That,
approval is given to option two to approve the use of after-care landfill
reserve to fund the additional $333,000 to allow the Leachate to Landfill
project to be delivered and refund the reserve over the next 10 years.
b)    That
the report (without attachments) relating to this item be released as
publicly available information following the tender process.
Carried
Cr Sharp said he is glad to see resolution to the ongoing leachate issue
of 11 years. Council should be congratulated as they have an obligation and
moral responsibility to remove leachate from our wastewater ponds, which
eventually ends up in our rivers and countryside. Therefore Cr Sharp was very
happy to recommend the resolution.
Cr Chote agreed with Cr. Sharp and is very happy with the funding
decisions and to see progression of the project.
Cr Tennent said it is fantastic to see the project finally going
ahead. Cr Tennent thanked management and staff for their involvement in the
process.
Minehan thanked staff for their work on the project and voiced his
approval that the project is going ahead.
Mayor Walker supported the recommendation however expressed
disappointment at the lack of robust governance input on the project. Mayor
Walker acknowledged that the project was founded on some really important
environmental principles, however there was not a robust asset management plan
in place nor a process comparable in size to work done on the Big Water Story
and wastewater issues. Mayor Walker stated that it is for governors to ensure process
is legal and that ratepayers’ money is allocated responsibly. Mayor
Walker challenged fellow councillors to reflect on the balance between rigour
and pace in getting projects across the line.
Sharp – Congratulated staff on their courage to put the project
forward given the barriers.
CEO Davidson clarified that 3 independent reports recommended improvement
to the quality of the wastewater treatment process and discharge, concluding
that removing leachate from the wastewater facility would indeed have a
positive effect.
6.2         Minutes
of the Risk and Audit Committee Meeting held on 15 August 2019
Resolved:  19.83
Moved:       Cr Tim Aitken
Seconded:  Cr
Brent Muggeridge
1.       That the minutes
of the meeting of the Risk and Audit Committee held on 15 August 2019 be
received.
Carried
6.3         Minutes
of the Community Development Committee Meeting held on 15 August 2019
Resolved:  19.84
Moved:       Cr Kelly Annand
Seconded:  Cr
Ian Sharp
1.       That the minutes
of the meeting of the Community Development Committee held on 15 August
2019 as circulated, be confirmed as true and correct
.Carried
6.3.1      Adoption of Economic
Development Action Plan
Resolved:  19.85
Moved:       Cr
Kelly Annand
Seconded:  Cr
Tim Aitken
That having considered all
matters raised in the report:
a)      That
the Community Development Committee recommend to Council the adoption of    the
Central Hawke’s Bay Economic Action Plan with noted changes.
Carried
Mayor Walker said that the Economic Development Action Plan is a
great step forward, providing clear direction for the economic future of CHB
and demonstrates the role council can play whether through action, advocacy or
facilitation. Mayor Walker added it is great to see forestry incorporated into
the action plan for land use.
Cr Annand agreed with Mayor Walker and was pleased to see the EDAP
adopted.
6.3.2      ADOPTION OF PROJECT CHARTER
FOR SECTION 17A REVIEW OF RETIREMENT HOUSING
Resolved:  19.86
Moved:       Cr
Kelly Annand
Seconded:  Cr
David Tennent
That
having considered all matters raised in the report:
a)      That
the Community Development Committee recommend to Council that the Project
Charter for the Section 17a Review of Retirement Housing be adopted.
Carried
Cr Annand spoke to the motion that this is very exciting and a step
in the right direction.
Cr Tennant said it is high time we had a 17a review of retirement
housing as we have an underutilised asset that could be developed into
something really worthwhile for the district. He wished the incoming council
all the best and hoped portfolio growth would result from the review.
6.3.3      Adoption
of Libraries Strategic Framework
Resolved:  19.87
Moved:       Cr
Kelly Annand
Seconded:  Cr
Ian Sharp
That having considered all
matters raised in the report:
a)         That
the Community Development Committee recommends to Council to adopt the
Libraries Strategic Framework 2019 – 2024.
Carried
6.4         Minutes
of the Environment and Regulatory Committee Meeting held on 15 August 2019
Resolved:  19.88
Moved:       Cr David
Tennent
Seconded:  Cr
Brent Muggeridge
1.
That the minutes of the meeting of the Environment and
Regulatory Committee held on 15 August 2019 as circulated, be confirmed as true and correct.
Carried
7            Report
Section
Submission on the Waste Management and Minimisation
Plan 2019
PURPOSE
The purpose of this report is to present to Council
submissions and feedback received on the Draft Waste Management and
Minimisation Plan (WMMP) 2019.
Resolved:  19.89
Moved:       Cr Gerard
Minehan
Seconded:  Cr
Tim Chote
a)   That the submissions on the WMMP be
received.
b)   That council receive the late
submission on the WMMP
Carried
Representatives from Argyll East School presented their submissions.
Neen Kennedy gave a Powerpoint presentation and spoke to her
submission.
Cr Sharp enquired if there was a preferred location for an
EnviroCentre as raised in Neen Kennedy’s submission.  Ms Kennedy
responded that it would be preferable for it to be located somewhere visible
and easily accessible to the public.
Abby O’Kane presented a submission on behalf of Jennifer
Brown.
Cr Tennent questioned why rubbish/recycling collection fees should
be subsidised for farmers but not for urban areas?
Miss O’Keane offered that some farmers may not be utilising
the existing recycling facilities and that this could be an opportunity to set
a precedent, encouraging farmers to recycle plastic waste.
Harold Petherick spoke to his submission.
7.2         Deliberations
on the Waste Management and Minimisation Plan 2019
Purpose
The purpose of this report is to present to Council
analysis of the submissions received in relation to the recent Waste
Management and Minimisation Plan (WMMP) 2019 consultation.
Resolved:  19.90
Moved:       Cr David Tennent
Seconded:  Cr Kelly
Annand
a)    That,
having considered all matters raised in the report, that Council deliberates
on submissions received on the Draft Waste Management and Minimisation Plan.
Carried
Resolved:  19.91
Moved:       Cr Ian Sharp
Seconded:  Cr
Kelly Annand
b)    That
feedback from Councillors having deliberated on submissions is included in
the WMMP to be brought back for adoption on 26th September 2019.
Carried
Cr Annand requested that education and the cost of dumping rubbish
be added to the plan for consideration.
Cr Aitken suggested involving schools in the design of responsible
waste disposal signs for community placement.
Cr Tennant asked if there are options in the WMMP for the recycling
of plastic bale wrap, stating the organisation has a role to play in
facilitating, not subsidising a recycling service.
Mayor Walker responded that expansion of rural area provisions is
covered in the actions including some practical ideas around recycling bale
wrap and other rural waste.
Cr Annand queried whether Enviro Schools reported back to council on
a regular basis, as it is important to understand what is happening in this
education space, harnessing the next generation’s enthusiasm for
environmental sustainability.
Cr Minehan requested in response to Neen Kennedy’s submission
that a physical location for an Enviro Centre be explored. Furthermore, that
the issue of roadside rubbish dumping be discussed.
Mayor Walker responded that the roadside rubbish issue encompasses a
lot of different activities and not just the WMMP but it is feedback that will
be noted.
Officers noted an action in the plan’s regulation space to
look at the enforcement of littering bylaws.
Mayor Walker requested education be added as a key trend in the plan
conclusions.
Cr Sharp congratulated and thanked all involved in the process while
expressing his hope that public consultation continues prior to the final plan
presentation to Council in September.
Cr Annand thanked all involved and stated that incorporation of
public views into the plan is a move in the right direction, paving the way for
future development.
Mayor Walker added that it is incredibly powerful how the community
has reached out to the Council on the WMMP and ESS. That in aspiring for a
waste-free CHB it is important that contributing community members/parties/schools
are acknowledged and supported going forward as they are pivotal in plan
development. Mayor Walker thanked all involved.
7.3         Adoption
of Hawke's Bay Civil Defence Group Annual Report 2017/18.
PURPOSE
The purpose of this report is to procedurally receive the
annual report of the Hawke’s Bay Civil Defence Emergency Management
(CDEM) Group, covering activities over the 2017/18 year.  Ian MacDonald
will be attendance for this item where he will speak to local achievements
for Civil Defence in Central Hawke’s Bay.
Resolved:  19.92
Moved:       Cr Brent
Muggeridge
Seconded:  Cr
Gerard Minehan
That, having
considered all matters raised in the report, the report be noted. Carried
Cr Annand asked for an example of what a recovery tool is?
Mr MacDonald answered that forming groups of people from areas such
as business, environment and infrastructure who would be involved in recovery
would be one tool.  Also deciding what the structure of the group (roles etc.)
would look like, getting as much pre-planning done as possible before something
happens.
Cr Aitken asked about community resilience - how long does Civil
Defence remain in affected areas and what is the follow up process?
Mr Macdonald responded that plans will be reviewed after 2-3 years
depending on the complexity. Also the plan is to follow up with locals in the
community as to whether the plan works - e.g. Kaikoura review.
Mayor Walker added that as community representatives Council should
be touching base regularly with community champions, checking in that they are
still engaged, supported and motivated.
7.4         Update
on Draft District Plan
PURPOSE
The purpose of this report is to formally update Council
on the Draft District Plan and to consider appointees to the Informal
Hearings Panel to consider all submissions on the draft District Plan.
Resolved:
19.93
Moved:       Cr Brent
Muggeridge
Seconded:  Cr
Kelly Annand
That having considered all matters raised in the
report:
a)         That
Council receive the minutes of the District Plan Sub- committee meeting of 14
August 2019.
b)        That
Council adopt the recommendation of the District Plan Subcommittee that the
existing District Plan Subcommittee hear the informal submissions and make
recommendations on all submissions to Council regarding the draft District
Plan.
c)         That
Officers review the Terms of Reference for District Plan Subcommittee to be
brought back and considered by Council prior to the District Plan Submission
hearings commencing.
Carried
Cr. Ian Sharp and Cr. Tennant declared a conflict of interest regarding
item b). and removed themselves from the table.
Cr. Maaka as the iwi representative on the District Plan Subcommittee
commented on the last phases, stating that every aspect had been fully workshopped
and debated; that the planning committee recommendation had been thoroughly
deliberated; and from an iwi perspective Cr Maaka fully supports the recommendation
of the sub-committee.
Cr. Annand asked for clarification on when the District Plan’s
budget and on-going costs would be presented to the Council?
Mr Tate intends that the budget would be brought back to the Council
table in November.
Mayor. Walker added that remuneration of independent persons on a
hearings panel would be tied into the terms of reference to be considered and adopted
by present Council, and also into the remuneration policy to be reviewed and
adopted by Council post-election.
Tim Chote voiced concerned that elected members not part of the
subcommittee and therefore not present at hearings would not have input into
recommendations.
Mayor Walker put forward that elected members could opt to observe
hearings. Mayor Walker asked how recommendations would come back from a
hearings panel to the Council table.
Officers explained that in early 2020, panel members will deliberate
on the submissions and take advice from our planning consultants about
recommended approaches to address them. Subsequently, another report will be
presented to Council for ratification. The final approval or rejection of submissions
will be made at the Council table.
Mayor Walker added that at any time the Council could deliberate on
recommendations from subcommittee; provide feedback and request further
recommendation before final decision.
Cr Muggeridge spoke to point b). of the recommendation pointing out
that it would be senseless to remove the people involved thus far from the
equation, that he respects their knowledge and the recommendations they will
bring to the council.
Cr Annand spoke to the motion and expressed her confidence in the
process concurring with Cr Muggeridge that she had confidence in those
involved, concluding that the District Plan is a vital strategic document for the
community.
Cr Aitken stated that he is comfortable that council will have input
into the final decisions on the plan.
Cr Sharp and Cr Tennant returned to the table 10:57am.
7.5         Draft
Results and Carry Forwards
PURPOSE
The matter for consideration by the
Council is the draft financial results for 2018/19
and for Council to approve the Carry Forwards and additional Loans.
Resolved:  19.94
Moved:       Cr
Ian Sharp
Seconded:  Cr
David Tennent
That
having considered all matters raised in the report:
a)         Council
approve the budget allocations proposed to be carried forward from 2018/19
year to 2019/20 year to enable projects to be completed and future work to be
funded.
b)         Council
approve the District Plan loan of $15,462 for a period of 10 years and Water
Supply Loan of $266,951 for Operational expenditure for a period of 10 years
to fund the additional expenditure required for the 2018/19 financial year.
Carried
Cr Muggeridge – in regards to District Plan spend - pg. 305 -
Council were notified November 2018 there would be $110,000 overspend. $77,000
has been spent to date leaving $33,000.  Is there further funds in this
year’s budget to supplement the $33,000?
Officers informed Council that there is a bigger budget and any
surplus from the 2019 $110,000 overspend budget would be added to the 2020
overall budget.
CEO Davidson clarified that while a potential need to loan fund
$110,000 was indicated in November 2018, some sub-activities have spent under-budget
or there has been more income. Therefore rather than loan funding the whole
$110,000 or $77,000 it is recommended that we only loan fund $15,000.
Cr Tennant voiced his disappointment that the Capital Projects Fund
is still sitting in deficit after a number of years, stating that it is
contrary to policy.
Mayor Walker asked Cr Tennant for suggestion as to where funds could
come from to remedy the deficit?
Cr Tennant suggested it was an operational matter.
Mayor Walker thanked Cr Tennant for reminding Council of the issue
and asked Mrs Smith to explain why the Capital Projects Fund is currently in
deficit?
Mrs Smith explained the deficit was created because payment for the
turf was required before the reserves were sold. Also in the process of selling
those assets, the Capital Projects Fund went into deficit when payment was made
for land surveying and valuation.
Mayor Walker asked if 6 properties were awaiting DOC Reserve status
to be lifted
Mr Tate confirmed there are 6 properties and 5 of those are in the
final stages of being declassified. The declassification of the 6th
property has been delayed due to a legal issue.
CEO Davidson added that while the time the DOC process has taken is
frustrating, the land value has increased over time resulting in a healthier
Capital Projects Fund.
Cr Muggeridge queried the explanation that the re-valuation is the
primary reason for the variance of close to 1.9 million in depreciation (pg.
303-305). Could officers please explain?
Officers explained that recommendation of the re-valuation was that depreciation
be adjusted this financial year. We write back out the depreciation and then
bring in a new depreciation based on the revaluation requirements, which
increases the depreciation expectation in this financial year.
Mayor Walker suggested that Cr Muggeridge’s question would be
a good question to put to the auditors when they present the audited accounts with
the Annual Report.
Cr Sharp spoke to the motion expressing that he is very happy
to move the recommendation, having complete faith in the accuracy of the figures
presented.
Cr Tennent gave his compliments to the staff involved.
Mayor Walker stated that the responsibility Council has in taking
recommendations is important. That it is recognition of an incredibly valuable
improvement in process around finances in this Council, and an important part
of how we are assure ratepayers money is used respectfully on the appropriate
activities. Mayor Walker reminded all present the report is the culmination of 3
years hard work and transparency before thanking all involved.
7.6         Annual
Dog Control Policy and Practices Report
PURPOSE
Each year Council reports on the effectiveness of the
Central Hawke’s Bay District Council’s dog policy and control
practices.  This report is required under the Dog Control Act 1996.
The report includes statistics for registrations and
complaint investigation, as well as key achievements for the reporting
period.
Resolved:  19.95
Moved:       Cr Gerard
Minehan
Seconded:  Cr
Kelly Annand
That, having
considered all matters raised in the report, the report be noted. Carried
Cr Tennent asked whether the review was useful in for strategic
planning going forward.
Officers confirmed the review has been most useful.
Mayor Walker stated that the increase in infringements and
prosecutions of those breaking dog control rules is a positive result, as it is
our responsible dog owners funding dog control activity via their payment of
dog registration fees.
7.7         District
Licensing Annual Report
PURPOSE
The Council is required to report annually on the
proceedings and operations of the District Licensing Committee under section
199 of the Sale and Supply of Alcohol Act 2012.
Resolved:  19.96
Moved:       Cr Tim Chote
Seconded:  Cr
Gerard Minehan
That, having
considered all matters raised in the report, the report be noted. Carried
Cr Chote stated that everything is going well and he is happy to
move the recommendation.
7.8         Outcome
of Rating Review Stage 1
PURPOSE
The purpose of the report is for Council to adopt the
outcome of the first stage of the Rating Review.
Resolved:  19.97
Moved:       Cr Gerard Minehan
Seconded:  Cr Tim
Chote
That having considered all
matters raised in the report:
a)         Council
adopts the step one analysis for its activities as per the attached schedule.
b)         The
Property and Building Activity costs be considered within Libraries, Council
Administration and Theatres and Halls.
Carried
Cr Sharp recognised the opportunity for new Council to ensure
the rating system is equitable, fair and achievable. Cr Sharp wished the new
Council well with the review.
Mayor Walker asked Officers if they would go through the funding
needs analysis to ensure it is consistent and robust?
Mrs Smith responded it prudent that the new council consider the
results of Stage 1 before considering Stage 2.
Cr Muggeridge queried whether Stage 1 could be revisited if
necessary.
Mrs Smith confirmed yes.
8            Chief
Executive Report
8.1         Organisation
Performance and Activity Report June - July
PURPOSE
The purpose of this report is to present to Council the
organisation report for June/July 2019.
Resolved:  19.98
Moved:       Cr Tim Aitken
Seconded:  Cr
Ian Sharp
That, having
considered all matters raised in the report, the report be noted. Carried
Cr Aitken voiced concern over the impact Central
Government’s 8 new projects would have on the District Plan.
Cr Tennant congratulated staff on an excellent
report.
Mayor Walker acknowledged the high level of
transparency presented by the organisation and also commended the visible
progress on The Big Water Story.
9            Public
Excluded Business
RESOLUTION
TO EXCLUDE THE PUBLIC
Resolved:  19.99
Moved:       Cr
Tim Chote
Seconded:  Cr
David Tennent
That the public be excluded from the
following parts of the proceedings of this meeting.
The general subject matter of each matter
to be considered while the public is excluded, the reason for passing this
resolution in relation to each matter, and the specific grounds under section
48 of the Local Government Official Information and Meetings Act 1987 for the
passing of this resolution are as follows:
General subject of each matter to be considered
Reason for passing this resolution in relation to
each matter
Ground(s) under section 48 for the passing of this
resolution
9.1 - Land Transport Contract
Procurement
s7(2)(b)(ii) - the withholding of the information
is necessary to protect information where the making available of the
information would be likely unreasonably to prejudice the commercial
position of the person who supplied or who is the subject of the
information
s7(2)(i) - the withholding of the information is
necessary to enable Council to carry on, without prejudice or disadvantage,
negotiations (including commercial and industrial negotiations)
s48(1)(a)(i) - the public conduct of the relevant
part of the proceedings of the meeting would be likely to result in the
disclosure of information for which good reason for withholding would exist
under section 6 or section 7
9.2 - Land Transport Section 17(a)
Report
s7(2)(b)(ii) - the withholding of the information
is necessary to protect information where the making available of the
information would be likely unreasonably to prejudice the commercial
position of the person who supplied or who is the subject of the
information
s7(2)(i) - the withholding of the information is
necessary to enable Council to carry on, without prejudice or disadvantage,
negotiations (including commercial and industrial negotiations)
s48(1)(a)(i) - the public conduct of the relevant
part of the proceedings of the meeting would be likely to result in the
disclosure of information for which good reason for withholding would exist
under section 6 or section 7
Carried
Resolved:  19.100
Moved:       Cr
Tim Aitken
Seconded:  Cr
Ian Sharp
That Council moves out of Closed Council
into Open Council.
Carried
10          Date
of Next Meeting
Resolved:  19.101
Moved:       Cr
David Tennent
Seconded:  Cr
Ian Sharp
That the next meeting of the Central
Hawke's Bay District Council be held on 26 September 2019.
Carried
11          Time
of Closure
The Meeting closed at 12:42pm.
The minutes of this meeting were
confirmed at the Council Meeting held on 26 September 2019.
...................................................
CHAIRPERSON