 Hastings District Council 

 
Civic Administration Building 
Lyndon Road East, Hastings 4156 

Phone:  (06) 871 5000 
Fax:  (06) 871 5100 

www.hastingsdc.govt.nz 

 
 

 

M I N U T E S 

 
 

LANDMARKS ADVISORY GROUP 

 
  
 

Meeting Date:  Monday, 18 March 2019 

 

 

 
 

 

 

 

 

1 

1 

1 

2 

3 

4 

5 

6 

7 

CG-14-13-00053  

1 

Minutes of a Meeting of the Landmarks Advisory Group held on 

18 March 2019 at 3.00pm 

  

Table of Contents 

 

Page No. 

Item 
  
1. 

2. 

4. 

7. 

3. 

Apologies 

Conflicts of Interest 

Confirmation of Minutes 

Quarterly Report 

Consideration of General Business Items 

3A  Recommendation to Exclude the Public from Item 10 

Keirunga  Gardens  -  Draft  Tree  Management  Plan  -  Consultation 
Phase Update 

Update on City Centre Revitalisation Plan 

Extraordinary Business Items 

 

5. 

6. 

8. 

 

 

CG-14-13-00053  

 

1 

HASTINGS DISTRICT COUNCIL 

 

MINUTES OF A MEETING OF THE LANDMARKS ADVISORY GROUP 

HELD IN THE LANDMARKS ROOM, GROUND FLOOR, CIVIC ADMINISTRATION 

BUILDING, LYNDON ROAD EAST, HASTINGS ON  

 MONDAY, 18 MARCH 2019 AT 3.00PM 

 

 
 

PRESENT: 
 

IN ATTENDANCE: 

ALSO PRESENT: 

 

Chair: Councillor Schollum 
Councillors Dixon and Travers 
Mrs Ruth Vincent (President, Landmarks Trust) 
Landmarks  Trust  Executive:  Joyce  Barry,  Barbara 
Brookfield and Diana McCormack 
 
Parks and Property Services Manager (Mr C Hosford)  
Parks Planning & Development Manager (Ms R Stuart) 
Parks Landscape and Projects Officer (Mr B Leslie) 
Environmental Enhancement Officer (Mr J Clews) 
Chief Financial Officer (Mr B Allan) 
Group  Manager:  Planning  and  Regulatory 
O’Shaughnessy) 
Senior Environmental Planner - Policy (Mr J Minehan) 
Team Leader Environmental Policy (Mrs M Gaffaney) 
Committee Secretary (Mrs C Hunt) 
 
Mr Richard Coles 

(Mr  J 

  

1. 

 

 

 
2. 

4. 

 

APOLOGIES  

Councillor Travers/Councillor Dixon 

That an apology for absence from Councillor Lawson be accepted. 

CARRIED 

An apology from Robin Middlebrook was noted. 

CONFLICTS OF INTEREST  

There were no declarations of conflicts of interest. 
 
CONFIRMATION OF MINUTES 

Councillor Dixon/Mrs Vincent  

That  the  minutes  of  the  Landmarks  Advisory  Group  held  Wednesday  5 
December 2018 be confirmed as a true and correct record and be adopted. 

CARRIED  

 

With the agreement of the meeting Item 6 was taken out of order. 
 
 

 

CG-14-13-00053  

2 

QUARTERLY REPORT 
(Document 19/262) 
 
Opera  House  Precinct  House  - The  Chief Financial  Officer,  Mr  Allan  advised 
that  he  had  been  seconded  to  the  role  of  Lead  Opera  House  Development 
Project. 
 
Mr Allan advised that the Opera House strengthening work was scheduled to be 
completed  by  September  2019  excluding  the fire  remediation  work,  which  was 
expected to be complete by December 2019.   The  Code of Compliance to be 
issued in January/February 2020. 
 
Plaza  – Work  on  the  Plaza  structure  is  progressing  in  preparation  for  the  roof 
installation which was scheduled to take place mid 2019.   
 

Municipal  Building  –  Mr  Allan  advised 
the  Municipal  structural 
strengthening had been approved. Work on the strengthening  was progressing 
with the priority focus remaining on the southern wall area to allow for the Opera 
House to open in February 2020 as scheduled.   

that 

The structural engineering design to strengthen the Municipal Building had been 
developed  to  allow  for  the  full  implementation  of  the  Mathews  and  Mathews 
Architects  concept  to  be  implemented  should  sufficient  funding  be  raised  and 
Council approval be received.  
 
The  Parks  Planning  and  Development  Manager,  Ms  Stuart updated  the  Group 
on the following current Council and community projects that had an impact on 
Landmarks goals: 
 
  Eastbourne Street Upgrade – Construction teams are now on site and the 

estimated project would take four months.   

  Flaxmere  Park  Playground  Upgrade  –  Ms  Stuart  advised  that  the  new 
playground  was  delayed  late  last  year  due  to  concerns  the  new  play 
equipment  would  not  arrive  in  time  for  the  new  playground  to  be  ready  in 
time for the Christmas and New Year holidays.  Work  had  now  begun  on 
the playground. Funding applications to Trust House Community Enterprise 
($25,000)  and  First  Light  Community  Foundation  ($20,000)  had  been 
successful and would enable a new large slide tower to also be purchased. 
 

  Raureka  Reserve  Management  Plan  –  Ms  Stuart  advised  that  the  Parks 
Asset Planner (Jennifer Leaf) who had undertaken the work on the Plan was 
now working for the Central Hawke’s Bay District Council.  There had been 
a  lot  of  submissions  even  though  it  was  a  relatively  small  park  as  it  had 
been proposed to not vest a portion of the Park to allow for future choices.  
However the Park was vested under the Reserves Act in its entirety. 

 

  Cornwall  Park  Reserve  Management  Plan  –  The  Parks  Planning  and 
Development  Manager,  Ms  Stuart  advised  that  35  submissions  had  been 
received  relating  to  the  provisions  included  in  the  Plan  with  regard  to  the 
former tea kiosk building and the lease to the playcentre.  There was also a 
desire  from  some  submitters  to  restore  the  historic  building  for  future 
generations. 

 
 

7. 

 

 

 

CG-14-13-00053  

3 

 

 

A  feasibility  study  would  be  required  to  investigate  possible  solutions  and 
the lease of the Playcentre extended to September 2020.  
 
Prioritise  and  bring  forward  some  projects.  Funds  were  available  over  the 
next three financial years for Windsor Park and these funds would be used 
for Cornwall Park.   

 
  Haumoana  Freedom  Camping  Area  –  A  grant  from  the  Ministry  of 
Business, Innovation and Employment of $190,000 enabled the upgrade of 
the Haumoana Domain for self contained vehicles to be completed.   
 

  Havelock  North  Business  Association  Artwork  –  Two  artworks 
purchased  by  the  Association  “Blade  Totems”,  to  be  installed  in  a  garden 
bed  Joll  Road  and  “Girl  on  a  Swing”  to  be  located  in  one  of  the  large 
branches of the gum tree in the Havelock North Village Green.  To ensure 
the  health of  the  tree,  officers  are  working  with  arborists  on  how  to  secure 
the artwork. 

 

  Façade Enhancement Scheme – The Environmental Enhancement Officer 
Mr  Clews  advised  that  building  façade  enhancements  of  the  East  Blocks 
was continuing.  The T & T Building, BJs Café and the DTR building were all 
now completed. 

 
It  was  noted  that  Queens  Park,  Invercargill  had  a  major  stumpery  which  was 
made up of logs and created artwork. 
 

Councillor Schollum/Councillor Travers  

A) 

That  the  report  of  the  Parks  Planning  and  Development  Manager 
titled “Quarterly Report” dated 18/03/2019 be received. 

CARRIED  

 
 
CONSIDERATION OF GENERAL BUSINESS ITEMS 
 
Te  Ara  Kahikatea  Artwork  –  The  Senior  Environmental  Planner  –  Policy,  Mr 
Minehan displayed a concept design (CG-14-13-00056) of  significant sculpture 
for    the  Whakatu  arterial.    Feedback  on  the  concept  designs  had  been  very 
positive and was in the initial planning stage and identifying suitable sites.   
 
HB  Co-operative  Garage  &  Heretaunga  Street  200  West  Laneway  –  The 
Team  Leader  Environmental  Policy,  Mrs  Gaffaney  advised  that  Council  had 
bought the former Briscoes building previously HB Co-operative Garage at 206 
Queen Street West Hastings which would enable the creation of a CBD linkage, 
pocket  park,  carpark,  improved  service  lane  access  through  the  200  block  to 
Heretaunga Street West. 
 
The  preliminary  concept  plan  was  confidential  at  this  stage  and  the  meeting 
viewed this in Public Excluded. 
 

 

3. 

 

 

 

 

CG-14-13-00053  

4 

 
3A.  RECOMMENDATION TO EXCLUDE THE PUBLIC FROM ITEM 3  

SECTION  48,  LOCAL  GOVERNMENT  OFFICIAL 
MEETINGS ACT 1987 

INFORMATION  AND 

Councillor Schollum/Councillor Travers  

THAT the public now be excluded from the following parts of the meeting, 
namely; 

3. 

Preliminary Concept Plan – 206 Queen Street West 

The  general  subject  of  the  matter  to  be  considered  while  the  public  is  excluded,  the 
reason  for  passing  this  Resolution  in  relation  to  the  matter  and  the  specific  grounds 
under  Section  48  (1)  of  the  Local  Government  Official  Information  and  Meetings  Act 
1987 for the passing of this Resolution is as follows: 

 

 

 

GENERAL  SUBJECT  OF 
EACH  MATTER  TO  BE 
CONSIDERED 

 

REASON FOR PASSING THIS 
RESOLUTION IN RELATION TO 
EACH MATTER, AND 
PARTICULAR INTERESTS 
PROTECTED 

GROUND(S) 
UNDER 
SECTION  48(1)  FOR  THE 
PASSING 
EACH 
RESOLUTION 

OF 

 

3.  Preliminary Concept 

Plan – 206 Queen 
Street West 

Section 7 (2) (i) 

Section 48(1)(a)(i) 

to  enable 

The withholding of the information is 
necessary 
local 
authority 
to  carry  on,  without 
prejudice 
disadvantage, 
negotiations  (including  commercial 
and industrial negotiations). 

the 

or 

Where  the  Local  Authority  is 
named or specified in the First 
Schedule  to  this  Act  under 
Section 6 or 7 (except Section 
7(2)(f)(i)) of this Act. 

CARRIED  . 

The  information  in  this  report  is 
commercially sensitive. 

 

___________________ 

The meeting resumed in open Session at 3.55pm. 

___________________ 

Landmarks Trust Update  

 

Mrs Vincent updated the Group on the Landmarks Trust and highlighted the 
following points: 

  The  Trust  enjoyed  the  regular  updates  with  Rachel  Stuart  and  Colin 

Hosford and being involved in the steps in the processes. 

  This month saw a number of submissions, the Keirunga Trees, the inner 

city living Variation 5, and Cornwall park upgrades.  

  The  Keirunga  tree  work  has  drawn  a  lot  of  attention  in  Havelock  North, 

and Council is to be commended to their responses to those concerns. 

  The Amy Lynch Sculpture is now in the next stage of organising with her 
structural  engineers,  and  getting  pricing  together  by  mid  April,  so  as  we 
can prepare some applications for funding. 
 A Facebook page was being looked into to use as a platform to raise the 
Trust’s profile, advertise events, and keep in touch with members.  

 

  As 

there  are  parameters 

to  consider  Rachel  checking  with 

the 

 

 

 
 

 

 

 

CG-14-13-00053  

5 

Communications Team, the Trust proceeds. 

  The  upcoming  forum  "Creating  Magic  Spaces"  be  at  Heretaunga 

Women’s Centre, Civic Square corner Thursday 28 March 2019.  

  The Landmarks Annual General Meeting will be held on 29 August 2019. 
  Derek Burns has now stepped away from his involvement in the Trust.  

 

Mrs Vincent/Councillor Schollum 

That the Landmarks Trust Update dated 18 March 2019 be received. 

CARRIED 

- 

-  DRAFT  TREE  MANAGEMENT  PLAN 

 
KEIRUNGA  GARDENS 
CONSULTATION PHASE UPDATE 
(Document 18/1076) 
 
The  Parks  and  Services  Manager,  Mr  Hosford  updated  the  Group  on  the 
Keirunga  Gardens  Draft  Tree  Management  Plan  which  was  released  for 
public consultation on 8 December 2018.   
 
Concept  and  ideas  had  been  taken  to  a  community  meeting  and  had  been 
well  received.    A  Draft  Plan  was  put  together  as  a  consultation  document.  
During  the  consultation  phase  an  independent  peer  review  report  was 
undertaken by Arborlab.   
 

The draft plan, the arborlab report and addendum report were made available 
to  the  public  so  that  their  submissions  could  consider  the  wide  range  of 
options  put  forward.    These  documents  could  be  accessed  on  the  Council 
website at; www.myvoicemychoice.nz     Submitters had been advised that in 
light  of  the  recommendations  of  the  Arborlab  report,  they  may  wish  to 
withdraw and/or amend their submissions to take into account an alternative 
arborcultural view. 
 
Due to public interest the submission period has been extended through to 5 
April 2019.  The Council would consider the submissions at its meeting on 14 
May 2019. Submissions will be analysed and options put forward to Council. 
 
Ms  Vincent  advised  that  the  Arborlab  report  was  very  detailed  and 
professional  and  added  large  amount  of  information.    The  addendum 
document  was  very  useful  document.      On  the  basis  of  the  information 
received  Ms  Vincent  advised  that  the  Landmarks  Trust  would  amend  their 
submission to support the Arborlab report. 
 

Councillor Dixon/Councillor Travers  

A) 

That the report of the Parks and Property Services Manager titled 
“Keirunga  Gardens  -  Draft  Tree  Management  Plan  -  Consultation 
Phase  Update”  dated  18/03/2019  be  received  for  information  and 
discussion purposes only. 

CARRIED  

5. 

 

 

 
 
 

 

CG-14-13-00053  

6 

UPDATE ON CITY CENTRE REVITALISATION PLAN 
(Document 19/263) 
 
The  Parks  Planning  and  Development  Manager,  Ms  Stuart  displayed  a 
powerpoint  presentation  (CG-14-13-00054)  and  updated  the  Group  on  the 
Hastings  City  Centre  Public  Spaces  Revitalisation  Plan  (the  Plan)  to  the 
Group that was adopted by Council on 26 February 2019. 

The  Revitalisation  Plan  identified  a  total  of  9  areas  to  assist  with  the 
improvement  of  the  city  centre  through  urban  design  initiatives  and  these 
areas are listed below: 
  Central Plaza 
  Heretaunga Street East Hospitality Precinct 
  Heretaunga Street West amenity Improvements 
  Railway Road Entrance Gateway 
  Civic Square 
  Karamu Road Precinct and Entrance Gateway 
  Pocket Parks 
  Laneways and Accessibility Connections 
  Street Upgrades 
 
All 23 projects with exception of the West Laneways and King Street Pocket 
Park  were  funded  by  the  Planning  and  Regulatory  team.    The  only  other 
project not funded was Karamu and Eastbourne Roads. A submission would 
be made to the Annual Plan and Long Term Plan for the funding. 
 
A  workshop  is  planned  for  1  April,  being  facilitated  by  Karl  Wixon  that 
members  of  the  Landmarks  Executive  were  invited  to  that  would  discuss 
these items in relation to the City Centre Strategy. 

A  Working  Group  would  be  established  to  assist  with  design  features  and 
implementation  of  individual  projects  contained  within  this  Plan  consistent 
with 
include 
representation  from:  Council,  the  Landmarks  Trust,  Mana  Whenua,  Arts  & 
Culture Sector, Hastings City Business Association, and Youth Council.  
 

framework  and  design  guide. 

  This  group  could 

the 

Councillor Schollum/Ms Brookfield  

A) 

B) 

That the report of the  Parks Planning and Development Manager 
titled “Update on City Centre Revitalisation Plan” dated 18/03/2019 
be received. 

That  the  Landmarks  Trust  have  two  members  of  the  Landmarks 
Executive to be part of the City Centre Working Group that will be 
established. 

CARRIED  

ADDITIONAL BUSINESS ITEMS  

There were no additional business items. 

 
6. 

 

 

 
 7. 
 
 
 
 
 
 

 

CG-14-13-00053  

7 

EXTRAORDINARY BUSINESS ITEMS  

There were no extraordinary business items. 

________________________ 

The meeting closed at 4.40pm 

 

 

Confirmed: 

 
 
 
 

Chairman: 

  
8. 

 
    

Date: 
 

 

