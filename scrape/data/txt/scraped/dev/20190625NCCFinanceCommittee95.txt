Minutes of Finance Committee - 25 June 2019
Finance Committee
Open Minutes
Meeting Date:
Tuesday
25 June 2019
Time:
3pm-3.06pm
Venue
Council
Chamber
Hawke's Bay Regional Council
159 Dalton Street
Napier
Present
Councillor Wise (In the Chair),
Acting Mayor White, Councillors  Boag, Brosnan, Dallimore, Hague,
Jeffery, McGrath, Price, Tapine, Taylor and Wright
In
Attendance
Chief
Executive, Director Corporate Services, Director Community Services, Director
City Services, Director City Strategy, Manager Communications and Marketing,
Manager People and Capability, MTG Director, Manager Community Strategies,
Senior Advisor Policy, Communications Specialist
Administration
Governance
Team
Finance Committee - 25 June
2019 - Open Minutes
Apologies
Councillors Boag / Hague
That the
apology from Mayor Dalton be accepted.
Carried
Conflicts of
interest
Nil
Public forum
Nil
Announcements by
the Acting Mayor
Nil
Announcements by
the Chairperson
Nil
Announcements by
the management
Nil
Confirmation of minutes
Councillor Price / Acting Mayor White
That the
Minutes of the meeting held on 14 May 2019 were taken as a true and accurate
record of the meeting.
Carried
Finance Committee - 25 June 2019 - Open
Minutes
Agenda
Items
1.    Napier
City Council Remuneration Policy
Type of Report:
Legal and
Operational
Legal Reference:
Local
Government Act 2002
Document ID:
757537
Reporting Officer/s &
Unit:
Sue Matkin, Manager People
& Capability
1.1   Purpose
of Report
The purpose of the Remuneration
Policy is to provide principles to guide the setting of remuneration for Napier
City Council employees.
At the Meeting
The Manager People and Capability spoke to the report,
noting that the Remuneration Policy (Policy) is reviewed every three years
and that the proposed changes are aimed at demonstrating a transparent and
fair approach to determining remuneration for Council staff. The guiding
principles outlined in the Policy are intended to assist staff and management
to understand how remuneration is set.
In response to questions from Councillors, the
following points were clarified:
·
The Policy does allow room for collective bargaining.
·
The Policy and remuneration framework have been developed in
conjunction with input from the Unions.
·
This work also follows a staff survey, with a 59% response
rate, that will lead into reviews in other areas also. Part of the staff
engagement was around pay.
·
The Policy does not preclude the Living Wage being considered
by Council in the future.
·
Reviews of the remuneration framework will be aligned with
reviews of the Policy.
Committee's recommendation
Councillors
Brosnan / Boag
The Finance Committee:
a.     Endorse
the Officers recommendation for Council to adopt the Remuneration Policy
2019-22
Carried
2.    Hawke's
Bay Museums Trust Statement of Intent 2019 - 2021
Type of Report:
Legal and
Operational
Legal Reference:
Local Government
Act 2002
Document ID:
761617
Reporting Officer/s &
Unit:
Chris Denby, Finance
Accountant
2.1   Purpose
of Report
To receive the final Statement of
Intent 2019 – 2021 for the Hawke’s Bay Museums Trust to Council
required for reporting requirements for Council-Controlled Organisations.
At the Meeting
The report was taken as read. There was no further
discussion on this item.
Committee's recommendation
Councillors
Taylor / Hague
The Finance Committee:
a.
Receive the final Hawke’s Bay Museums Trust Statement of Intent 2019
–2021
Carried
The meeting closed at 3.06pm.
Approved and adopted as a true and accurate record of the
meeting.
Chairperson .............................................................................................................................
Date of approval ......................................................................................................................