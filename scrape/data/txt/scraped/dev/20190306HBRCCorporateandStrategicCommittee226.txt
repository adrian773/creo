Minutes of Corporate and Strategic Committee - 6 March 2019
Unconfirmed
MINUTES OF A
meeting of the Corporate and Strategic Committee
Date:                          Wednesday 6 March 2019
Time:                          9.00am
Venue:
Council Chamber
Hawke's Bay Regional Council
159 Dalton Street
NAPIER
Present:                     N Kirton - Chairman
P Bailey
R Barker
P Beaven
T Belford
A J Dick
D Hewitt
M Paku
F Wilson
T Waaka
In Attendance:          J Palmer – Chief Executive
J Ellerm –
Group Manager Corporate Services
P Munro – Te
Pou Whakarae, Maori Partnerships
A Roets –
Governance Administration Assistant
M Collings –
Corporate Accountant
J Lawrence –
Group Manager Office of the CE & Chair
M Sharp –
Project Manager
G Hickton –
Chairman, HB Tourism
A Dundas –
General Manager, HB Tourism
1.       Welcome/Apologies/Notices
The Chair
welcomed everyone to the meeting and Pieri Munro offered a karakia.
The Chair advised
that Item 9 HB Tourism Update will be considered as the first item of business,
followed by Item 11.
Mike Paku advised
that he needs to leave at 2pm.
Resolution
C&S1/19    That the apologies for absence from Councillor Rex Graham and Mike
Mohi be accepted.
Wilson/Paku
CARRIED
2.       Conflict
of Interest Declarations
There were no conflict
of interest declarations.
3.       Confirmation
of Minutes of the Corporate and Strategic Committee meeting held on 12 December
2018
C&S2/19
Resolution
Minutes of the Corporate and Strategic Committee meeting held on
Wednesday, 12 December 2018, a copy having been circulated prior to the
meeting, were taken as read and confirmed as a true and correct record.
Beaven/Wilson
CARRIED
4.
Follow-ups from Previous Corporate and
Strategic Committee Meetings
It was noted that work is under way with
LINZ to determine the best financial treatment of the Tangoio conservation
estate in Council’s accounts, and a recommendation will be made to the
June C&S meeting.
The current Ethical Investment Policy was
reaffirmed by Council resolution in October 2018 on recommendation from the
Corporate and Strategic Committee.  It was suggested that the Fund
Managers present how their ethical investment policies align with
Council’s at the next C&S meeting in lieu of a workshop for
interested councillors as previously agreed.
C&S3/19
Resolution
That the Corporate
and Strategic Committee receives and notes the “Follow-ups
from Previous Corporate and Strategic Committee Meetings” report.
Barker/Wilson
CARRIED
5.
Call for Minor Items of Business Not on
the Agenda
That the Corporate and Strategic Committee accepts the following
“Minor Items of
Business Not on the Agenda” for discussion as Item 13.
Topic
Raised
by
1.
East
Coast Farming expo
Cr
F Wilson
9.
HB Tourism Update
The Chair introduced Mandy Sharp, HBRC Project Manager, before Annie
Dundas and George Hickton from Hawke’s Bay Tourism provided the update,
covering:
·
regional review of HBT’s funding model
and the national debate on tourism funding solutions is ongoing
·
January 2019 HB recorded 5% increase in visitor
spending from January 2018 – of $80 million and forecast year-end approximately
$648 (2% increase)
·
Seasonal growth since 2015 is +20% in Winter
and Spring, 16% in Autumn and 17% in Summer
·
Regional Visitor Strategy to deliver a
sustainable funding model and encompass marketing, infrastructure and event
attraction. First stage to draft the Terms of Reference; Second stage will
include a regional survey/workshops/ consultation; and third stage will focus
on report development, presentation and sign off.
·
The region’s Mayors and Chair indicated
strong support for an all of region approach to a sustainable funding model
and strategy.
·
Annie Dundas participating in a national group
to identify preferred funding models and will have a draft proposal for
discussion after attending a 5-day workshop next week. Potentially a national
solution applied regionally.
·
Bed taxes and how collection from paid
accommodation providers might happen
·
HBRC 2018-19 economic development rates
shifted more toward commercial sector, and no feedback received from the
sector to date
·
Initial interim results of the Living Wage survey
were outlined, with replies that 36% are paying the living wage, 8% intend
to, 23% can’t afford to and comments that some pay more than living
wage or pay to some staff and not others. Would like a broader group of
responses prior to creating the final report which will be circulated once
complete.
C&S4/19
Resolution
That the Corporate and
Strategic Committee receives and notes the “HB Tourism
Update” staff report.
Wilson/Belford
CARRIED
11.
Organisational Performance Update for
the Period 1 July 2018 to 31 January 2019
James Palmer introduced the item, the first edition of what is
intended to be monthly organisational performance reporting.
Discussions traversed:
·
The report combines content which typically
came to Council through separate reports such as Human Resources, monthly
activity reporting to Council, and risk management reporting
·
Seeking feedback on both the content and
formatting of the document as well as frequency
·
Potentially a dashboard showing organisational
performance trends against a set of agreed lead indicators (metrics) to be
presented at the beginning of the monthly council meetings
·
Suggestion to collapse (replace) the monthly
significant activities report into the organisation performance report
·
Summaries helpful
·
Preferences expressed for a thorough, detailed
report quarterly and brief ‘exception reporting’ monthly
·
Helpful to see a snapshot of what’s
coming up over the next month or so to keep ‘in the loop’
·
Suggested that a brief summary narrative and
key performance metrics be included in the monthly activities report to
council, plus summary financials
·
Suggested participating in
‘pre-meeting’ question and answer ‘forum’ to be
better prepared ahead of time
·
General agreement to receiving the detailed Organisational
Performance report on a quarterly basis with more focus on underperforming
areas and including a detailed breakdown of the Enforcement and Regulation
team’s activities, e.g. categorising pollution callouts
·
Capital budgets (Asset Management) to increase
the level of service of the Heretaunga Plains scheme to 1 in 500 has been
‘smoothed’ over 3 years. More work to be done to define the scope
and options for the full system including costs and timelines, and this work
will be brought to Council as the programme of work develops. A brief update
on where the project is at was requested for the next E&S meeting.
·
Pollution response and compliance budgets in
relation to cost recovery and non-chargeable work
·
$260,000 overspend on the Port Consultation is
the result of underestimating the resource required to carry out such an
extensive engagement process
·
Return on Investments will be considered
through the reforecasting exercise in April.
The
meeting adjourned at 10.30am and reconvened at 10.45am with Crs Alan Dick and
Debbie Hewitt absent
Further discussions traversed:
·
Recruitment expenditure may be slightly over
budget by the end of the financial year, with several hard to replace roles
still to be filled
·
Suggested that vacancies be shared with the
PSGEs
·
Expanded workforce and increased activity
outside during summer reflected in increased accidents /injuries reported
·
Sustainable homes funding available for septic
tanks and double-glazing
·
Significant increase in workforce since January
2018 is reflected in increased vehicle fleet numbers
·
Increased consideration of replacing vehicles with
EVs where possible
·
‘suitable to burn’ app has been
promoted to HB Fruitgrowers and other horticultural groups throughout the
region
Crs Debbie
Hewitt and Alan Dick re-joined the meeting at 11.00am
·
A report on the first year of the Erosion
Control Scheme will be provided to the June E&S meeting
·
The new bus ticketing system is expected to be
operational by the end of this year, and progress is reported regularly to
the Regional Transport Committee
·
Cash and cash equivalent lines in the
financials
·
Inputting data and establishing regimes for
field based compliance reporting workflows in IRIS has taken significant
amounts of staff time
C&S5/19
Resolution
That the Corporate and Strategic Committee receives
and notes of the “Operational Update for Seven
Months Ending 31 January 2019” and provides feedback to Council
staff.
Barker/Bailey
CARRIED
6.
Report and Recommendations from the
Finance Audit and Risk Sub-committee
Jessica Ellerm introduced the item outlining:
·
Rebekah Dinwoodie attended the meeting, her
first as Independent member of the sub-committee
·
received the 6 monthly Risk Management report
·
agreed the proposed scope for the follow-up Water
Management Internal Audit, which replaces the Business Continuity Plan internal
audit on the audit programme
·
financial Data Analytics Internal Audit report
was presented
·
Financial delegations are addressed as a
separate agenda item
·
Reviewed the 2017-18 Audit NZ Management
Report on the qualified Audit opinion received for the 2017-18 annual report
·
contract management policies and processes,
with review of the Policy and system to be reported to the May sub-committee
meeting
·
Treasury reporting will continue to develop
·
Cyber security subject of regular discussions
and staff training
C&S6/19
Resolutions
The Finance, Audit and Risk Sub-committee
recommends that the Corporate and Strategic Committee:
1.     Receives and notes the “Report and Recommendations
from the 12 February 2019 Finance, Audit and Risk Sub-committee
Meeting”
2.     Agrees that the decisions to be made are not significant under the
criteria contained in Council’s adopted Significance and Engagement
Policy, and that the Committee can exercise its discretion and make decisions
on this issue without conferring directly with the community or persons
likely to be affected by or have an interest in the decision.
Six Monthly
Report on Risk Assessment and Management
3.     Receives and notes the resolutions of the sub-committee,
confirming the robustness of Council’s risk management systems,
processes and practices
4.     Notes that a review of the Risk Assessment and Management
framework will be undertaken in the 2019-20 financial year.
Data
Analytics Internal Audit Report
5.     Receives and notes the resolutions of the sub-committee,
confirming that appropriate
action has been taken by management in response to the Data Analytics
Internal Audit recommendations.
2017-18 Audit
NZ Management Report
6.     Confirms the Finance, Audit and Risk Sub-committee’s
satisfaction that the “2017-18 Audit NZ Management Report”
is sufficient and that there are no outstanding issues of concern.
Reports Received
7.     Notes that the following reports were provided to the Finance
Audit and Risk Sub-committee.
7.1.   Proposed
Scope for Follow-up Water Management Internal Audit (resolved:
Confirms the
proposed Scope for the Follow-up Water Management Internal Audit)
7.2.   Treasury
Report
7.3.   Resource
Management Information System (IRIS) Implementation Update
7.4.   February
2019 Sub-Committee Work Programme Update.
Bailey/Kirton
CARRIED
7.
2019 Local Governance Statement Update
Joanne Lawrence introduced the item,
which was taken as read with the exception of a query in relation to the next
opportunity for consideration to be given to Council establishing Māori Wards or constituencies.
C&S7/19
Resolutions
1.      That the Corporate and Strategic Committee receives and notes the
“2019 Local Governance Statement Update” staff report.
2.      The Corporate and Strategic Committee recommends that Hawke’s
Bay Regional Council:
2.1.      Agrees that the decisions to be made are not significant under the
criteria contained in Council’s adopted Significance and Engagement
Policy, and that Council can exercise its discretion and make this decision
without conferring directly with the community.
2.2.      Accepts the “2019 Local Governance Statement” incorporating
amendments as agreed by the Corporate and Strategic Committee on 6 March
2019, for publication to the Hawke’s Bay Regional Council website.
Barker/Belford
CARRIED
8.
Financial Delegations
Manton Collings introduced the item with discussions highlighting:
·
Seeking feedback and guidance from Councillors
following amendments made to incorporate the feedback given at the
sub-committee meeting
·
Amounts delegated must be clearly stated as
“per transaction” or “per annum” or “individual
transactions”
·
Audit processes and controls check that all
approvals are within the Policy and correct internal delegations
·
Options for reporting on significant transactions
over $200,000 to Council and oversight versus delegation
·
Delegations for contingency fund spending to
be removed as agreed these will be subject to Council approval
C&S8/19
Resolutions
1.      That the Corporate and Strategic Committee:
1.1       receives and notes the “Financial Delegations”
staff report
1.2       provides feedback on the proposed financial delegation levels.
2.      The Corporate and Strategic Committee recommends that Council:
2.1.      Agrees that the decisions to be made are not significant under the
criteria contained in Council’s adopted Significance and Engagement
Policy, and that the Committee can exercise its discretion and make decisions
on this issue without conferring directly with the community or persons
likely to be affected by or have an interest in the decision.
2.2.      Adopts the Staff Financial
Delegations as amended to incorporate the feedback from the 6 March 2019
Corporate and Strategic Committee meeting.
Wilson/Barker
CARRIED
10.
Matters Arising from Audit NZ 2018-28 Long Term Plan Audits
Jessica Ellerm introduced the item, which was taken as read, with
discussions traversing:
·
LTP issues and approach mostly consistent
nationally
·
Appendix 6 sets out the differences between
the 2015 and 2018 financial forecasts and suggest this council’s
figures are somewhat commensurate with what is happening nation wide
·
LTP project review findings included the need
for sufficient time to be given to councillors to digest and consider
information prior to workshops as well as earlier engagement between the
project team and tangata whenua
C&S9/19
Resolution
That the Corporate and
Strategic Committee receives and notes the “Matters Arising from
Audit NZ 2018-28 Long Term Plan Audits” staff report and the
Auditor General’s report titled “Matters arising from our
audits of the 2018-28 long-term plans”.
Kirton/Barker
CARRIED
12.
Health and Safety Work Programme
Viv Moule outlined the progress of activities responding to the
Audit recommendations, with discussions highlighting:
·
draft Health and Safety Governance Charter
created for councillors
·
councillors will be provided with an
opportunity to spend time in the field to get a better understanding of some of
the health and safety risks staff experience
·
feedback provided on the language used in the
documents
·
The Implementation Plan addresses the high
risk issues
·
The draft Strategic Plan draws on strategic
health and safety planning guidelines and provides high level targets
·
Concerns raised in relation to the number of
incidents during January
·
re-drafted Governance Charter will be provided
to the March Council meeting for adoption and then to be signed by the
Council Chair and the Finance, Audit & Risk Sub-committee Chair.
C&S10/19
Resolution
That the Corporate and
Strategic Committee receives and notes the “Health and Safety
Work Programme” staff report.
Wilson/Barker
CARRIED
13.
Discussion of Minor Items Not on the
Agenda
Topic
Raised
by
East Coast Farming Innovation expo:
·     Expo runs from 6-7 March
2019
·     The Agenda for tomorrow
will cover Hill country planting – all welcome in Wairoa
Cr
F Wilson
Closure:
There
being no further business the Chairman declared the meeting closed at 12:03pm
on Wednesday 6 March 2019.
Signed
as a true and correct record.
DATE: ................................................               CHAIRMAN:
...............................................