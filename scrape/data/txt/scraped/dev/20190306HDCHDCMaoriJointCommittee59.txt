Minutes of HDC - Maori Joint Committee Meeting - 6 March 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
HDC - Māori Joint
Committee
Meeting
Date:
Wednesday, 6 March 2019
CG-14-14-00083                                                                         1
Minutes
of a Meeting of the HDC - Māori Joint
Committee
held
on 6 March 2019 at 1.00pm
Table of Contents
Item                                                                                    Page No.
1.         APOLOGIES   1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  2
4.         FORUM
ITEM : Te Awa o Te Atua Reserve  2
5.         Forum
Item - 3Waters Review Update  2
12.       Additional Business
Item    3
12A.    Review of Māori Participation
in Council Decision Making   3
6.         Resignation
Shayne Walker  4
7.         Omarunui
Landfill development planning   4
8.         Treaty
of Waitangi Workshops  5
9.         Te
Matā o Rongokako Track Update  5
10.       Te Ara Kahikatea -
Whakatū Arterial Name  6
11.       Te Reo Māori
Level One and level Two Courses  6
13.       Extraordinary
Business Items  7
CG-14-14-00083                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE HDC
- Māori Joint Committee
HELD
IN THE Council
Chamber, Ground Floor, Civic Administration Building, Lyndon Road East,
Hastings ON
Wednesday, 6 March 2019 AT 1.00pm
Present:                          Chair: Mr
Robin Hape
Councillors
Travers, Barber, (Deputy Chair) and Poulain
Messrs
Te Rangihau Gilbert, Ms Tracee Te Huia, Mrs Evelyn Ratima and Mr Ngaio Tiuka
Kaumatua: Jerry Hapuku
ALSO Present:              Chief Executive (Mr Nigel Bickle)
Pou
Ahurea Matua Principal Advisor: Relationships, Responsiveness and Heritage
– (Dr James Graham)
Group
Manager: Asset Manager (Mr C Thew)
Group
Manager: Planning and Regulatory (Mr J O’Shaughnessy)
Manager:
Democracy and Governance Services (Mrs J Evans)
Committee
Secretary (Mrs C Hunt)
AS REQUIRED:                  Te Muri Whaanga (Item 4)
Troy Brockbank, WPS Opus (Item 5)
Toni
Goodlass, Hawke’s Bay LASS (Item 5)
Peter
Paku (HDC:Tangata Whenua Wastewater Committee Member) (Item 5)
Kaumātua Jerry Hapuku gave the
opening Karakia.
1.        APOLOGIES
Councillor Barber/Councillor Travers
That
apologies for absence from Shayne Walker, Mayor Hazlehurst and Councillors
Lawson and O’Keefe be accepted.
CARRIED
An apology from Tania Kupa-Huata
HDC:Tangata Whenua Wastewater Committee Member was noted for Item 5.
2.         Conflicts
of Interest
There were no
declarations of conflicts of interest.
3.         Confirmation
of Minutes
Mr Hape/Councillor Travers
That the
minutes of the HDC - Māori Joint Committee Meeting held Wednesday
28 November 2018 be confirmed as a true and correct record and be adopted.
CARRIED
4.
FORUM ITEM : Te Awa o Te Atua Reserve
(Document 19/207)
Te Muri Whaanga presented the booklet
“Ngā Tikanga o Te Harakeke (Preparation and harvesting of Flax)
which contained contacts and tikanga protocols and a resource for Ngāti
Kahungunu weavers.
Ms Te Huia/Mrs Ratima
A)        That
the report of the Pou Ahurea Advisor: Responsiveness, Relationships
& Heritage titled “FORUM ITEM : Te Awa o Te Atua Reserve”
dated 6/03/2019 be received.
With
the reasons for this decision being that the objective of the decision will
contribute to meeting the current and future needs of communities for (good
quality local infrastructure)
·    The Kaupapa is to retain indigenous mātauranga of harakeke,
ensuring the product is well cared for and correct protocols are protected
are maintained.  If the protocols are kept a plentiful supply of
harakeke for present and future weavers will be confirmed.
CARRIED
Tangata Whenua Members (Evelyn Ratima,
Peter Paku and Tania Kupa-Huata) of the HDC: Tangata Whenua Joint Waste
Committee were invited to the presentation of the 3Waters Review Update.
5.
Forum Item - 3Waters Review Update
(Document 19/193)
Mr Brockbank
advised that Morrison Low were undertaking the 3Waters Review on behalf of
Hawke’s Bay Local Authority Shared Services (HBLASS) and he had been
engaged from WPS Opus to support Iwi Engagement.
The Government
was reviewing how to improve the management of drinking water, stormwater and
wastewater (3Waters) to better support New Zealand’s prosperity,
health, safety and environment.
Mr Brockbank
displayed a powerpoint presentation (CG-14-4-00084) on the background of
3Waters and the need to have cultural input.
Central
government’s major outcomes:
·
Safe, acceptable (taste,
colour, smell) and reliable drinking water
·
Better environmental
performance for our water services
·
Efficient, sustainable,
resilient and accountable water services
·
Achieving these aims in
ways our communities can afford their water bills
Mr Brockbank advised the first approach
for the review was to work with councils, next stage based on feedback how
best take on next engagement with wider communities.
He sought feedback from the Committee on
what was regionally important to Māori and  key issues for iwi and
Māori.
Points highlighted included:
·
Partnership – what need as Māori
equity and delivery in community
·
Quality of water – issues around
chlorine
·
Affordability – do not want water
charges for the district.
·
Report on assets and principles shared
·
Terminology – 3 waters infrastructure
needs to be clear.
·
Primary concern around regulation
·
Regional option seems logical
·
No water metering
·
Māori values
·
Māori dimension around water –
resourcing and flowback to iwi and hapū
·
Cannot view Māori aspirations as just
culture
·
Important there is Treaty context within
cultural aspect
·
Each water is a wānanga itself
·
Maori Committee members noted that engagement
with the Maori Joint Committee was not iwi engagement and that direct contact
with groups needed to be made.
·
The Committee was not the forum for engaging
with Maori.
·
Intergenerational wealth – is not money
Ms Te Huia/Councillor Poulain
A)        That
the report of the Pou Ahurea Matua - Principal Advisor: Relationships,
Responsiveness and Heritage titled “Forum Item - 3Waters Review
Update” dated 6/03/2019 be received.
CARRIED
With the agreement of the meeting the
Additional Business Item 12 was taken out of order.
12.
ADDITIONAL BUSINESS
ITEM (Document 19/175)
12A.
Review of Māori Participation in Council
Decision Making
(Document 19/220)
The Manager: Democracy and Governance
Services, Mrs Evans updated the Committee on the outcome of the workshop held
with Council on arrangements for Māori participation in decision making. The HDC: Māori Joint Committee
full Council workshop agreed in principle on the appointment of tangata
whenua members to Council’s standing committees.
Appointments made would be until the
triennial elections in October 2019.   Any decision for future
arrangements would be up to the incoming Mayor and Councillors following the
election.
Mr Hape/Councillor
Barber
A)    That
the report of the Manager: Democracy and Governance Services titled “Review
of Māori Participation in Council Decision Making” dated 6/03/2019
be received.
B)    That
the HDC : Māori Joint Committee recommend to Council that it agrees to
the appointments of tangata whenua members, with voting rights to the
Hastings District Council Standing Committees.
C)    That the HDC : Māori Joint Committee recommend to Council
that the  following tangata whenua members be appointed to the following
Standing Committees from 28 March 2019:
Community
Development
Evelyn Ratima
Finance and
Risk
Ngaio Tiuka
Strategy, Planning and
Partnerships
Tracee Te Huia
Works and
Services
Te Rangihau Gilbert
D)   That the
HDC: Māori Joint Committee review the current Terms of Reference and
membership for the 2019-2022 triennium
With the reasons for this decision being that the objective of the
decision will contribute to meeting the current and future needs of
communities for local public services in a way that is most cost-effective
for households and business by improving
Māori engagement in the Council’s governance and decision making
processes.
CARRIED
6.
Resignation Shayne Walker
(Document 19/152)
Councillor Barber/Ms Te Huia
A)        That
the report of the Pou Ahurea Matua - Principal Advisor: Relationships,
Responsiveness and Heritage titled “Resignation Shayne Walker”
dated 6/03/2019 be received.
B)        That
the HDC : Māori Joint Committee accept the resignation of Shayne Walker.
CARRIED
7.
Omarunui Landfill development planning
(Document 19/152)
The Group Manager: Asset Management, Mr
Thew sought guidance from the Committee on the future
planning and consultation in regards to the development of the Omarunui
landfill.
It was noted that consultation also be
undertaken with Mana Ahuriri and Marae at Waiohiki, Ahuriri, Ōmahu and Moteo
Pā
Mr Tiuka/Councillor Barber
A)        That
the report of the Group Manager: Asset Management titled “Omarunui
Landfill development planning” dated 6/03/2019 be
received.
CARRIED
8.
Treaty of Waitangi Workshops
(Document 19/174)
The Pou Ahurea Matua – Principal Advisor:
Relationships, Responsiveness and Heritage, Dr Graham updated
the Committee on the cultural development programme.  A second Treaty of
Waitangi Workshop would be held in May 2019 for Councillors and the
Leadership Team and a workshop was also planned for Council senior or middle
managers.
Ms Te Huia/Mr Gilbert
A)        That
the report of the Pou Ahurea Matua - Principal Advisor: Relationships,
Responsiveness and Heritage titled “Treaty of Waitangi Workshops
” dated 6/03/2019 be received for information.
With
the reason for this recommendation being that the objective of the decision
will contribute to meeting the current and future needs of communities and
the staff at Council in a way that ensures that the Treaty of Waitangi is
applied across all facets of Local Government; and,
·
That the objective of the decision will
contribute to effective local decision making and action by, and on behalf of
communities.
CARRIED
9.
Te Matā o Rongokako Track Update
(Document 19/175)
The Pou Ahurea Matua – Principal Advisor:
Relationships, Responsiveness and Heritage, Dr Graham
advised that 11, 12 and 13 June 2019 had been set for the Hearing.  A
tentative timeline (CG-14-14-00088) for the hearing would be circulated to
members.
The Group Manager: Planning and
Regulatory Services advised that the Commissioners appointed to the Hearing
were Rauru Kirikiri and Paul Cooney.
Councillor Barber/Mrs Ratima
A)        That
the report of the Pou Ahurea Matua - Principal Advisor: Relationships,
Responsiveness and Heritage titled “Te Matā o Rongokako
Track Update” dated 6/03/2019 be received.
With the
reasons for this decision being that the objective of the decision will
contribute to effective local decision making and action by, and on behalf of
communities, and the decision will contribute to meeting the current and
future needs of communities by:
i)          Ensuring that committee is properly informed of the progress to
date on the full track remediation on Te Matā o Rongokako and the
proposed process that will follow.
CARRIED
10.
Te Ara Kahikatea - Whakatū Arterial Name
(Document 19/176)
The Pou Ahurea Matua –
Principal Advisor: Relationships, Responsiveness and Heritage, Dr Graham updated the Committee on the approved
name of the new arterial route constructed between State Highway 2 North and
Pākōwhai Road, which was officially opened on Saturday February 23
and named Te Ara Kahikatea.
Ms Te Huia/Councillor O'Keefe
A)        That
the report of the Pou Ahurea Matua - Principal Advisor: Relationships,
Responsiveness and Heritage titled “Te Ara Kahikatea -
Whakatū Arterial Name” dated 6/03/2019 be
received.
With the
reasons for this decision being that the objective of the decision will
contribute to good quality local infrastructure in
a way that is most cost-effective for households and business by:
i)
Providing the name Te Ara Kahikatea to
this new road clearly resonates with a sense of place, a sense of history,
and the empowerment of the wider community on a collaborative journey that
has linked Council, mana whenua and other key stakeholders throughout the
project including the enablement of both business’ and households to
better access and travel throughout this area.
ii)
CARRIED
11.
Te Reo Māori Level One and level Two Courses
(Document 19/177)
Ms Te Huia/Mr Gilbert
A)        That
the report of the Pou Ahurea Matua - Principal Advisor: Relationships,
Responsiveness and Heritage titled “Te Reo Māori Level One
and level Two Courses” dated 6/03/2019 be received
for information.
With the
reason for this recommendation being that the objective of the decision will
contribute to meeting the current and future needs of communities and the
staff at Council in a way to ensure that te reo Māori is preserved as a
taonga of great value to Aotearoa New Zealand.
CARRIED
13.       Extraordinary
Business Items
There were no extraordinary business items.
________________________
Karakia:  Te Rangihau Gilbert
The meeting closed at 4.10pm
Confirmed:
Chairman:
Date: