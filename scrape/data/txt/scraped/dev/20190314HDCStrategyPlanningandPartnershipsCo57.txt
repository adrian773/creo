Minutes of Strategy Planning and Partnerships Committee Meeting - 14
March 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Strategy Planning and
Partnerships Committee
Meeting
Date:
Thursday, 14 March 2019
CG-14-72-00029                                                                         1
Minutes
of a Meeting of the Strategy Planning and
Partnerships Committee held on 14 March 2019 at 1.00pm
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  1
4.         Presentation
by Biodiversity Hawke's Bay  2
5.         Environmental
Policy - Workstream Update  2
6.         Additional
Business Items  3
7.         Extraordinary
Business Items  3
CG-14-72-00029                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE Strategy
Planning and Partnerships Committee HELD IN THE Council Chamber, Ground Floor,
Civic Administration Building, Lyndon Road East, Hastings ON  Thursday,
14 March 2019 AT 1.00pm
Present:                          Councillor Lyons (Chair)
Mayor Hazlehurst
Councillors
Barber (Deputy Chair), Dixon, Harvey, Heaps, Kerr, Lawson, Nixon,
O’Keefe, Poulain, Redstone, Schollum, Travers and Watkins
ALSO
Present:              Group Manager:  Planning and Regulatory
Services (Mr J O’Shaughnessy)
Environmental
Policy Manager (Mr R Wallis)
Manager:
Democracy and Governance Services (Mrs J Evans)
Committee
Secretary (Mrs C Hunt)
AS REQUIRED:                  Charles Daughtery, Christine Cheyne and Mike Halliday
(Biodiversity Hawke’s Bay)
Walter
Breustedt (ECO Management Group Limited)
1.         Apologies
Councillor
Lyons/Councillor Watkins
That
apologies for lateness from Councillors Dixon, Heaps, Kerr, O’Keefe and
Redstone be accepted.
CARRIED
2.         Conflicts
of Interest
There were no
declarations of conflicts of interest.
3.         Confirmation
of Minutes
Councillor Watkins/Councillor Travers
That the
minutes of the Strategy Planning and Partnerships Committee Meeting held Thursday
15 November 2018 be confirmed as a true and correct record and be adopted.
CARRIED
Councillors
Heaps, Kerr and Redstone joined the meeting at 1.07pm
4.
Presentation by Biodiversity Hawke's Bay
(Document 19/208)
Councillor Heaps, Portfolio Lead introduced
members of Biodiversity Hawke’s Bay and ECO Management Limited who
presented to the Committee.
Mr Daughtery, Biodiversity
Hawke’s Bay displayed a powerpoint presentation (CG-14-72-00027) providing
an overview on an
investment in Biodiversity being an investment in Agriculture; Health, Tourism;
Economic Growth and a Sustainable Future.
Councillors Dixon and O’Keefe
joined the meeting at 1.20pm.
Mr Daughtery advised that Biodiversity
Hawke’s Bay was seeking financial
contribution from Council  for Endowment – Contributions; Operating Costs (Contributions) and Tangible,
Active Partnerships (Joint funding applications, Public engagement and
Advocacy)
Mr Breustedt displayed a powerpoint
presentation (CG-14-72-00026) providing an overview of steps to implement the
Hawke’s Bay Biodiversity Strategy.
Mayor Hazlehurst withdrew from the meeting at 1.50pm
and rejoined the meeting at 2.00pm
Councillor Dixon/Councillor Redstone
A)        That
the report of the Project Advisor titled “Presentation by
Biodiversity Hawke's Bay” dated 14/03/2019 be
received.
CARRIED
5.
Environmental Policy - Workstream Update
(Document 19/196)
The Environmental Policy Manager, Mr
Wallis  advised that paragraph 3.7 Wahi Taonga had been an amended as
follows:
“As reported last November the
issue of the Te Mata track has had a direct impact on the Wāhi Taonga
project. Iwi and hapu have clearly stated that it is inappropriate to move
forward on future considerations for Te Mata te Tipuna until such time as the
track is remediated. The way forward for this issue will help to provide
clear direction for the wider wahi taonga project.   The resource
consent to remediate the remaining section of the track has now been
notified. Once this is completed discussions can begin with manawhenua on the
most appropriate way forward on future access options for Te Mata te Tipuna.
While the resource consent process is
proceeding the Te Tira Toitu te Whenua subcommittee can begin to look at the
process of addressing the recommendations that arose out of the Cultural
Assessment Report including how the district plan might recognise a kaupapa Māori
methodology for establishing policies and resource consent applications”.
Councillor Watkins/Councillor Lawson
A)        That
the report of the Environmental Policy Manager titled “Environmental
Policy - Workstream Update” dated 14/03/2019 be
received.
B)        That the Committee endorsed the submission lodged by the Council
on Variation 5 to ensure that entrances and accessways and garaging and
service areas associated with the above ground residential activity can be
provided at ground level as a permitted activity.
With the
reasons for this decision being that the objective of the decision will
contribute to meeting the current and future needs of communities for
performance of regulatory functions in a way that is most cost-effective for
households and business by:
i)          Setting priorities for the policy work that is required for the
Council to meet its regulatory functions and deliver outcomes to the
community in a timely manner.
CARRIED
6.         Additional Business Items
There were no additional business items.
7.         Extraordinary
Business Items
There were no extraordinary business items.
________________________
The meeting closed at 2.15pm
Confirmed:
Chairman:
Date: