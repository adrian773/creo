EXTRAORDINARY FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

11 JUNE 2019 

  MINUTES OF WAIROA DISTRICT COUNCIL 

EXTRAORDINARY FINANCE, AUDIT & RISK COMMITTEE MEETING 

HELD AT THE COUNCIL CHAMBER, WAIROA DISTRICT COUNCIL, CORONATION SQUARE, WAIROA 

ON TUESDAY, 11 JUNE 2019 AT 11.00AM 

 

PRESENT: 

His  Worship  the  Mayor  Craig  Little  (Mayor),  Cr  Denise  Eaglesome-Karekare 
(Deputy Mayor), Cr Jeremy Harker, Mr Philip Jones 

IN ATTENDANCE:   Clr  Charlie  Lambert,  Clr  Mike  Bird  ,S  May,  (Tumu  Whakarae  Chief  Executive 
Officer), S Heath (Pouwhakarae – Hua Pūmau Hapori/Ratonga Group Manager 
Community  Assets  and  Services),  G  Borg  Pouwhakarae  –  Pūtea/Tautāwhi 
Rangapū  Group  Manager  Finance  and  Corporate  Support),  K  Tuapawa 
(Pouwhakarae  –  Pārongo/Wheako  Kiritaki  Group  Manager  Information  and 
Customer  Experience),  G  Waikawa  (Kaiurungi  Mana  Arahi  –  Governance 
Officer) 

1 

KARAKIA 

Karakia was given by Clr C Lambert 

2 

APOLOGIES FOR ABSENCE  

None. 

3 

DECLARATIONS OF CONFLICT OF INTEREST 

None. 

4 

PUBLIC PARTICIPATION 

None. 

5 

GENERAL ITEMS 

5.1 

FINANCIAL PERFORMANCE TO 30 APRIL 2019 

COMMITTEE RESOLUTION  2019/60  

Moved: 
Seconded:  Cr Jeremy Harker 

Cr Denise Eaglesome-Karekare 

That the Committee receive the report. 

Points discussed: 

Absence of capital expenditure. 

Rates by activity. 

CARRIED 

Capex by activity together with indicative funding of capital expenditure – 30 April as a minimum - 
30 May as ideal. 

Page 1 

EXTRAORDINARY FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

11 JUNE 2019 

P Jones impressed on forecast. 

The date of the next FAR Committee will be changed from 25 June 2019 to Tuesday, 2 July 2019. 

 

5.2 

ADOPTION OF THE ANNUAL PLAN 2019/20 

COMMITTEE RESOLUTION  2019/61  

Moved: 
Seconded:  Cr Jeremy Harker 

Cr Denise Eaglesome-Karekare 

That: 

1. 

The Committee receives the Annual Plan 2019/20, attached as Appendix 1 and recommends 
to Council that it be adopted with minor changes noting that Council did not need to consult 
as stated in Section 95 (2A) Local Government Act 2002 as it is of the opinion there are no 
significant or material differences between Year 2 of Council’s LTP and 2019/20 Annual Plan. 

2. 

The committee notes while there are changes in the following areas; capital subsidies, other 
operating expenses, asset purchases and construction and the use of reserves but as these 
changes  did  not  impact  Council’s  financial  strategy  or  the  limits  set  under  the  financial 
prudent regulations and confirms the need not to consult. 

CARRIED 

Points raised: 

Funding impact statements against each activity reconciling back to annual plan at a later date. 

 

 
Meeting was closed with a karakia by Clr Charlie Lambert. 
 
The Meeting closed at 11.49. 

 

The  minutes  of  this  meeting  were  confirmed  at  the  Extraordinary  Finance,  Audit  &  Risk 
Committee Meeting held on 25 June 2019. 

 

 

 

 

................................................... 

CHAIRPERSON 

 

Page 2 

