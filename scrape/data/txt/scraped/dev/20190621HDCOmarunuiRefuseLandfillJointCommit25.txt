Minutes of Omarunui Refuse Landfill Joint Committee Meeting - 21 June
2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Omarunui Refuse Landfill
Joint Committee
Meeting
Date:
Friday, 21 June 2019
CG-14-27-00047                                                                         1
Minutes
of a Meeting of the Omarunui Refuse Landfill
Joint Committee
held
on 21 June 2019 at 1.00pm
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  1
4.         Health
and Safety Report - 6 monthly  1
5.         Nine
Month Activity Report 2
6.         Additional
Business Items  4
7.         Extraordinary
Business Items  4
CG-14-27-00047                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE Omarunui
Refuse Landfill Joint Committee HELD IN THE Council Chamber, Ground Floor,
Civic Administration Building, Lyndon Road East, Hastings ON
Friday, 21 June 2019 AT 1.00pm
Present:                          Councillor Heaps
Councillors
Lawson and Redstone (HDC)
Councillors
Dallimore (Deputy Chair) and Tapine (NCC)
IN ATTENDANCE:             Group Manager: Asset Management (Mr C Thew)
Waste and Data Services Manager (Mr M Jarvis)
Waste Minimisation Officer (Mr A Atkins)
Solid Waste Engineer (Mr P Doolan)
Waste Minimisation Lead (Mr R van Veldhuizen) (NCC)
Management Accountant (Mr J Tieman)
Health
and Safety Manager (Ms J Kuzman)
Senior Health and Safety Co-ordinator (Mr N Bass)
Committee
Secretary (Mrs C Hunt)
1.         Apologies
Councillor
Heaps/Councillor Lawson
That an apology
for absence from Councillor Nixon be accepted.
CARRIED
2.         Conflicts
of Interest
There were no declarations
of conflicts of interest.
3.         Confirmation
of Minutes
Councillor Heaps/Councillor Redstone
That the minutes of the Omarunui
Refuse Landfill Joint Committee Meeting held Friday 7 December 2018, including
minutes while the public were excluded be confirmed as a true and correct
record and be adopted.
CARRIED
4.
Health and Safety Report - 6 monthly
(Document 18/1110)
The Senior
Health & Safety Co-ordinator Ms Bass presented
her report to the Committee regarding the Health and Safety Management at the
Omarunui Refuse Landfill.
Councillor Heaps/Councillor Tapine
A)        That the
report of the Senior Health & Safety Co-ordinator titled “Health
and Safety Report - 6 monthly” dated 21/06/2019 be
received.
CARRIED
5.
Nine Month Activity Report
(Document 19/545)
The Waste and Data Services Manager, Mr
Jarvis presented his report updating the Omarunui Joint Refuse Landfill
Committee on landfill activities for the period ending March 2019.
Development Update for Area B & C - Circulated at the meeting (CG-14-27-00049) was a copy of the
Feedback Form that had been distributed to residents on the future
development at the Landfill.
A meeting had been held at the Waiohiki
Marae on 23 May 2019 with officers from the Hastings District and Napier City
Councils attending to respond to questions.  It was agreed that Council
arrange for members of the Ngāti Pārau Hapū Trust to visit the
Omarunui Landfill by 30 August 2019.
Some issues raised by Iwi were in response
to the Waste Management and Minimisation Plan submission and not Landfill
related.
Development Update for Area A & D - A small amount of work relating to earth works and temporary
capping had been undertaken.  The development budget for the full year was
$2,424,958, but it was unlikely that this amount would be spent as the
extension of Area D liner was now programmed for the next financial
year.
Landfill Gas – The gas generating engine now has full capacity of gas.
Additional vertical gas wells have been constructed and integrated into the
gas collection network.  This has resulted in more gas being captured
and further reduces the opportunity of odour escaping into the atmosphere
from the compacted rubbish.
Operational and Maintenance Work – An additional 20 irrigation pods were installed during
the summer  which has enabled the landfill to
dispose of a greater volume of leachate by way of its spray irrigation
consent.
The leachate pond was constructed in 1987
and a leak detection company were engaged to assess how well the liner was
performing.  The company found a couple of anomalies during testing and staff
are now considering an option to replace or reline the pond to ensure its long
term integrity.  Funds are available for this so there would be no
impact on the annual budget.
Health and Safety and Human Resources – With the new legislation some changes were being
implemented to ensure staff had a half hour break away from the kiosk.
A trial closing the landfill
gates for 30 minutes between 12.30pm and 1.00pm has been instigated to gauge
the impact of this change.  To date feedback from the Landfill users has
indicated minimal issue with this, however it will not be known for sure
until the trial is completed.
If operators are running late and cannot make it by
4.00pm the gates will remain open to 4.20pm to accommodate disruption.
Efficiencies - The landfill continue to look for operational and developmental
efficiencies.
Plant Replacement – There have been issues with the Caterpillar Compactor and
preparation of tender documents are underway so that a new machine can be
purchased in the New Year.  There were funds in the Plant Account to purchase
the machinery.
There was the option to lease or purchase
the machinery.  Both options would be included in the tender
documents.  Consideration would also be given to retaining the existing
compactor as a back-up machine.
Plant Management Contract –  M.W. Lissette are the current Omarunui Plant
Management Contractor and officers would soon be assessing operational needs
and intend to re-tender this work early next financial year.
Hawke’s Bay Regional Council
Consent Compliance – A “full compliance”
was awarded by the Hawke’s Bay Regional Council on the annual
monitoring report.
Financial Summary - Mr Tieman referred to the financial summary (CG-14-27-00044)
advising that the net surplus from operations as at 31 March 2019 was $808,650
above budget.  The surplus was partly a result of higher than expected
special waste volumes and the programming of expenditure work.
Tonnages are currently tracking up on last year’s actual
total (66,295 tonnes versus 64,257 tonnes).
Expenditure is $47,061 over budget due to
Emission Trading Scheme with higher tonnes, leachate treatment improvement
and external plant hire.
Councillor Tapine/Councillor Redstone
That the report of the Waste and Data
Services Manager, titled “Nine Month Activity Report” dated 21
June 2019, be received.
CARRIED
6.         Additional Business Items
There were no additional business items.
7.         Extraordinary
Business Items
There were no extraordinary business items.
________________________
The meeting closed at 2.00pm
Confirmed:
Chairman:
Date: