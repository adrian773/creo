Minutes of Regulatory Committee - 19 February 2019
Regulatory Committee
Open Minutes
Meeting Date:
Tuesday
19 February 2019
Time:
4.32pm-4.43pm
Venue
Large
Exhibition Hall
Napier Conference Centre
Napier War Memorial Centre
Marine Parade
Napier
Present
Councillor Jeffery (In
the Chair), Mayor Dalton, Councillors Boag, Brosnan, Dallimore, Hague,
McGrath, Price, Tapine, Taylor, White, Wise and Wright
In
Attendance
Director
Infrastructure Services, Director Community Services, Director City Services,
Director City Strategy, Manager Communications and Marketing, Chief Financial
Officer, Strategic Planning Lead
Stephanie
Kennard, Napier City Business Inc.
Administration
Governance
Team
Regulatory Committee - 19
February 2019 - Open Minutes
Apologies
Nil
Conflicts of
interest
Nil
Public forum
Nil
Announcements by
the Mayor
Nil
Announcements by
the Chairperson
Nil
Announcements by
the management
Nil
Confirmation of minutes
Councillors Jeffery / Taylor
That the
Minutes of the meeting held on 13 November 2018 were taken as a true and
accurate record of the meeting.
Carried
Regulatory Committee - 19
February 2019 - Open Minutes
Agenda
Items
1.    NCC
and Napier City Business Inc Events 2019
Type of Report:
Procedural
Legal Reference:
Local
Government Act 2002
Document ID:
694616
Reporting Officer/s &
Unit:
Fleur Lincoln, Strategic
Planning Lead
1.1   Purpose
of Report
The purpose of this report is to
obtain a resolution of Council to allow trading in a public place to occur at
Napier City Council and Napier City Business Inc events in Napier city until
the end of October 2019.
At the Meeting
The Strategic Planning Lead spoke to the report. In
response to questions from Councillors she confirmed that in some cases
‘beverages’ will include alcoholic beverages; however, events
held within public spaces would be required to meet the Joint Alcohol
Strategy and the Alcohol Decision Matrix. In some cases, establishments will
have their own liquor licensing. To provide Council with some certainty in
this area the following points were noted in addition to those already in the
officer’s report:
1.2
Background Summary
Criteria
9 should include: ‘If alcohol is to be sold at the event, the usual
liquor licensing requirements also apply.’
A new
Criteria 10 should be added as follows: ‘Any sale of alcohol must be
consistent with the Joint Alcohol Strategy and the Alcohol Decision Matrix.’
It was also clarified that
consideration had been given as to how to minimise waste from events or
whether to host a waste-free event. Officers confirmed that how waste is to
be addressed would be included in future reports to Council regarding events,
but noted that while fully waste free events are the preference, Council
cannot guarantee this will be achieved for all events.
In response to a
Councillor’s request to put monitoring and feedback in place after
events, the Director City Strategy noted that this decision is required to
enable the approval process only. Mechanisms are already in place to monitor
events and provide feedback to Council.
For clarity, an amendment was
proposed to specify alcoholic beverages in the motion.
Officer’s Recommendation
That the Regulatory Committee:
a.     Approve
the sale of food and beverages to the public on public land within the city
centre as part events held by either Napier City Council or Napier City
Business Inc until the 31st October 2019.
Substantive motion
Committee's recommendation
Councillors
Boag / Tapine
That the Regulatory Committee:
a.     Approve
the sale of food and beverages, including alcoholic beverages, to the public
on public land within the city centre as part events held by either Napier
City Council or Napier City Business Inc until the 31st October
2019.
Carried
The meeting closed at 4.43pm.
Approved and adopted as a true and accurate record of the
meeting.
Chairperson .............................................................................................................................
Date of approval ......................................................................................................................