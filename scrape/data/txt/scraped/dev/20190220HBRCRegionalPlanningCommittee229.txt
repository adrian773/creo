Minutes of Regional Planning Committee - 20 February 2019
MINUTES OF A
meeting of the Regional Planning Committee
Date:                          Wednesday 20 February 2019
Time:                          10.00am
Venue:
Council Chamber
Hawke's Bay Regional Council
159 Dalton Street
NAPIER
Present:                     T Waaka – Co Chair Ngāti Pāhauwera Development & Tiaki
Trust
Cr R Graham –
Co Chair
Cr P Bailey
Cr R Barker –
Deputy Co Chair
Cr P Beaven
Cr T Belford
Cr A J Dick
Cr D Hewitt
N Kirikiri – Te
Toi Kura O Waikaremoana
Cr N Kirton
J Nelson-Smith
– Heretaunga Tamatea Settlement Trust
P Paku – Heretaunga
Tamatea Settlement Trust
A Tapine – Tātau
Tātau o Te Wairoa Trust
Cr F Wilson
In Attendance:            M Mohi – Chairman Maori Committee
J Palmer –
Chief Executive
J Lawrence –
Group Manager External Relations
T Skerman –
Group Manager Strategic Planning
I Maxwell –
Group Manager Integrated Catchment Management
C Dolley –
Group Manager Regional Assets
P Munro – Te
Pou Whakarae – Māori Partnerships
G Ide – Principal Advisor Strategic Planning
C Edmonds –
Manager Policy & Planning
B Brough – Brough
Resource Management Ltd
R Ellison –
Wakataurua Ltd
D Meredith –
Senior Planner
A Roets –
Governance Administration Assistant
1.       Welcome/Apologies/Notices
The Chairman, Mr
Rex Graham welcomed everyone to the meeting and Pieri Munro offered a karakia,
acknowledging those present.
Resolution
RPC1/19      That the apologies for absence from Matiu
Heperi-Northcroft (Ngāti
Tūwharetoa Hapū), Joinella Maihi-Carroll (Mana Ahuriri Trust), Karauna Brown (Te Kopere o te Iwi Hineuru), and Tania Hopmans (Maungaharuru Tangitū Trust) be accepted.
Barker/Tapine
CARRIED
2.       Conflict
of Interest Declarations
There were no conflict
of interest declarations.
3.       Confirmation of Minutes of the
Regional Planning Committee meeting held on 12 December 2018
RPC2/19
Resolution
Minutes of the Regional Planning Committee meeting held on
Wednesday, 12 December 2018, a copy having been circulated prior to the
meeting, were taken as read and confirmed as a true and correct record.
Nelson-Smith/Waaka
CARRIED
4.
Follow-ups from Previous Regional
Planning Committee Meetings
There was a request for an update on
feedlots across the region and James Palmer advised that a media release on
feedlot compliance, reminding landowners of their obligations, will be made
on this topic heading into winter. In addition, an invitation to visit a
Raupunga operation was extended by Toro Waaka.
In relation to the TANK Plan Change (PC9)
Pre-Notification Planning Pathway, Tom Skerman advised a request from one of
the territorial local authorities to extend the pre-notification consultation
by 2 weeks is under consideration and staff will inform all parties of
whether the extension is granted.
RPC3/19
Resolution
That the Regional
Planning Committee receives the report “Follow-up Items from
Previous Meetings”.
Bailey/Beaven
CARRIED
5.
Call for Minor Items of Business Not on
the Agenda
Reason
for Report
That Regional Planning Committee
accepts the following “Minor Items of Business Not on the Agenda” for discussion as Item 10:
Topic
Raised by
1.
Update on when HBRC plans will be amended to give
effect to the National Environmental Standard for Plantation Forestry
T Waaka
2.
HBRC Youth Council
Cr P Bailey
3.
Eels at Puketapu
Cr P Bailey
4.
Waitangi Day celebrations
Cr R Graham
6.
Potential Tukituki Plan Change
Mr Tom Skerman introduced the item.
Councillor Hewitt moved a motion that the
item be deferred to the RPC’s next meeting on 17 April as the matter
has not been discussed by the Tukituki Taskforce. Seconded by Councillor
Dick.
Debate on the motion traversed:
·
Cannot expect unanimous support and suggestion
that there needs to be an opportunity for the Taskforce and the Community to
discuss the outcomes before any decisions are made – potentially at the
next Taskforce Group meeting 12 March, which Tom Kay has been invited
to, acknowledging that the Forest & Bird (not the only party opposed)
needs to form part of the solution. A letter from the Taskforce to the Forest
& Bird National Office will be shared with the committee members.
·
In response to a query about whether, if the
report was deferred to the 17 April meeting, it is possible to have a Plan
Change in effect for the 2019-20 summer, Tom Skerman advised that timeframes
are incredibly tight, a deferring until April would make them even tighter.
·
Stated that committee members need to
understand the implications and consequences that the deferral of this item
by one/two months will have.
RPC4/19
Resolutions
That the Regional Planning Committee defers consideration of the Potential
Tukituki plan change item to its meeting scheduled for 17 April 2019 to
give the community, through the Tukituki Taskforce, an opportunity to
consider it.
Hewitt/Dick
For: Dick, Hewitt, Paku, Nelson-Smith, Wilson,
Kirton, Kirikiri, Tapine, Waaka (9 /65%)
Against: Barker, Bailey, Belford, Beaven, Graham (5
/35%)
RPC threshold for at least 80% agreement of
members in attendance was not met, and therefore the motion was LOST
After loss of the motion to defer the
item to the April meeting, consideration of the item commenced, with
discussions covering:
·
Forest & Bird frustrations are with
HBRC’s management of the process and not with the CHB community
·
Forest & Bird national office has
submitted formal confirmation of the CHB branch opposition to the proposed
plan change and unhappiness with the Tukituki Taskforce advocating for policy
change when the terms of reference for the group clearly state that it is not
a policy advocacy group.
·
The terms of reference for Tukituki Taskforce
also contains provisions similar to the TANK stakeholder group, to give
people an opportunity to link back to their organisations with discussions
and decisions taken at meetings
·
Initial proposal to extend minimum flow
effective dates for 2 years (summers) as staff view that 2 years is the
minimum time required to allow the community to meet the objectives
·
Of the 55 responses to the request for
feedback received, 21 generally supported the proposal to defer, 29
generally opposed the proposal and 5 took a neutral position
·
Request for the feedback was part of the fast
fail approach, where if significant opposition encountered at the outset then
plan change process does not progress
·
Approximately 50 surface water consents are
affected by the low flow limits, with opinion expressed that this group wants
the deferral and not the community. That view was disputed with statement
that others affected by the minimum flow limits include Medallion Meats which
hires 120 staff, 65% of which are Māori, the consent is not for
irrigation, nor held by a grower.
·
The view expressed by staff for example is
that a streamlined plan change process would have to clear a very high bar
with significant support for the current Minister for the Environment to look
favourably on agreeing to use a streamlined planning pathway.
·
Indications are that the current Government
and Minister do not generally support use of the streamlined Planning pathway
as that was something the previous government had introduced.
·
Request for a copy of the Taskforce
Group’s Water Conservation Plan to be circulated to the Committee
members
·
James Palmer explained that if the motion is
passed, staff would bring further advice back to the Committee on whether the
standard or streamlined planning track is the best option to achieve desired
outcomes
·
opposing views of whether to proceed with a
plan change were expressed during debate on the recommendations
Recommendations
1.
That the Hawke’s Bay Regional Planning
Committee receives and notes the “Potential Tukituki plan
change” staff report.
2.     The
Hawke’s Bay Regional Planning Committee recommends that Hawke’s
Bay Regional Council:
2.1.     Agrees
that the decision to be made is not significant under the criteria contained
in Council’s adopted Significance and Engagement Policy, and that the
Committee can exercise its discretion and make this decision without
conferring directly with the community in addition to the feedback already
provided by stakeholders.
2.2.     Agrees
to progress a proposed plan change to defer the 2018 minimum flow
regime by a further two years to 1 July 2021 using the standard or streamlined
path for plan making, pending a staff recommendation on the best pathway
to be used.
Moved by Cr Hewitt/
Seconded
by Cr Dick
Cr Fenton Wilson left the meeting at
11.15am, part way through debate, and the quorum was lost with item 6.
Potential Tukituki Plan Change unresolved.
Due to loss of quorum the meeting lapsed
and the following items in addition to item 6 Potential
Tukituki Plan Change are deferred to the April 2019 Regional Planning Committee
meeting.
Councillor Bailey left the meeting at 11.19am and Councillor Kirton
left the meeting at 11.20am
7.
Regional Three Waters Review
8.
Tangata Whenua Remuneration Review Process Update
9.
Resource Management Policy Projects Update
10.
Statutory Advocacy Update
11.
Discussion of Minor Items of Business Not on the Agenda
Closure:
Pieri Munro offered
a karakia to formally close the meeting.
The quorum having
been lost and the meeting having lapsed, the Chairman declared the meeting
closed at 11.30am on Wednesday, 20 February 2019.
Signed
as a true and correct record.
DATE: ................................................               CHAIRMAN:
...............................................