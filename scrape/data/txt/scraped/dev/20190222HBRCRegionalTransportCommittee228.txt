Minutes of Regional Transport Committee - 22 February 2019
MINUTES OF A
meeting of the Regional Transport Committee
Date:                          Friday 22 February 2019
Time:                          10.15am
Venue:
Council Chamber
Hawke's Bay Regional Council
159 Dalton Street
NAPIER
Present:                     Cr A J Dick – HBRC – Chair
Cr F Wilson – HBRC – Deputy Chair
Cr
K Price – NCC
Cr
A Redstone – HDC
L
Young – WDC
E
Speight – NZTA
Alternates:                          Cr D Tennent – CHBDC
In Attendance:          J
Palmer – CE HBRC
T Skerman – Group Manager Strategic Planning
B
Gregory – HBRC Māori Committee
P
Michaelson – AA
M
Broderick – NZ Police
I
Emmerson – Road Transport Association
A
Mills – KiwiRail
L
Malde – HB DHB
J
Lloyd – CHBDC
A
Roets – Governance Administration Assistant
TAG                           A Redgrave – HBRC
R Malley
– NCC
S
McKinley – CHBDC
M
Clews – HDC
O
Postings – NZTA
1.       Welcome/Apologies/Notices
The Chair welcomed
everyone to the meeting.
Crs Fenton Wilson
and Ann Redstone advised that they need to leave the meeting at 12.20pm and 12.30pm
respectively.
The Chair also
congratulated Oliver Postings (NZTA) on his new position as System Manager in
Hawke’s Bay and Gisborne region, replacing Wayne Oldfield.
Resolution
RTC1/19       That the apologies for absence from Mayor Craig Little (Wairoa
District Council), Steve Young (Napier Port), Jag Pannu (Hastings
District Council) and Mayor Alex Walker (Central Hawke’s Bay
District Council) be accepted.
Redstone/Price
Carried
2.       Conflict
of Interest Declarations
There were no
Conflict of Interest Declarations.
3.
Short Term Replacements for the 22
February 2019 Regional Transport Committee Meeting
RTC2/19
Resolution
That Libby Young be appointed as member of the Regional Transport Committee of the
Hawke’s Bay Regional Council for the meeting of Friday, 22 February
2019 as short term replacement on the Committee for Mayor
Craig Little.
Wilson/Price
CARRIED
4.       Confirmation
of Minutes of the Regional Transport Committee Meeting held on 7 December 2018
RTC3/19
Resolution
Minutes of the Regional Transport Committee held on Friday, 7
December 2018, a copy having been circulated prior to the meeting, were taken
as read and confirmed as a true and correct record.
Wilson/Price
CARRIED
5.
Follow-Ups From Previous Regional
Transport Committee Meetings
Stock effluent disposal facility in
Wairoa:  Cr Wilson had a discussion with AFFCO
Management regarding making use of their facility rather than having to build
a new facility.  Cr Wilson thanked Libby Young for WDC’s
comprehensive report by OPUS.  AFFCO very receptive in finding a
solution and currently looking to see whether they can manage the extra
volumes under their current consent conditions.  Cr Wilson to follow-up
on this matter.
Plan for
safety to be considered for reopening of the Napier-Wairoa line:  Will report in full to the next meeting.
Implications
on the transport network of water bottling plants at Awatoto and Tomoana:
Steve Young (Napier Port) who was not present at
the meeting, gave a short summary on the water volumes.
·
Dec 2018 – 48 containers exported
·
Jan 2019 – 25 containers exported.
·
B-Double Unit for empty containers:  Unit
being manufactured currently and will hopefully be completed and commissioned
in mid-March.
Not having an adverse effect on the road
network at this stage.
Business
case for route between Wairoa District and Bay of Plenty via SH 28:  Emma Speight reported that this is progressing through the
PGF process.
Napier to
Wairoa Log proposal:   About 3 weeks
away for completing the civil works on the land slide at Raupunga.
Planning the opening event for the beginning of May 2019. To update the
committee on progress.
Tom Skerman
– the business proposal with KiwiRail is progressing.  Council is
considering a request from KiwiRail at their next meeting on 27 February.
RTC4/19
Resolution
That the Regional Transport Committee
receives the “Follow-ups from Previous Regional Transport
Committee Meetings”.
Redstone/Speight
CARRIED
6.
Call for Minor Items of Business Not on
the Agenda
That Regional Transport Committee accepts the following “Minor Items of Business Not on the Agenda”
for discussion as Item 13
Item
Topic
Raised
by
1.
Update on the funding  assistance rate for emergency
works
Shawn McKinley
2.
Future RLTP changes based on the PGF
Shawn McKinley
7.
Variations to the Regional Land
Transport Plan
Anne Redgrave reported on this and asked
the committee to consider the proposed variations to the Regional Land
Transport Plan 2018.  Further discussions traversed:
·
It was noted that the Land Transport
Management Act allows for an RLTP to be varied at any given time.
·
Flexibility is required in order to respond to
funding opportunities, timing changes and other variations.
·
The requests  include the LED Accelerated
Replacement on State Highways, the Black Beach Nuhaka to Opoutama Resilience
Improvement and the Whirinaki Cycleway extension projects in the 2018 RLTP.
·
Libby Young reported that Wairoa is aiming to
become a Dark Sky Reserve and asking  NZTA to take the LED design into
consideration. Emma Speight acknowledged the comment but requested that NZTA
be formally notified as it is a different process to follow.
·
Dark Sky Reserve is managing lighting
pollution in the district and promoting areas that can be used for stargazing
etc.  Currently 2-3 regions in NZ that have this status.
·
Black Beach Nuhaka to Opoutama Resilience
Improvement:  Wairoa District Council is seeking to have the road
realignment added to the RLTP by way of variation and will apply to the
Provincial Growth Fund for funding for the route. A successful application
would enable WDC to provide a resilient route and safeguard tourism, jobs,
businesses and community access to the Mahia Peninsula.
·
The cost of the project is $15.5 million over
three years.
·
Whirinaki cycleway extension:  Proposed 7.5km
northern extension to the existing Bay View cycleway along the coast.
·
This route will connect Napier with PanPac
which has over 500 employees and will have a significant value for
pedestrians and or commuters.
·
Napier City Council and  Hastings
District Council have made a provision for a contribution to this link in
their cycling programmes.
This variation is necessary to add an
NZTA contribution to the programme.Anne Redgrave
reported on this and asked  the committee to consider the proposed
variations to the Regional Land Transport Plan 2018.  Further
discussions traversed:
Anne noted that the
Land Transport Management Act allows for an RLTP to be varied at any given
time.
Flexibility is
required in order to respond to funding opportunities, timing changes and
other variations.
The requests
include the LED Accelerated Replacement on State Highways, the Black Beach
Nuhaka to Opoutama Resilience Improvement and the Whirinaki Cycleway
extension projects in the 2018 RLTP.
Libby Young
reported that Wairoa is aiming to become a Dark Sky Reserve and asking  NZTA
to take the LED design into consideration. Emma Speight acknowledged the
comment but requested that NZTA be formally notified as it is a different
process to follow.
Dark Sky Reserve is
managing lighting pollution in the district and promoting areas that can be
used for stargazing etc.  Currently 2-3 regions in NZ that have this
status..
Black Beach Nuhaka
to Opoutama Resilience Improvement:  Wairoa District Council is seeking
to have the road realignment added to the RLTP by way of variation and will apply
to the Provincial Growth Fund for funding for the route. A successful
application would enable WDC to provide a resilient route and safeguard
tourism, jobs, businesses and community access to the Mahia Peninsula.
The cost of the
project is $15.5 million over three years.
Whirinaki cycleway
extension:  Proposed 7.5km northern extension to the existing Bay View
cycleway along the coast.
This route will
connect Napier with PanPac which has over 500 employees and will have a
significant value for pedestrians and or commuters.
Napier City Council
as well as Hastings District Council have made a provision for a contribution
to this link in their cycling programmes.
This variation is
necessary to add an NZTA contribution to the programme.
RTC5/19
Resolution
1.      That the Regional Transport Committee receives and notes the ‘Variations
to the Regional Land Transport Plan’ staff report.
2.      The Regional Transport Committee recommends that Council:
2.1       Agrees
that the decisions to be made are not significant under the criteria
contained in Council’s adopted Significance and Engagement Policy, and
that Council can exercise its discretion and make decisions on this issue
without conferring directly with the community and persons likely to be
affected by or to have an interest in the decision.
2.2       Approves
the variations to the Regional Land Transport Plan, introducing the LED
Accelerated Replacement Programme, the Blacks Beach Nuhaka to Opoutama
Resilience Improvement and the Whirinaki Cycleway Extension to the plan.
Price/Wilson
CARRIED
8.
February 2019 HBRC Transport Manager's
Report
Anne Redgrave updated the committee on
Provincial Growth fund applications with discussions highlighting:
·
a request was made for more detail on Driver Licencing
funding available
·      The initial Driver licencing funding two year programme will end
on 30 June 2019, and will be re-evaluated for ongoing demand. No local shared
funding allocated beyond 2019, thus would rely on Councils, agencies and NZTA
to contribute to the fund. 65% of fatalities in the
East Coast Region, were unlicensed drivers. The number of police referrals to
providers is increasing due to an initiative by the Police to have
information on driver licensing providers directly on hand. There should be a
strategy put in place supporting the licensing system for those who are
unlicensed.
Cr Alan Dick
moved a motion; being:
To allocate
further funding for Driver’s Licencing to meet sufficient local share
and to put a strategy in place supporting the licensing system for unlicensed
individuals.  Seconded by Cr David Tennent.
RTC6/19
Resolution
“That the Regional Transport
Committee recommends to Councils and other effected bodies to allocate
funding for Driving Licensing to meet sufficient local share and to put a
strategy in place supporting the licensing system for unlicensed individuals.”
Dick/Wilson
CARRIED
Consideration of the item recommenced,
with discussions covering:
·
A meeting of the Driver Licensing Governance
Group  will be convened to progress driver licensing further.
·
The Whakatu Arterial Link, the Region’s
biggest project, was successfully opened to traffic on 19 December 2018.
·      Hastings District Council invited the Chair for the blessing of
the road “Te Ara Kahikatea” on Saturday, 23 February at 10am.
RTC7/19
Resolution
That the Regional Transport Committee receives and notes the “February
2019 HBRC Transport Manager's report”.
Tennent/Redstone
CARRIED
9.
NZTA Central Region - Regional Relationships Director's Report February
2019
Emma Speight gave a presentation on the
new road safety strategy and updates on the local network with discussions
highlighting:
·
Next steps in the development of the new road
safety strategy will be released for public consultation in April. The
Consultation period will include engagement with partners and public to seek
feedback on the direction and on whether to adopt a Vision Zero approach.
·
The Safe Networks is an initiative with local
government, targeting reduction of up to 160 road deaths every year, starting
with priority areas of Auckland, Waikato and Canterbury. $1.5 billion will be
invested over 3 years.
·
Safety Boost programme, which started in 2018,
has been extended with safety improvements planned for 11 rural state
highways in Gisborne, Hawke’s Bay, Manawatu-Whanganui and the West
Coast. Simple improvements like road widening, rumble strips along the side
and centre of roads, improving signs, and installing road safety barriers.
·
Asset Management Data Standard (AMDS) project
now in development.  Focus on easier data management and analytics
between organisations to improve service using smart technologies. Draft
business case will be launched in December 2019 and anticipate that a full
business case will be operative in September 2020.
·
Motorcycle Safety (Shiny Side Up) is working
collaboratively with the Transport Agency and ACC’s Ride Forever safety
programme with a range of events currently running from 3 February – 4
March 2019.  More info available on NZTA’s website.
·
Travel Demand Management is working to achieve
a nationally coordinated approach including innovative solutions to demand
issues for users, councils and other partners by enabling more travel
choices.
·
An update on the local network was given.
·
Concerns raised in relation to the safety and
congestion on the road between Pakipaki and Waipukurau – increase in
heavy trucks carrying timber coming from CHB.  AA supports the view and
disappointed to hear that realignment of the road has been removed.
·
NZTA facing financial constraints as a
consequence of shifting priorities for Government.
·      Waikare Gorge is regarded as a significant investment and
important piece of work with the project being initiated and in early stages,
however no timeline for completion at this stage.
RTC8/19
Resolution
That the Regional Transport Committee
receives the ‘NZTA Central Region – Regional Relationships Director’s
Report for February 2019.
Wilson/Price
CARRIED
10.
Roadsafe Hawke's Bay February 2019
Update
Anne Redgrave updated the committee on
the statistics on road safety with discussions highlighting:
·
Increases in deaths and serious injuries
recorded across the country.
·
The Communities at Risk Register, compiled by
NZTA, has highlighted that Wairoa is now elevated as the highest overall
personal risk for all crash types of any district of city in the country.
·
Wairoa’s statistics have been trending
for a long time despite efforts and investment made in the area.
·
Wairoa District Council has taken a good
strong look at the investment currently to combat some of the high number of
road deaths.
·
Statistics also includes state highways in the
region and thus not directly related to local roads.
·
Cr Wilson reported that drugs, in particular,
are a real concern in Wairoa as in other rural towns.
·
Following the driver’s licence
discussion, Anne Redgrave reported that a teacher from Wairoa College, which
is funded by HBRC,   is running a programme for students with the
aim of getting them through all stages of driver licensing before leaving
school.
·
RoadSafe has a good presence in Wairoa with
different programmes running.
·
Currently 10hours  a week  allocated
for a  staff member in  Wairoa  on RoadSafe programmes. Cr
Wilson asked whether it is time for this to be increased, in light of
Wairoa’s road safety statistics.
·
RoadSafe Hawke’s Bay recent activities
discussed and listed in the paper.
RTC9/19
Resolution
That the Regional Transport Committee receives and notes the “RoadSafe
Hawke's Bay February 2019 Update” staff report.
Wilson/Price
CARRIED
11.
February 2019 Public Transport Update
The report was taken as read with Anne
Redgrave to highlight the following:
·
The bus service webpage has recently being
separated from the HBRC’s website with more enquiries being dealt with
as a result.  Feedback about the new stand-alone website (www.goBay.co.nz) has been very positive.
·
The new bus ticketing and smartcard
system  is further delayed. HBRC has conducted initial testing on its
system and has confirmed that the tariffs and passenger categories are loaded
into the system correctly.
·
However, site acceptance testing by the
supplier has indicated a requirement for further development and testing.
·
Aiming to have the new system in place in
September 2019.
·
Overall decline in patronage, however services
between Napier and Hastings are increasing, particularly the Express
services.
·
Initiatives underway with organisations to
provide concession discounts.
·
HBRC has funded a 6-month  trial for HBRC
staff to travel  more cheaply on the bus. Very good response to date.
·
HBRC challenges Napier City Council and
Hastings District Council to also introduce staff subsidies to encourage
their staff to use the buses.
·
DHB continues to see an increase in use of
their subsidised staff bus scheme.
RTC10/19
Resolution
That the Regional Transport Committee receives and notes the “February
2019 Public Transport Update” report.
Redstone/Speight
CARRIED
12.
Verbal Updates by Advisory
Representatives
·
Matt Broderick (NZ Police), updated the committee on the relevant issues raised in the media
around a crash which occurred on the Ngaruroro river bridge the previous
night.
·
27 incidents from December 2018 at the same
location.  Seek some collaboration with NZTA to send out some media
messaging to  dissuade people from jumping from the bridges.
·
NZ Police seeking more visible signage warning
communities around the safety hazards.
·
Currently investigating further road police
deployment across the district.  Police not always deployed at the
highest risk locations.
·
Lisa Malde (HB DHB) reported that transport trips for staff increased by 112 %, which
is due to the subsidised bus transport scheme. DHB will be marking shared
road markings in the vicinity of the hospital site and will be running
campaigns in partnership with Hastings District Council to help educate both
driver’s and cyclists.
·
DHB  has been certified through the
Certified Emissions Measurement  and  Reduction programme, with
them being the fourth entity in Hawke’s Bay and fifth entity in NZ to
be certified.
·
This will not only measure emissions, but
study emission reduction targets. Will  aim to minimise environmental
impact and health increase impacts.
·
Paul Michaelson (AA) – The AA Research Foundation has completed work on making  speed
limits clear - a link will be distributed to committee.
·
Saddle Road:  reduction in speed to
60km/h bit conservative.  In response to the comment made, NZTA has
commissioned a report on the appropriate speed on Saddle Road.
·
Ian Emmerson (Road Transport Association) – Positively commented on Whakatu Arterial which seems to
cater for increased traffic during harvest very well.
·
Concern raised on the seal condition on State
Highway 2 and 5 in various places.  Oliver Postings reported that
work  is underway and will be completed before winter starts.
·
Antony Mills (KiwiRail) – “Go Live” of Napier Port level crossing on the
western entrance of the Port.  Will be operational for trucks and
trains  on Sunday, 24 February.
·
Road integration into the Port will go live by
end of March 2019.
·
Major improvement in pedestrian control from
the carpark to the beach.
Recommendation
That the Regional Transport Committee receives the “Verbal Updates by Advisory
Representatives”.
13.
Discussion of Minor Items Not on the
Agenda
Item
Topic
Raised
by
1.
Future RLTP changes based on the PGF:
·
CHB is applying to the PGF for some  improvements to
enable HPMV access and  improved resilience on some of its key rural
routes. Advising that these will be brought to the committee  for
consideration as variations to the RLTP
Shawn McKinley
2.
Update on the funding  assistance rate for emergency
works:
·
Struggling to complete work by 30 June.
·
Serious consequences if work is not completed in time –
rate payers will have an increase in rates if not completed in time.
·
Working collaboratively with NZTA to build a business case in
dealing with the issues and concerns.
·
Update on the business case to be presented to Mayor Alex
Walker,  after which it will be presented to local government to lobby
for getting  the funding assistance rate adjusted.
Shawn McKinley
Closure:
There
being no further business the Chairman declared the meeting closed at 12.20pm
on Friday 22 February 2019.
Signed
as a true and correct record.
DATE: ................................................               CHAIRMAN:
...............................................