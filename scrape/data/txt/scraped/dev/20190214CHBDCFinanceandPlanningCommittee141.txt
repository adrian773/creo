Minutes of Finance and Planning Committee Meeting - 14 February 2019
Finance and Planning Committee
Meeting Minutes                                                  14
February 2019
MINUTES OF Central
HAwkes Bay District Council
Finance and
Planning Committee Meeting
HELD AT THE Council
Chamber, 28-32 Ruataniwha Street, Waipawa
ON Thursday, 14
February 2019 AT at the conclusion of the Public Forum which
commences at 9.00am
PRESENT:              Cr Ian Sharp (Deputy Mayor)
Mayor Alex Walker
Cr Tim Aitken
Cr Kelly Annand
Cr Shelley
Burne-Field
Cr Tim Chote
Cr Gerard Minehan
Cr Brent Muggeridge
Cr David Tennent
IN ATTENDANCE:
Joshua Lloyd (Group
Manager, Community Infrastructure and Development)
Monique Davidson
(CEO)
Bronda Smith (Group
Manager, Corporate Support and Services)
Doug Tate (Group
Manager, Customer and Community Partnerships)
Nicola Bousfield
(People and Capability Manager)
Leigh Collecutt
(Governance and Support Officer)
1            Apologies
Committee Resolution
Moved:       Cr
Ian Sharp
Seconded:  Mayor Walker
That Dr Roger Maaka is tabled as an apology for the meeting.
Carried
2            Declarations
of Conflicts of Interest
None
3            Standing
Orders
Committee Resolution
Moved:       Cr
Gerard Minehan
Seconded:  Cr
Shelley Burne-Field
THAT the following standing orders are suspended for the duration
of the meeting:
·      20.2 Time limits on speakers
·      20.5 Members may speak only once
·
20.6 Limits on number of speakers
AND THAT 21.4 Option C under section 21 General procedures for
speaking and moving motions be used for the meeting.
Carried
4            Confirmation
of Minutes
Committee Resolution
Moved:       Cr
Gerard Minehan
Seconded:  Cr
Tim Aitken
That the minutes of the Finance and
Planning Committee Meeting held on 29 November 2018 as circulated, be
confirmed as true and correct.
Carried
5            Local
Government Act Compliance – Planning, Decision Making and Accountability
Council is required to make decisions in
accordance with the requirements of Part 6 Subpart 1 of the Local Government
Act 2002.
Committee Resolution
Moved:       Cr
Ian Sharp
Seconded:  Cr
David Tennent
THAT Council has read the reports associated with items 6.1 to 6.5
and considers in its discretion under Section 79(1)(a) that sufficient
consultation has taken place in addition to the Councillors knowledge of the
items to meet the requirements of Section 82(3) in such a manner that it is
appropriate for decisions to be made during the course of this meeting.
Amendment
Moved:       Cr
Ian Sharp
Seconded:  Cr
David Tennent
That item 5 - Local Government Act Compliance  - Planning,
Decision Making and Accountability is removed from all Finance and Planning
Committee Meeting Agendas going forward.
Carried
6            Report
Section
6.1         Finance
and Planning Committee Work Programme Update
PURPOSE
The purpose of this report is to provide an update to the
Finance and Planning Committee on the key priorities as identified in the
Terms of Reference and work programme for the committee.
Committee Resolution
Moved:       Cr Tim Aitken
Seconded:  Cr
Shelley Burne-Field
That, having considered all matters raised in the
report, the report be noted.
Carried
·
Mayor Walker asked officers about progress of
the rates review.
It was confirmed
that it was a long term review over a 12-24 month period and that it was
currently on track.
·
Cr Tennent asked whether if the Counci made a
decision on the scope of work for the rates review, if this would bind the next
Council in any way.
It was confirmed by
Cr Sharp and officers that this could be rescoped if needed.  The intention
of the review is that it is a guideline which outlined milestones, rather than
a binding strategy.
6.2         Quarterly
Financial Reporting for December 2018
PURPOSE
Provide Council with a summary of
Council's second quarter financial performance for the 2018/19 financial
year.
Committee
Resolution
Moved:       Cr
Gerard Minehan
Seconded:  Cr
Kelly Annand
That,
having considered all matters raised in the report, the report on Council's
second quarter financial performance for the 2018/19 financial year be noted.
Carried
·
Cr Muggeridge asked whether officers were
confident that project work was going to be completed and that contractors were
going to be available, given a large proportion of work is phased for the last
6 months of the year.
Officers confirmed
that the capital works programme was a priority and that sound programme
management would help to achieve milestones.  Where officers are not
confident that deliverables would be met, these have been signalled to Council.
It was also
confirmed that while finding qualified contractors is potentially a risk, that
a good amount of interest has been generated for projects through the use of
different engagement tools.
·
Mayor Walker asked about income subsidies from
NZTA for emergency work and whether this was included in the figures.  It
was confirmed that some of it was included and that most of the emergency work
from weather events had either been programmed or completed, however a paper
would be coming to Council to discuss further work needed after the December
weather event.
·
Cr Aitken asked officers how they had achieved
the reduction of $202,032 for bad debts since September.  It was confirmed
that this was managed thorugh various channels, including officers actively
working to make payment arrangements, mortgage demands and outsourcing some
debt management.
·
Cr Tennent discussed roading issues after rain
events and asked whether the regional council accepted responsibility for
remedial work required when roads were impacted by rivers or waterways.
Mr Lloyd confirmed that although the regional council were not directly giving
a financial contribution, that they were assisting officers by making the
process as easy as possible and assisting with storage.
The Chief Executive
noted that there were continuing discussions with the regional council about
long term sustainability in terms of funding for extreme events.
6.3         Quarterly
Activity Reporting - October to December 2018
PURPOSE
The purpose of this report is to provide Council with a
summary of the organisation’s quarterly activity reporting for the
period October to December 2018.
Committee Resolution
Moved:       Cr Kelly Annand
Seconded:  Cr
David Tennent
That, having considered all matters raised in the
report, the report be noted.
Carried
·
Cr Minehan indicated that stakeholders would
like to have ongoing meetings with Building Control officers to discuss changes
in legislation and other relevant matters.
·
Discussion took place about growth assumptions
made in last year’s Long Term Plan and given the significant growth in
some areas of the district it was agreed that this would need to be re
forecast.
·
Cr Tennent asked what the Council’s
obligations were in terms of dealing with animal welfare.  It was
confirmed that the recent review of the animal control service highlighted that
some improvements needed to be made in this area.  It was also noted that
there is a lack of clarity about the Council’s role in conjunction with
other agencies.
·
Cr Burne-Field asked what could be expected in
terms of responsiveness when an additional animal control staff member was
appointed.  The Chief Executive confirmed that it would enable the
organisation to be more proactive and visible, rather than strictly just
dealing with animal control enforcement.
·
Cr Burne-Field also requested that the
organisation have more proactive communication about what to do when people
need support or help with animal control.  This was noted by officers.
·
Cr Aitken asked about the large pile of metal at
the transfer station and whether this could be moved. It was confirmed that
contractors were working to deal with this.
·
Cr Tennent asked whether watering was still
taking place at Russell Park.  Officers indicated while there would still
be some watering taking place that recent rainfall has not meant that this had
not been necessary.
·
Cr Annand requested that the target for number
of users of the Memorial Hall be clarified for future reporting.
·
Cr Burne-Field noted concerns about the number
of ammonia exceedences for wastewater treatment.
·
Cr Muggeridge asked for clarification about
wastewater flow volumes and the point at which officers would be highly
concerned about exceedences.
It was confirmed
that exceedences were reported to the regional council and that the council was
working with them on planning to help reduce these issues.
6.4         Elected
Members Expenses for July to December 2018
PURPOSE
The purpose of this report is to update
Council on the Elected Members’ Expenses for the six month period of
July to December 2018.
Committee
Resolution
Moved:       Cr
Tim Aitken
Seconded:  Cr
David Tennent
That,
having considered all matters raised in the report, the Elected Members
Expenses for July to December 2018 report be noted.
Carried
6.5         Key
Project Status Report #4 - Big Water Story
PURPOSE
The purpose of the report is to provide information to
Council on the progress and status of #thebigwaterstory programme.
Committee Resolution
Moved:       Mayor Alex
Walker
Seconded:  Cr
Gerard Minehan
That, having considered all matters raised in the
report, the report be noted.
Carried
·
Cr Sharp offered congratulations around supplier
mangement and engagement for these projects.
·
Cr Minehan sought clarification about timeframes
for consents from the regional council.
·
Cr Annand sought clarification about what the
indicators were for bore locations.  Mr De Klerk confirmed that locations
were dictated by seismic testing and locations of surrounding bores.
·
Cr Aitken sought clarification about the impact
that establishing new bores would have on existing bores.  It was
confirmed that testing would be done to ensure that there was not an
impact.  If if was determined that there was an impact, officers would explore
other options.
·
Mayor Walker asked whether the required
resourcing would likely have implications for the annual plan.  It was
confirmed that this has been accounted for.
·
Cr Tennent sought clarification of the consent
process for farm supply in terms of the taking of water.  Mr De Klerk
clarified that initially consent would be sought for a test bore.  Once it
was determined that a test bore was fruitful and that testing was satisfactory,
consent would then be sought for a production bore and associated water take.
·
Mayor Walker noted the recent discussion at the
regional council meeting around delivery of infrastructure projects in the
district.  It was tabled that there was confidence in the processes
officers were following for programme delivery.
7            Date
of Next Meeting
Committee Resolution
Moved:       Cr
David Tennent
Seconded:  Cr
Tim Chote
THAT the next meeting of the Central
Hawke's Bay District Council be held on 28 March 2019.
Carried
8            Time
of Closure
The Meeting closed at 10.10am.
The minutes of this meeting were
confirmed at the Finance and Planning Committee
Meeting held on 28
March 2019.
...................................................
CHAIRPERSON