Minutes of HDC – Tangata Whenua Wastewater Joint Committee Meeting
- 28 June 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
HDC–Tangata Whenua
Wastewater Joint Committee
Meeting
Date:
Friday, 28 June 2019
CG-14-15-00007                                                                         1
Minutes
of a Meeting of the HDC–Tangata Whenua
Wastewater Joint Committee held on 28 June 2019 at 9.25am
Table of Contents
Item                                                                                    Page No.
1.         Apologies  2
2.         Conflicts
of Interest 2
3.         Confirmation
of Minutes  2
4.         Going
Forward - Tangata Whenua Wastewater Joint Committee  2
5.         2017/18
Annual Wastewater Treatment Resource Consent Compliance Report 3
6.         Additional
Business Items  5
7.         Extraordinary
Business Items  5
CG-14-15-00007                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE HDC–Tangata
Whenua Wastewater Joint Committee HELD IN THE Landmarks Room, Ground Floor, Civic
Administration Building, Lyndon Road East, Hastings ON
Friday, 28 June 2019 AT 9.25am
Present:                          Chair: Councillor Heaps
Councillors Barber, Travers and Watkins
Mrs Evelyn Ratima and Tania Kupa Huata
IN ATTENDANCE:             3 Waters Services Manager (Mr B Chapman)
Wastewater Manager (Mr D James)
Principal Advisor: Relationships, Responsiveness and Heritage
– (Dr James Graham)
3 Waters Process Engineer (Mr K Sevaratnam)
3 Waters Consents Management Officer (Mr S Taylor)
3 Waters Service Assurance Manager (Mr Wojtek Dziubek)
Wastewater Treatment Plant Assistant (Mr A Baker)
General Counsel (Mr S Smith)
Committee Secretary (Mrs C Hunt)
AS
REQUIRED:                  Mr Mark von Dadelszen, Legal Counsel
Mr
Grant Russell, Planning Consultant, Stantec
MINUTE’S SILENCE
The Committee paid tribute to Bob
McWilliams (9 April 2018), who had been employed at the Wastewater Treatment
Plant for 27 years and was instrumental in the development of the biological
trickling filter system, which saw raw human sewage no longer being discharged
into the sea.
The Committee also paid tribute to Gordon
Paku, who passed away on 15 October 2018, and had
been a member of the HDC Tangata Whenua Joint Wastewater Committee since
2003.  The Committee acknowledged the dedicated support and work
Gordon had contributed for the last fifteen years, towards a solution for a
sustainable waste water management system (the biological trickling filter
system) installed at East Clive.  Gordon
would also be remembered for his great sense of humour.
The Chair then requested a
Minute’s Silence to recognise the  passing of both Bob McWilliams
(Wastewater Treatment Manager) and Gordon Paku (Committee Member).
1.         Apologies
Councillor Watkins/Councillor
Travers
That an
apology from Mr Peter Paku be accepted.
CARRIED
Leave of Absence
had previously been granted to Councillor Lyons.
2.         Conflicts
of Interest
There were no
declarations of conflicts of interest.
3.         Confirmation
of Minutes
Councillor Heaps/Councillor Watkins
That the
minutes of the HDC – Tangata Whenua Wastewater Joint Committee Meeting
held Thursday 1 March 2018 be confirmed as a true and correct record and be
adopted.
CARRIED
4.
Going Forward - Tangata Whenua Wastewater Joint
Committee
(Document 19/481)
Queens Birthday Honours 2019  - The Principal Advisor: Relationships, Responsiveness and
Heritage, Dr James Graham, prior to addressing the report, congratulated
Evelyn Ratima on being awarded the Queen’s Service Medal (QSM) in
recognition of supporting and transforming the community of Whakatu.
Dr Graham advised that the purpose of the
report was to seek guidance regarding the two vacant tangata whenua
representative roles on the Committee.  The two vacancies have resulted
in the passing of Tipu Tareha (2013) and Gordon Paku (2018).
It is important that as old members
depart and new members are appointed that the journey that the Committee has
been on is not lost.  The knowledge and understanding is
intergenerational for the continuation of the journey,
Dr Graham advised that previously the Tangata Whenua representatives would be nominated by two
Māori Executives.   However, these organisations were created
under the auspices of the New Zealand Māori Council, which had recently
changed its structural approach and these executives no longer existed.
The New Zealand Māori Council
structure had changed and its “local” representative body was
more regional by nature and was constituted as the Takitimu District
Māori Council.
Legal Counsel, Mr von Dadelszen advised
that the new consent was not as prescriptive as previously and allowed for
more scope and that if the Committee wanted to include the three waters
(stormwater, drinking water and wastewater) there was no reason why it could
not be done.  Any change would require Council agreement to include in a
condition as the Committee had 50/50 representation.  The current
consent does not allow a casting vote for the Chair.
The meeting agreed that they did not want
to broaden the scope of the Committee to include other than wastewater.
The new consent did not prescribe how
tangata whenua were chosen and guidance for  consultation and engagement
with the correct communities and bodies.
The meeting agreed that they did not want
appointees to be “handpicked” and that a fair and transparent
process occur.
It was agreed that a workshop be held to
discuss and establish a process for appointment of Tangata Whenua
representatives to the Committee.
Ms
Huata-Kupa/Councillor Travers
A)
That the report of the Wastewater Manager titled “Going
Forward - Tangata Whenua Wastewater Joint Committee” dated 28/06/2019
be received.
B)
That the Committee establish a process to appoint tangata
whenua members to the HDC : Tangata Whenua Wastewater Joint Committee to
fulfil the provisions of Condition 29 of the District Council’s
Wastewater Discharge Consent granted on 25 June 2014.
C)
That a Committee Workshop be held on 12 July 2019 to review a
process on a method of appointments and report back to the HDC : Tangata
Whenua Wastewater Joint Committee by 2 August 2019.
CARRIED
5.
2017/18 Annual Wastewater Treatment Resource
Consent Compliance Report
(Document 19/594)
The Wastewater Manager, Mr James spoke to the Annual Wastewater Resource Consent Compliance report
and updated the Committee on key wastewater activities for the period ending June 2017.
The Process Engineer, Mr Sevaratnam
spoke to the powerpoint presentation (WAT-5-09-4-19-11) providing an
overview of the 2017 annual discharge consent compliance summary.
Despite a minor malfunction of the drogue
being highlighted against Condition 17 in that it failed to collect data
during the fourth quarter and was unable to determine the current.  The
consent was compliant.
Officers did not foresee that the
malfunction of the drogue would be a major issue and they were in regular
contact with the Hawke’s Bay Regional Council.
The following works were underway at the wastewater plant:
·
Replacement of the dewatering system in the
Milliscreen building.
·
Replacement of one of the domestic step
screens with a Centre-Flo band screen.
·
Upgrade of the plant control system (PLC).
·
Outfall pump station manifold renewal
planning.
·
Emergency beach overflow chamber renewal.
Grant Russell, Planning Consultant,
Stantec advised that there were some minor
administrative matters in the review of the consent conditions that
would be undertaken.
Condition 27 of
the long term 35 discharge consent required Council to undertake a Nine
Yearly Review. Investigations and studies were likely to commence in 2021/22.
During the first review there would be an
increase in engagement and contribution from the Committee with the following
being considered:
·
Population and industry trends assessment
·
Wastewater flows and loads trends assessment
·
Trade waste profile assessment
·
Assessment of environmental guidelines and
standards for treated wastewater
·
Assessment of operational aspects and changes
in wastewater treatment technology and potential opportunities for beneficial
reuse
·
Undertake benthic review, hydrodynamic study
and public health impacts assessments
An Open Day of the Wastewater Treatment
Plant was held on 24 November 2018.  A condition of the Consent stated
that an advertisement of the Open Day be included in the Rates Notice.
However, radio advertising was undertaken as well which proved to be highly
effective and far reaching.
Mr Russell advised that in order to
preserve the journey of discovery (1999-2000) of the HDC : Tangata Whenua
Wastewater Joint Committee with members departing it had been decided to put
together a short video clip (CG-14-15-00008) which was displayed. To view
open the hyperlink below:  https://drive.google.com/a/grundyproductions.co.nz/file/d/18cr-rhaS2DQa4hSAyYop68rLlP2Vy1X6/view?usp=sharing
It was noted that in recognition and
memory of Bob McWilliams and the pivotal role he had played in the
development of the biofilter trickling system that a memorial plaque at the
East Clive Treatment Plant would be appropriate.
The Chair advised the meeting that David
James had resigned his position after eight years to embark on new ventures
and on behalf of the Committee wished him well for the future.
Councillor Travers/Councillor Watkins
A)        That
the report of the Wastewater Manager titled “2017/18
Annual Wastewater Treatment Resource Consent Compliance Report ”
dated 28/06/2019 be received.
With the reasons for this decision
being that the objective of the decision will contribute to meeting the
current and future needs of communities for good quality local
infrastructure.
CARRIED
6.         Additional Business Items
There were no additional business items.
7.         Extraordinary
Business Items
There were no extraordinary business items.
________________________
The meeting closed at 11.15am
Confirmed:
Chairman:
Date: