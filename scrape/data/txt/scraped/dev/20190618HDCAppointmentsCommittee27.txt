 Hastings District Council 

 
Civic Administration Building 
Lyndon Road East, Hastings 4156 

Phone:  (06) 871 5000 
Fax:  (06) 871 5100 

www.hastingsdc.govt.nz 

 
 

OPEN 

 

M I N U T E S 

 
 

APPOINTMENTS COMMITTEE 

 
  
 

Meeting Date:  Tuesday, 18 June 2019 

 

 

 
 

 

 

 

 

CG-14-9-00011  

1 

Minutes of a Meeting of the Appointments Committee held on 

18 June 2019 at 12.30pm 

 
  

Table of Contents 

 

Apologies 

Conflicts of Interest 

Confirmation of Minutes 

Additional Business Items 

Extraordinary Business Items 

Recommendation to Exclude the Public from Item 7 

 

Page No. 

1 

1 

1 

1 

1 

1 

Item 
  
1. 

2. 

3. 

4. 

5. 

6 

 

 

CG-14-9-00011  

 

1 

HASTINGS DISTRICT COUNCIL 

 

MINUTES OF A MEETING OF THE APPOINTMENTS COMMITTEE 

HELD IN THE GREEN ROOM, GROUND FLOOR, CIVIC ADMINISTRATION 

BUILDING, LYNDON ROAD EAST, HASTINGS ON  

 TUESDAY, 18 JUNE 2019 AT 12.30PM  

 

 
PRESENT: 
 

IN ATTENDANCE: 

Councillor Kerr (Chair) 
Councillors Barber and Poulain  
 
Group Manager: Economic Growth & Organisation 
Improvement (Mr C Cameron) 
Chief Financial Officer (Mr B Allan) 
Manager Strategic Finance (Mr B Chamberlain) 
Committee Secretary (Mrs C Hunt) 

 
  
1. 

 
2. 

 
3. 

 

   

4. 

 
  
5. 

 
    
   
6. 

 

 

 

APOLOGIES 

Leave of Absence had previously been granted to Councillor Watkins. 
  

CONFLICTS OF INTEREST  

There were no declarations of conflicts of interest. 

CONFIRMATION OF MINUTES 

Councillor Kerr/Councillor Barber  

That  the  minutes  of  the  Appointments  Committee  Meeting  held  Thursday  28 
June  2018,  including  minutes  while  the  public  were  excluded,  be  confirmed 
as a true and correct record and be adopted. 

CARRIED   

ADDITIONAL BUSINESS ITEMS  

There were no additional business items. 

EXTRAORDINARY BUSINESS ITEMS  

There were no extraordinary business items. 

RECOMMENDATION TO EXCLUDE THE PUBLIC FROM ITEM 7   

SECTION  48,  LOCAL  GOVERNMENT  OFFICIAL 
MEETINGS ACT 1987 

INFORMATION  AND 

Councillor Poulain/Councillor Kerr  

THAT  the  public  now  be  excluded  from  the  following  parts  of  the  meeting, 
namely; 

CG-14-9-00011  

2 

7. 

Appointment of Director to Hastings District Holdings Limited 

 

The general subject of the matter to be considered while the public is excluded, the reason 
for passing this Resolution in relation to the matter and the specific grounds under Section 
48 (1) of the Local Government Official Information and Meetings Act 1987 for the passing 
of this Resolution is as follows: 

 

 

GENERAL SUBJECT OF EACH 
MATTER TO BE CONSIDERED 

 

 

 

REASON FOR PASSING THIS 
RESOLUTION IN RELATION TO 
EACH MATTER, AND 
PARTICULAR INTERESTS 
PROTECTED 

GROUND(S) 
UNDER 
SECTION  48(1)  FOR  THE 
PASSING 
EACH 
RESOLUTION 

OF 

 

 

7.  Appointment of Director 

Section 7 (2) (a) 

Section 48(1)(a)(i) 

to Hastings District 
Holdings Limited 

The withholding of the information is 
necessary  to  protect  the  privacy  of 
natural  persons,  including  that  of  a 
deceased person. 

Privacy Reasons. 

Where  the  Local  Authority  is 
named  or  specified  in  the 
First  Schedule  to  this  Act 
under Section 6 or 7 (except 
Section 7(2)(f)(i)) of this Act. 

CARRIED   

________________________ 

 

The meeting closed at 12.35pm 

 

Confirmed: 

 
 
 
 

Chairman: 

 

 

Date: 
 

 

