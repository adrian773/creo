Minutes of Community Services Committee - 2 April 2019
Community Services Committee
Open Minutes
Meeting Date:
Tuesday
2 April 2019
Time:
4.00pm
– 4.21pm
Venue
Graeme
Lowe Stand Corporate Lounge 2
McLean Park
Latham Street
Napier
Present
Mayor Dalton, Deputy Mayor
White (In the Chair), Councillors Boag, Brosnan, Dallimore, Hague, Jeffery,
McGrath, Price, Tapine, Taylor, Wise and Wright
In
Attendance
Chief
Executive, Director Infrastructure Services, Director City Services, Director
City Strategy, Manager Communications and Marketing, Chief Financial Officer,
Manager Community Strategies
Administration
Governance
Team
Community Services
Committee - 02 April 2019 - Open Minutes
Apologies
Nil
Conflicts of
interest
Nil
Public forum
Nil
Announcements by
the Mayor
Nil
Announcements by
the Chairperson
Nil
Announcements by
the management
Nil
Confirmation of minutes
Councillors Wright / Price
That the
Minutes of the meeting held on 27 November 2018 were taken as a true and
accurate record of the meeting.
Carried
Community Services Committee - 02 April
2019 - Open Minutes
Agenda
Items
1.    Submission
of Remit Application to Local Government New Zealand - Housing
Type of Report:
Procedural
Legal Reference:
N/A
Document ID:
713591
Reporting Officer/s &
Unit:
Natasha Carswell, Manager
Community Strategies
1.1   Purpose
of Report
To seek approval to submit a
Remit application to Local Government New Zealand (LGNZ) for them to approach the Government to seek funding support for the operation,
upgrade and growth of local authority social housing portfolios.
At the Meeting
The following points were made in discussion:
·
It is believed that this remit provides a sensible and
sustainable way to address issues faced by Councils in the social housing
space.
·
The remit meets the theme of “localism” strongly
supported at the 2018 LGNZ conference, which advocates for local government
being best placed to support local communities.
Committee's recommendation
Councillors
Boag / Wright
The Community Services Committee:
a.     Approve
the submission of the Remit application regarding Social Housing.
b.     That
a DECISION OF COUNCIL is required urgently to allow the remit to be
submitted at the April 2019 Zone 3 meeting.
Carried
Council Resolution
Councillors Wright / Brosnan
That Council:
a.     Approve
the submission of the Remit application regarding Social Housing.
Carried
2.      SUBMISSION
OF REMIT APPLICATION TO LOCAL GOVERNMENT NEW ZEALAND - LIABILITY TO BUILDING
DEFECTS CLAIMS
Type of Report:
Procedural
Legal Reference:
N/A
Document ID:
716495
Reporting Officer/s &
Unit:
Wayne Jack, Chief Executive
2.1   Purpose
of Report
To seek approval to submit a
Remit application to Local Government New Zealand (LGNZ) to take action as
recommended by the Law Commission in its 2014 report on “Liability of Multiple
Defendants” to limit the liability of councils in New Zealand in relation
to building defects claims.
At the Meeting
It was confirmed that the intention is to both put
forward the remit and work with LGNZ and central government, while also
managing risk in developments directly as well.
It was agreed that the ratepayer should not have to
bear the burden of workmanship that is below par.
Committee's recommendation
Councillors
Price / Wise
The Community Services Committee:
a.     Approve
the submission of the Remit application regarding liability of councils in relation
to building defects claims
b.     That
a DECISION OF COUNCIL is required urgently to allow the remit to be
submitted at the April 2019 Zone 3 meeting.
Carried
Council Resolution
Councillors Wright/ Taylor
That Council:
a.     Approve
the submission of the Remit application regarding liability of councils in relation
to building defects claims
Carried
3.    Support
of Hastings District Council Remit Application to Local Government New Zealand
- Alcohol Harm
Type of Report:
Procedural
Legal Reference:
N/A
Document ID:
715928
Reporting Officer/s &
Unit:
Michele Grigg, Senior Advisor
Policy
3.1   Purpose
of Report
To seek Council’s support
for Hastings District Council’s (HDC) Remit application to Local
Government New Zealand (LGNZ) asking them to identify opportunities and
actively advocate on national policy changes to reduce alcohol harm.
At the Meeting
The following points were made in discussion:
·
It is important to ensure that alcohol harm continues to be
addressed and this remit raises another aspect of the issue from a local
government perspective.
·
It was noted that there is a risk that responsible drinkers may
be inadvertently penalised and care should be taken that it is harmful levels
of drinking that is directly targeted.
·
It was noted the background to this remit may not be the
appropriate context to advise that “Hawke’s Bay is renowned as
wine region”.
Committee's recommendation
Councillors
Boag / Taylor
The Community Services Committee:
a.     Endorse
Hastings District Council’s Remit application to LGNZ about reducing alcohol
harm.
b.     Agree
that a DECISION OF COUNCIL is required urgently to allow the remit to
be submitted noting NCC’s endorsement at the April 2019 Zone 3 meeting.
Carried
Council Resolution
Councillors Jeffery / Hague
That Council:
a.     Endorse
Hastings District Council’s Remit application to LGNZ about reducing alcohol
harm.
Carried
4.      Hawke's
Bay Museums Trust Collection Management Agreement
Type of Report:
Contractual
Legal Reference:
N/A
Document ID:
715942
Reporting Officer/s &
Unit:
Antoinette Campbell, Director
Community Services
4.1   Purpose
of Report
To seek Council approval to enter
into a new Management Agreement with the Hawke’s Bay Museum Trust (HBMT)
for a period of one year while the Hawke’s Bay Regional Collection Joint
Working Group conducts its review of governance and operational arrangements of
how the collection is to be maintained and governed.
At the Meeting
There was no discussion on this item.
Committee's recommendation
Mayor
Dalton / Councillor Wright
The Community Services Committee:
a.     Approve
that Napier City Council enter into a one-year Management Agreement with the
Hawke’s Bay Museum Trust.
Carried
5.    Regional
Indoor Sports and Events Centre (RISEC) Trustees
Type of Report:
Procedural
Legal Reference:
N/A
Document ID:
709895
Reporting Officer/s &
Unit:
Antoinette Campbell, Director
Community Services
5.1   Purpose
of Report
To endorse the appointment of the
Regional Indoor Sports and Events Centre (RISEC) trustees for a further term of
three years.
At the Meeting
The following points were noted in discussion:
·
It is difficult to endorse a board structure which does not
reflect the diversity expected by Sport New Zealand – there are no
women or youth in the proposed trustees.
·
Sport Hawke’s Bay have already had it suggested that they
are not meeting the diversity expectations that Sport New Zealand see as
vital, as boards are strongly placed to set standards and model expected
behaviours.
·
Research shows that it was important to actively
“shoulder tap” a range of people who may have the skills required
but lack the confidence to put themselves forward.
·
Further information is to be sought on the process undertaken
with regards to the proposed board structure prior to the Council meeting
scheduled for 16 April 2019.
Officer’s
Recommendation
The Community Services Committee:
a.     Endorse
the Advisory Group’s recommendation to the RISEC Board to appoint the
current trustees for a further three-year term.
This Officer’s recommendation was laid on the
table, pending further information in relation to the lack of diversity in
the current proposed trustees.
6.    Safer
Napier programme - annual update
Type of Report:
Information
Legal Reference:
Enter Legal Reference
Document ID:
713588
Reporting Officer/s &
Unit:
Michele Grigg, Senior Advisor
Policy
6.1   Purpose
of Report
To provide a summary of the
2017/18 year of the Safer Napier programme, including key highlights and
benefits to Council and Napier.
At the Meeting
In response to questions from Councillors it was
advised that:
·
A number of different programmes are underway under the
umbrella of Safer Napier, including some with a more regulatory focus.
·
Neighbourhood Watch have a part to play in the wider approaches
to safety and have recently expanded their own offerings to include responses
to street safety, post-burglary support and so on.
Committee's recommendation
Councillors
Wright / Brosnan
The Community Services Committee:
a.     Note
the Safer Napier programme update.
Carried
Community Services Committee - 02 April
2019 - Open Minutes
PUBLIC EXCLUDED
ITEMS
Councillors Jeffery / Brosnan
That the public be excluded
from the following parts of the proceedings of this meeting, namely:
1.         Art
Deco Trust Loan Repayment
Carried
The general subject of each
matter to be considered while the public was excluded, the reasons for passing
this resolution in relation to each matter, and the specific grounds under
Section 48(1) of the Local Government Official Information and Meetings Act
1987 for the passing of this resolution were as follows:
General subject of each
matter to be considered.
Reason for passing this resolution
in relation to each matter.
Ground(s) under section
48(1) to the passing of this resolution.
1.  Art Deco Trust Loan
Repayment
7(2)(b)(ii) Protect information where the making
available of the information would be likely unreasonably to prejudice the
commercial position of the person who supplied or who is the subject of the
information
48(1)A That the public conduct
of the whole or the relevant part of the proceedings of the meeting would be
likely to result in the disclosure of information for which good reason for
withholding would exist:
(i) Where the local authority is named or specified in Schedule 1 of this
Act, under Section 6 or 7  (except 7(2)(f)(i)) of the Local Government
Official Information and Meetings Act 1987.
The meeting moved into committee
at 4.21pm
Approved and adopted as a true and accurate record of the
meeting.
Chairperson .............................................................................................................................
Date of approval ......................................................................................................................