ECONOMIC DEVELOPMENT COMMITTEE MEETING MINUTES 

18 JUNE 2019 

  MINUTES OF WAIROA DISTRICT COUNCIL 

ECONOMIC DEVELOPMENT COMMITTEE MEETING 

HELD AT THE COUNCIL CHAMBER, WAIROA DISTRICT COUNCIL, CORONATION SQUARE, WAIROA 

ON TUESDAY, 18 JUNE 2019 AT 1.30PM 

 

PRESENT: 

Cr  Denise  Eaglesome-Karekare  (Deputy  Mayor),  Ms  Angela  Thomas,  Ms 
Whaiora Maindonald 

IN ATTENDANCE:   K  Tipuna  (Pouwhakarae  –  Hapori/Whakatūtaki  Group  Manager  Community 
and  Engagement),  Annalena  Atzwanger  (Kaiurungi  Whakawhanake  Hapori 
Economic  Development  Officer),  Roz  Thomas  (Vision  Projects),  Courtney 
Hayward (Vision Projects), G Waikawa (Governance Officer – Kaiurungi Mana 
Arahi) 

 

1 

KARAKIA 

Karakia was given by K Tipuna. 

2 

APOLOGIES FOR ABSENCE  

APOLOGIES 

COMMITTEE RESOLUTION  2019/58  

Moved:  Ms Angela Thomas 
Seconded:  Ms Whaiora Maindonald 

That the apologies received from His Worship the Mayor, Councillor Min Johansen and Ms Karen 
Burger be accepted and leave of absence granted. 

CARRIED 

 

3 

None 

DECLARATION OF CONFLICT OF INTEREST 

4 

CHAIRPERSON’S ANNOUNCEMENTS 

None 

5 

LATE ITEMS OF URGENT BUSINESS 

Regional Wellbeing Report - Public Excluded 

Draft Te Wairoa e Whanake - Public Excluded 

6 

PUBLIC PARTICIPATION 

A maximum of 30 minutes has been set aside for members of the public to speak on any 
item on the agenda. Up to 5 minutes per person is allowed. As per Standing Order 14.14 

Page 1 

ECONOMIC DEVELOPMENT COMMITTEE MEETING MINUTES 

18 JUNE 2019 

requests to speak must be made to the meeting secretary at least one clear day before 
the meeting; however this requirement may be waived by the Chairperson. 

7 

MINUTES OF THE PREVIOUS MEETING 

COMMITTEE RESOLUTION  2019/59  

Moved:  Ms Whaiora Maindonald 
Seconded:  Cr Denise Eaglesome-Karekare 

That  the minutes  of  the  Economic  Development  Committee  Meeting  held  on  26 March  2019 be 
confirmed. 

CARRIED 

The Pouwhakarae – Hapori/Whakatūtaki Group Manager Community and Engagement advised 
work is still in progress for the Memorandum of Understanding with Wairoa District Council and 
Adventure Wairoa. 

  

8 

GENERAL ITEMS 

8.1 

THE GAIETY UPDATE 

COMMITTEE RESOLUTION  2019/60  

Moved:  Ms Angela Thomas 
Seconded:  Ms Whaiora Maindonald 

That the Committee receive the report. 

 

8.2 

UPDATE FROM THE GROUP MANAGER COMMUNITY & ENGAGEMENT 

COMMITTEE RESOLUTION  2019/61  

Moved:  Ms Angela Thomas 
Seconded:  Ms Whaiora Maindonald 

That the Economic Development Committee receives this report. 

CARRIED 

CARRIED 

The Chairperson gave an update on the Official opening of the Wairoa to Napier Rail Line and gave 
a  big  thank  you  to  Pouwhakarae  –  Hapori/Whakatūtaki|Group  Manager  Community  and 
Engagement and his economic development team for their outstanding work. 

 

COMMITTEE RESOLUTION  2019/62  

Page 2 

ECONOMIC DEVELOPMENT COMMITTEE MEETING MINUTES 

18 JUNE 2019 

Moved:  Ms Angela Thomas 
Seconded:  Ms Whaiora Maindonald 

That the public be excluded from the following parts of the proceedings of this meeting at 2.30pm. 
 
The  general  subject  matter  of  each  matter  to  be  considered  while  the  public  is  excluded,  the 
reason  for  passing  this  resolution  in  relation  to  each  matter,  and  the  specific  grounds  under 
section 48 of the Local Government Official Information and Meetings Act 1987 for the passing of 
this resolution are as follows: 
 

General subject of each matter 
to be considered 

9.1 – Regional Wellbeing Report 

9.2 – Te Wairoa e Whanake 

Reason for passing this 
resolution in relation to each 
matter 

s7(2)(b)(ii) - the withholding of 
the information is necessary to 
protect information where the 
making available of the 
information would be likely 
unreasonably to prejudice the 
commercial position of the 
person who supplied or who is 
the subject of the information. 

s7(2)(i) - the withholding of the 
information is necessary to 
enable Council to carry on, 
without prejudice or 
disadvantage, negotiations 
(including commercial and 
industrial negotiations) 

Ground(s) under section 48 for 
the passing of this resolution 

s48(1)(a)(i) - the public conduct 
of the relevant part of the 
proceedings of the meeting 
would be likely to result in the 
disclosure of information for 
which good reason for 
withholding would exist under 
section 6 or section 7 

s48(1)(a)(i) - the public conduct 
of the relevant part of the 
proceedings of the meeting 
would be likely to result in the 
disclosure of information for 
which good reason for 
withholding would exist under 
section 6 or section 7 

 

MOTION 

COMMITTEE RESOLUTION  2019/63  

Moved:  Ms Whaiora Maindonald 
Seconded:  Cr Denise Eaglesome-Karekare 

That the Committee moves out of Closed Committee into Open Committee at 3.00pm. 

CARRIED 

CARRIED 

 
Closing karakia given by K Tipuna 
 

The Meeting closed at 3.02pm. 

 

The minutes of this meeting were confirmed at the Economic Development Committee Meeting 
held on 30 July 2019. 

 

Page 3 

ECONOMIC DEVELOPMENT COMMITTEE MEETING MINUTES 

18 JUNE 2019 

 

 

 

 

................................................... 

CHAIRPERSON 

 

Page 4 

