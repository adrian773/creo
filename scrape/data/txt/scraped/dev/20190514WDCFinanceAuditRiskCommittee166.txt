FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

14 MAY 2019 

  MINUTES OF WAIROA DISTRICT COUNCIL 

FINANCE, AUDIT & RISK COMMITTEE MEETING 

HELD AT THE COUNCIL CHAMBER, WAIROA DISTRICT COUNCIL, CORONATION SQUARE, WAIROA 

ON TUESDAY, 14 MAY 2019 AT 1.30PM 

 

PRESENT: 

His  Worship  the  Mayor  Craig  Little  (Mayor),  Cr  Denise  Eaglesome-Karekare 
(Deputy Mayor), Cr Jeremy Harker, Mr Philip Jones 

IN ATTENDANCE:   Steven  May  (Tumu  Whakarae  Chief  Executive  Officer),  S  Mutch  (Partner- 
Auditors Ernst & Young), Gary Borg (Pouwhakarae – Pūtea/Tautāwhi Rangapū 
Group  Manager  Finance  and  Corporate  Support),  Stephen  Heath 
(Pouwhakarae  –  Hua  Pūmau  Hapori/Ratonga  Group  Manager  Community 
Assets and Services), G Waikawa (Kaiurungi Mana Arahi – Governance Officer) 

1 

KARAKIA 

Karakia was given by Cr Eaglesome-Karekare 

APOLOGIES FOR ABSENCE  

DECLARATION OF CONFLICT OF INTEREST 

2 

Nil 

3 

None. 

4 

CHAIRPERSON’S ANNOUNCEMENTS 

None. 

5 

LATE ITEMS OF URGENT BUSINESS 

The Chief Executive advised the committee an internal audit of the Contract Management Review 
will begin next week. 

6 

PUBLIC PARTICIPATION 

A maximum of 30 minutes has been set aside for members of the public to speak on any 
item on the agenda. Up to 5 minutes per person is allowed. As per Standing Order 14.14 
requests to speak must be made to the meeting secretary at least one clear day before 
the meeting; however this requirement may be waived by the Chairperson. 

7 

MINUTES OF THE PREVIOUS MEETING 

COMMITTEE RESOLUTION  2019/60  

Moved: 
Seconded:  Mr Philip Jones 

Cr Denise Eaglesome-Karekare 

That the minutes of the Ordinary Meeting held on 2 April 2019 be confirmed and the FAR  Work 
Plan will be part of the meeting. 

Page 1 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

14 MAY 2019 

CARRIED 

 

8 

GENERAL ITEMS 

8.1 

ANNUAL REPORT 2018-19 TIMETABLE AND AUDIT PLAN 

COMMITTEE RESOLUTION  2019/61  

Moved: 
Seconded:  Mr Philip Jones 

His Worship the Mayor Craig Little 

That the Committee receives the proposed timetable and audit plan for the Annual Report 2018-
19. 

CARRIED 

 
 
 

8.2 

HEALTH & SAFETY REPORT 

COMMITTEE RESOLUTION  2019/62  

Moved: 
Seconded:  Cr Jeremy Harker 

Cr Denise Eaglesome-Karekare 

That the Committee receive the report and Health & Safety risks to staff becomes part of the Zero 
Harm Officer’s report. 

CARRIED 

The Committee discussed the FAR Work Plan in detail. 

#  Date Entered  Action to be taken 

Responsible 

To be completed by 

Closed/Open 

1.  2 April 2019 

Top five H&S risks to staff and 
what 
to 
minimise them. 

is  being  done 

Kevin 
Stevenson 

14 May 2019 

Open 

- 

To  be  included  in  Zero 
Harm Officer’s report 

2.  2 April 2019 

3rd quarter financial results 

Gary Borg 

11 June 2019 

Open 

-  March 

financials 

to 

Council on 11 June 2019 

 

 

- 

April  financials  to  FAR 
committee  on  25  June 
2019 

3.  2 April 20-19 

Adopt  FAR  work  plan/TOR  as 
per 
FAR 
Committee minutes 

2019 

Feb 

26 

Gary Borg 

25 June 2019 

Open 

Steven May 

 

 

Page 2 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

14 MAY 2019 

 

 

- 

standing  agenda  item  for 
work plan 

 

16 July 2019 

Open 

4.  2 April 2019 

Detailed  work  plan  on  how 
Non-conformances 
from  EY 
audit report will be addressed 

Steven 
May/Gary 
Borg 

 

 

 

 

- 

establish  an  outstanding 
report  to  management 
register  with  a  regular 
update. 

 

5.  14 May 2019 

6.  14 May 2019 

Interim  Summary  from  EY  on 
Annual  Report  process  and 
FY18/19  close  off.    Liaise  with 
Stuart  Mutch 
request 
summary. 

to 

Gary 
Borg/Stuart 
Mutch 

Full draft Annual report NLT 17 
Sept  2019  to  be  reviewed  by 
FAR  before 
to 
Auditors 

submitting 

Gary 
Borg/Steven 
May 

16 July 2019 

Open 

16 July 2019 

Open 

17 Sept 2019 

Open 

7.  14 May 2019 

Contract Management Review 

Stephen 
Heath 

17 Sept 2019 

Open 

8.  14 May 2019  Draft Financial Statements 

Gary Borg 

17 Sept 2019 

Open 

 

 

9 

PUBLIC EXCLUDED ITEMS 

RESOLUTION TO EXCLUDE THE PUBLIC 

COMMITTEE RESOLUTION  2019/63  

Moved:  Mr Philip Jones 
Seconded:  Cr Jeremy Harker 

That the public be excluded from the following parts of the proceedings of this meeting at 2.32pm. 

The  general  subject  matter  of  each  matter  to  be  considered  while  the  public  is  excluded,  the 
reason  for  passing  this  resolution  in  relation  to  each  matter,  and  the  specific  grounds  under 
section 48 of the Local Government Official Information and Meetings Act 1987 for the passing of 
this resolution are as follows: 

General subject of each matter 
to be considered 

Reason for passing this 
resolution in relation to each 
matter 

Ground(s) under section 48 for 
the passing of this resolution 

9.1 - Auditor's Report to 
Management for the WDC 2018-
28 Long Term Plan 

s7(2)(a) - the withholding of the 
information is necessary to 
protect the privacy of natural 

s48(1)(a)(i) - the public conduct 
of the relevant part of the 
proceedings of the meeting 

Page 3 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

14 MAY 2019 

persons, including that of 
deceased natural persons 

would be likely to result in the 
disclosure of information for 
which good reason for 
withholding would exist under 
section 6 or section 7 

CARRIED 

 

COMMITTEE RESOLUTION  2019/64  

Moved: 
Seconded:  Cr Jeremy Harker 

Cr Denise Eaglesome-Karekare 

That the FAR Committee moves out of Closed Committee into Open Committee at 3.14pm. 

CARRIED 

 
Closing karakia was given by Crl Eaglesome-Karekare 

 

The Meeting closed at 3.15pm 

 

The  minutes  of this  meeting  were  confirmed  at  the  Finance,  Audit  &  Risk  Committee  Meeting 
held on 25 June 2019. 

 

 

 

 

................................................... 

CHAIRPERSON 

 

Page 4 

