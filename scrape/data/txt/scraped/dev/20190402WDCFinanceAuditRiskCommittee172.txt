FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

2 APRIL 2019 

  MINUTES OF WAIROA DISTRICT COUNCIL 

FINANCE, AUDIT & RISK COMMITTEE MEETING 

HELD AT THE COUNCIL CHAMBER, WAIROA DISTRICT COUNCIL, CORONATION SQUARE, WAIROA 

ON TUESDAY, 2 APRIL 2019 AT 1.30PM 

 

PRESENT: 

His  Worship  the  Mayor  Craig  Little  (Mayor),  Cr  Denise  Eaglesome-Karekare 
(Deputy Mayor), Cr Jeremy Harker, Mr Philip Jones 

IN ATTENDANCE:   Steven May (Tumu Whakarae Chief Executive Officer), Stuart Mutch (Auditor, 
Ernest  Young),  Gary  Borg  (Pouwhakarae  –  Pūtea/Tautāwhi  Rangapū  Group 
Manager Finance and Corporate Support), Russell Rodgers (Finance Manager), 
Lauren Jones (Kaikaute Putea/Financial Accountant), Gay Waikawa (Kaiurungi 
Tutohu  Governance  Officer), 
Kore 
Whakawhara/Zero Harm Officer). 

Stevenson 

(Kaiurungi 

Kevin 

 

1 

KARAKIA 

Karakia was given by Cr Denise Eaglesome-Karekare 

APOLOGIES FOR ABSENCE  

DECLARATION OF CONFLICT OF INTEREST 

2 

Nil 

3 

None 

4 

CHAIRPERSON’S ANNOUNCEMENTS 

The Chairperson welcomed Mr Russell Rogers, Finance Manager and also thanked him for applying 
for  the  role  and  also  welcomed  Ms  Lauren  Jones,  (Kaikaute  Putea/Financial  Accountant)  to  her 
first FAR Committee meeting. 

5 

LATE ITEMS OF URGENT BUSINESS 

None 

6 

PUBLIC PARTICIPATION 

A maximum of 30 minutes has been set aside for members of the public to speak on any 
item on the agenda. Up to 5 minutes per person is allowed. As per Standing Order 14.14 
requests to speak must be made to the meeting secretary at least one clear day before 
the meeting; however this requirement may be waived by the Chairperson. 

 

Mr  Caves  commented  and  asked  various  questions  on  Item  8.2  (External  Audit 
Management Report for the Year Ended 30 June 2018) & Item 8.3 (External Audit Plan for 
the Year Ending 30 June 2019) in the FAR Committee Agenda dated 2 April 2019. 

 

Page 1 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

2 APRIL 2019 

7 

MINUTES OF THE PREVIOUS MEETING 

COMMITTEE RESOLUTION  2019/56  

Moved: 
Seconded:  Mr Philip Jones 

Cr Denise Eaglesome-Karekare 

That the minutes of the Ordinary Meeting held on 26 February 2019 be confirmed. 

  

8 

GENERAL ITEMS 

8.1 

HEALTH & SAFETY REPORT 

COMMITTEE RESOLUTION  2019/57  

Moved: 
Seconded:  His Worship the Mayor Craig Little 

Cr Denise Eaglesome-Karekare 

That the Committee receive the report. 

CARRIED 

CARRIED 

 
The Zero Harm Officer presented his report and reported on Background, Current Situation being 
Hazard  Identification/Register/Accident  or  Near  Miss  Reports,  Site  Wise,  I  Auditor,  Joint 
Procurement, H & S Committee and Safety Audits Summary-July-March 2019. 
 
The Zero Harm Officer also gave a presentation on ecoPortal (Health and Safety, Risk Management 
Software). 
 
Various questions were asked of the Zero Harm Officer. 
 

8.2 

EXTERNAL AUDIT MANAGEMENT REPORT FOR THE YEAR ENDED 30 JUNE 2018 

COMMITTEE RESOLUTION  2019/58  

Moved: 
Seconded:  Mr Philip Jones 

Cr Jeremy Harker 

That the Committee receives the Audit Management Report for the Year Ended 30 June 2018. 

 
The  Pouwhakarae  –  Pūtea/Tautāwhi  Rangapū  Group  Manager  Finance  and  Corporate  Support 
presented his report.   
 
Attached to the report was the Audit Management Report 2018. 
 
 

CARRIED 

Page 2 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

2 APRIL 2019 

8.3 

EXTERNAL AUDIT PLAN FOR THE YEAR ENDING 30 JUNE 2019 

COMMITTEE RESOLUTION  2019/59  

Moved: 
Seconded:  Mr Philip Jones 

Cr Jeremy Harker 

That the Committee receives the External Audit Plan 2019. 

 
The  Pouwhakarae  –  Pūtea/Tautāwhi  Rangapū  Group  Manager  Finance  and  Corporate  Support 
presented his report and reported on the External Audit Plan for the Year Ending 30 June 2019. 
 

The Committee discussed an Action Plan to start tracking tasks and those responsible are prepared 
to talk to their action/s at the next FAR Committee. 

CARRIED 

 

Task 

Top five H&S risks to staff and what is being 
done to minimise them 

Completion 
date  

Responsible 

Open/Closed 

14 May 2019 

Kevin Stevenson  Open 

3rd quarter financial results 

14 May 2019 

Gary Borg 

14 May 2019 

Steve May 

Open 

Open 

Adopt FAR work plan/TOR as per 26 Feb 19 
FAR Committee minutes 

Detailed  work  plan  on  how  Non-
conformances  from  EY  audit  report  will  be 
addressed 

Interim Summary from EY on Annual Report 
process  and  FY18/19  close  off.  Liaise  with 
Stuart Much to request summary 

Full draft  Annual report NLT  3  Sep 19 to  be 
reviewed  by  FAR  before  submitting  to 
Auditors 

14 May 2019 

Steve  May/Gary 
Borg 

Open 

16 July 2019 

Gary 
Borg/Stuart 
Much  

Open 

3 Sep 2019 

Gary 
/Steve May 

Borg 

Open 

Page 3 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

2 APRIL 2019 

 

Closing karakia was given by Cr Denise Eaglesome-Karekare. 

 

 

 

 

 

 

 

The Meeting closed at 2.44pm. 

 

The  minutes  of this  meeting  were  confirmed  at  the  Finance,  Audit  &  Risk  Committee  Meeting 
held on 14 May 2019. 

 

 

 

 

................................................... 

CHAIRPERSON 

 

Page 4 

