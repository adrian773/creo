Minutes of Finance and Risk Committee Meeting - 26 March 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Finance and Risk Committee
Meeting
Date:
Tuesday, 26 March 2019
CG-14-71-00032                                                                         1
Minutes
of a Meeting of the Finance and Risk Committee held on
Tuesday,
26 March 2019 at 1.05pm
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  2
7.         Hawke's
Bay Airport Limited Half Year Report and 2019/20 Statement of Intent 2
8.         Hawke's
Bay Regional Sports Park Trust Draft Half Year Report to 31 December 2018  2
4.         Financial
Quarterly Report for the six months ended 31 December 2018  3
5.         Local
Government Funding Agency - Half Year Report to 31 December 2018 and Draft
2019/20 Statement of Intent 4
6.         Hawke's
Bay Museums Trust Half Year Report and Draft 2019/20 Statement of Intent 4
9.         Additional
Business Items  5
10.       Extraordinary
Business Items  5
CG-14-71-00032                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE Finance
and Risk Committee
HELD
IN THE Council
Chamber, Ground Floor, Civic Administration Building, Lyndon Road East,
Hastings
ON
Tuesday, 26
March 2019 AT 1.05pm
Present:                          Chair: Councillor Travers
Mayor Hazlehurst
Councillors
Dixon, Harvey, Heaps, Kerr (Deputy Chair), Lyons, Nixon, O’Keefe,
Poulain, Redstone and Watkins
IN
ATTENDANCE:             Chief Executive (Mr N Bickle)
Chief Financial
Officer (Mr B Allan)
Financial
Controller (Mr A Wilson)
Manager Strategic
Finance (Mr B Chamberlain)
Group Manager:
Asset Management (Mr C Thew)
Manager:
Democracy and Governance Services (Mrs J Evans)
Committee
Secretary (Mrs C Hilton)
ALSO PRESENT:              CE, Hawke’s Bay Airport Limited (Mr S Ainslie)
Interim
Chief Financial Officer (Ms R Orchard)
Commercial
and Business Development Manager (Mr D Smith)
CE,
Hawke’s Bay Regional Sports Park Trust (Mr J Mackintosh)
1.         Apologies
Councillor
Lyons/Councillor Dixon
That
apologies for absence from Councillors Barber, Lawson and Schollum and apologies
for lateness from Councillors Kerr and Nixon, be accepted.
CARRIED
2.         Conflicts
of Interest
Councillor
Kerr:
·
Item 8 - Hawke's Bay Regional Sports Park Trust Draft Half Year
Report To 31 December 2018
Councillor
Lyons:
·
Item 6 - Hawke's Bay Museums Trust Half Year Report and Draft
2019/20 Statement of Intent
3.         Confirmation
of Minutes
Councillor Dixon/Councillor O'Keefe
That the
minutes of the Finance and Risk Committee Meeting held Tuesday
13 November 2018 be confirmed as a true and correct record and be adopted.
CARRIED
Councillor
Kerr joined the meeting at 1.07pm.
With the
agreement of the Chair and Finance and Risk Committee, items 7 and 8 were taken
out of order and addressed - as representatives from the Hawke’s Bay
Airport Limited and the Hawke's Bay Regional Sports Park Trust were present to
address these respective items.
7.
Hawke's Bay Airport
Limited Half Year Report and 2019/20 Statement of Intent
(Document 19/192)
The Chief Executive (Mr S Ainslie), Interim Chief Financial Officer (Ms R Orchard) and the Commercial
and Business Development Manager (Mr D Smith) of the
Hawke’s Bay Airport Limited gave a power point presentation (CG-14-71-00035) addressing the Half Year report and Statement of
Intent.  They responded to questions from the committee.
Councillor Nixon joined the meeting at
2.00pm.
Mayor Hazlehurst/Councillor Heaps
A)        That
the report of the Manager Strategic Finance titled “Hawke's
Bay Airport Limited Half Year Report and 2019/20 Statement of Intent”
be received.
B)        That the Hawke’s Bay Airport Limited Half Year report to 31
December 2018 be received.
C)        That the 2019/20 Draft Statement of Intent of Hawke’s Bay
Airport Limited be received with any feedback provided to the Hawke’s
Bay Airport Ltd Board.
CARRIED
Councillor Nixon
withdrew from the meeting at 2.07pm.
8.
Hawke's Bay Regional
Sports Park Trust Draft Half Year Report to 31 December 2018
(Document 19/197)
Councillor Kerr had previously
declared a conflict of interest in regard to this item and took no part in
discussion or voting.
The Chief Executive (Mr J Mackintosh)
of the Hawke’s Bay Regional Sports Park Trust, gave a power point
presentation (CG-14-71-00036) addressing the Half
Year report and responded to questions from the committee.
Councillor Nixon rejoined the
meeting at 2.20pm.
Following discussion on this item, it
was noted that the Finance and Risk Subcommittee did not have delegated
authority to address the issue of bridge funding and any such recommendations
would need to be referred to the Council meeting (on 28 March 2019) for
consideration as part of the Annual Plan Process considerations.
Councillor Lyons/Councillor Nixon
A)        That
the report of the Manager Strategic Finance titled “Hawke's
Bay Regional Sports Park Trust Draft Half Year Report to 31 December 2018”
dated 26/03/2019 be received.
B)        That the Hawke’s Bay Regional Sports Park Trust Half Year
Report to 31 December 2018 be received.
C)        That
the Council receives the Trust's submission to the Council’s 2019-20
Annual Plan for consideration in June 2019.
D)        That the Finance and
Risk Committee recommend to Council that $150,000 be made available to the
Hawke’s Bay Community Fitness Centre Trust as bridge funding for a
period of up to two years to enable the completion of the Hawke's Bay
Regional Sports Park carpark upgrade.
CARRIED
4.
Financial Quarterly Report for the six months ended
31 December 2018
(Document 19/189)
Councillor Kerr rejoined the meeting at this point.
The Financial Controller, Mr A
Wilson, spoke to a power point presentation (CG-14-71-00034) and responded to
questions from the committee.
Councillor Kerr/Councillor Dixon
That the report of the Financial
Controller titled “Financial Quarterly Report for the six months
ended 31 December 2018” dated 26/03/2019 be
received.
CARRIED
________________________
The
meeting adjourned for afternoon tea at 3.00pm
and
resumed at 3.10pm
________________________
Councillor
Harvey did not rejoin the meeting at this point.
5.
Local Government Funding Agency - Half Year Report
to 31 December 2018 and Draft 2019/20 Statement of Intent
(Document 19/190)
Councillor Nixon/Councillor Lyons
A)        That
the report of the Manager Strategic Finance titled “Local
Government Funding Agency - Half Year Report to 31 December 2018 and Draft
2019/20 Statement of Intent” dated 26/03/2019 be
received.
B)        That the New Zealand Local Government Funding Agency Limited Half
Year Report to 31 December 2018 be received.
C)        That the New Zealand Local Government Funding Agency Limited Draft
2019/20 Statement of Intent be received and that Council provide positive
feedback on a well written Statement of Intent to the New Zealand Local
Government Agency Board.
CARRIED
6.
Hawke's Bay Museums Trust Half Year Report and
Draft 2019/20 Statement of Intent
(Document 19/191)
Councillor Lyons had previously
declared a conflict of interest in regard to this item and left the meeting
at 3.15pm.
Initial Motion
Councillor Kerr/Councillor Poulain
A)   That the
report of the Manager Strategic Finance titled
“Hawke's Bay
Museums Trust Half Year Report and Draft 2019/20 Statement of Intent” dated 26/03/2019 be received, and that Council recommend to the Hawke’s Bay
Museums Trust that the Statement of Intent should be altered to include the
KPI’s as recommended by the Rationale Report.
B)   That the Hawke’s Bay Museums Trust Half Year Report for the
six months ended 31 December 2018 be received.
The Motion was PUT and LOST
Councillor
Heaps/Councillor Dixon
A)   That the
report of the Manager Strategic Finance titled
“Hawke's Bay
Museums Trust Half Year Report and Draft 2019/20 Statement of Intent” dated 26/03/2019 be received, and that Council provide feedback to the
Hawke’s Bay Museums Trust and/or the Joint Working Group that the
Statement of Intent should be altered to include the KPI’s as
recommended by the Rationale Report.
B)   That the Hawke’s Bay Museums Trust Half Year Report for the
six months ended 31 December 2018 be received.
CARRIED
Agenda Items
7 and 8 had previously been taken out of order and addressed.
9.         Additional Business Items
There were no additional business items.
10.       Extraordinary
Business Items
There were no extraordinary business items.
________________________
The meeting closed at 3.43pm
Confirmed:
Chairman:
Date: