Minutes of Temporary Road Closures Subcommittee Meeting - 29 00 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Temporary Road Closures
Subcommittee
Meeting
Date:
Thursday, 29 August 2019
CG-14-16-00333                                                                         1
Minutes
of a Meeting of the Temporary Road Closures
Subcommittee held on
Thursday,
29 August 2019 at 8.45am
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Conflicts
of Interest 1
3.         Confirmation
of Minutes  1
4.         Temporary
Road Closures - Mokamoka Road - 1 September 2019, Annual Blossom Parade - 14
September 2019  1
5.         Additional
Business Items  3
6.         Extraordinary
Business Items  3
CG-14-16-00333                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE Temporary
Road Closures Subcommittee HELD IN THE Guilin Room, Ground Floor, Civic
Administration Building, Lyndon Road East, Hastings
ON Thursday, 29 August 2019 AT 8.45am
Present:                          Councillor Nixon (Chair)
Councillor Watkins (Deputy Chair)
Transportation Manager – Mr J Pannu
Environmental Consents Manager – Mr M Arnold
IN ATTENDANCE:             Transportation Officer (Mrs L Burden)
Democracy & Governance Advisor (Mrs C Hilton)
1.         Apologies
There were no
apologies.
2.         Conflicts
of Interest
There were no declarations
of conflicts of interest.
3.         Confirmation
of Minutes
Councillor Nixon/Councillor Watkins
That the minutes of the Temporary Road
Closures Subcommittee Meeting held Thursday 20 June
2019 be confirmed as a true and correct record and be adopted.
CARRIED
4.
Temporary Road Closures - Mokamoka Road - 1
September 2019, Annual Blossom Parade - 14 September 2019
(Document 19/870)
Councillor Watkins/Mr Arnold
A)        That the Committee receives the report titled  Temporary Road Closures -
Mokamoka Road - 1 September 2019, Annual Blossom Parade - 14 September 2019
B)        That the Committee approve the following temporary road closures  as no submissions
have been received in respect of these applications:
1.       The Hawke’s Bay Car Club (HBCC) - Mokamoka Road from
RP No.1000 to RP No. 9000. The road closure will be between 9:00am and
5:00pm, Sunday, 1 September 2019.
2.       Creative Hastings - Annual
Blossom Parade to be held on Saturday, 14 September 2019.
Parade
Set-up Area
6:00am –
3:00pm
Russell
Street South from Eastbourne Street East to
Lyndon Road East
Lyndon Road
East from Russell Street South to Hastings Street
South
Warren
Street South from Lyndon Road East to the
entrance to the Library car park
Parade
Route
7:00am –
1:00pm
Heretaunga
Street West from Nelson Street South to Market
Street South
Heretaunga
Street East from Russell Street North to Warren
Street South
10:00am
– 1:00pm
Eastbourne
Street West from Market Street South to Russell
Street South
Queen
Street West from Russell Street North to Nelson
Street South
Nelson
Street South from Queen Street West to Heretaunga
Street West
Market
Street South from Heretaunga Street West to
Eastbourne Street West
10:00am
– 3:00pm
Karamu Road
North from Heretaunga Street East to Eastbourne
Street East
Eastbourne
Street East from Russell Street to Warren Street
South
C)        The Committee approve the above temporary road closures subject to
the following conditions to be complied with to the satisfaction of the Group
Manager: Asset Management:
i.        These events are conducted in accordance with the New
Zealand Transport Agency Code of Practice Temporary Traffic Management
(CoPTTM).
ii.       The Traffic Management Plans including provision for
appropriate signage are approved by the Traffic Management Coordinator acting
under delegated authority.
iii.      The Traffic Management Plans must be complied with
including any specific conditions.
iv.      Copies of the relevant liability insurance policies are
received.
v.       That the cost of all advertising is met by the event
organisers.
vi.      Emergency Services are contacted regarding the holding
of these events with details of the dates, locations and time frames.
vii.     As per the Traffic Management Plan provisions - all emergency
services will be accommodated and access provided through the sites as
required.
viii.    The
applicant is responsible under the Health and Safety at Work Act 2015 for all
health and safety risks associated with this activity and must take
reasonably practicable steps to ensure the safety of all persons during these
temporary road closures.
ix.      The applicant has in place at all times, appropriate
Health and Safety measures (to prevent harm to any persons), including (but
not limited to) any measures provided for in the submitted Health and Safety
Plans including any conditions attached.
x.       These Hawke’s Bay Car Club events shall be
conducted in accordance with the requirements of the Motorsport New Zealand
Motor Sport Manual, National Sporting Code and Appendices.
CARRIED
5.         Additional Business Items
There were no additional business items.
6.         Extraordinary
Business Items
There were no extraordinary business items.
________________________
The meeting closed at 8.51am
Confirmed:
Chairman:
Date: