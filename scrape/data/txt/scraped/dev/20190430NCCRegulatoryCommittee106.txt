Minutes of Regulatory Committee - 30 April 2019
Regulatory Committee
Open Minutes
Meeting Date:
Tuesday
30 April 2019
Time:
3.00pm
- 3.11pm
Venue
Council
Chamber
Hawke's Bay Regional Council
159 Dalton Street
Napier
Present
Councillor Taylor (In the Chair), Acting Mayor
White, Councillors Boag, Brosnan, Dallimore, Hague, Jeffery, McGrath, Price,
Tapine and Wright
In Attendance
Chief Executive, Director Corporate Services,
Director Community Services, Director Infrastructure Services, Director City
Strategy, Manager Communications and Marketing, Manager Building Consents,
Manager Regulatory Solutions/ Business Excellence & Transformation
Administration
Governance Team
Regulatory Committee - 30 April
2019 - Open Minutes
Apologies
Acting Mayor White / Councillor Brosnan
That the apologies from Mayor Dalton and Councillor Wise
be accepted.
Carried
Conflicts of
interest
Councillor Jeffery declared an interest in agenda item 1 and
in order to manage this interest requested his Deputy to act as presiding
member for the meeting and did not participate in the vote.
Councillor Wright declared an
interest in agenda item 2 as the family in question are personal family
friends. It was not considered that this interest required active management in
this meeting.
Announcements by
the Acting Mayor
The Acting Mayor noted that she has spoken to the Headmaster
of Puketapu School to offer Council’s support to the local community following
the tragic accident over the weekend, and advised that a “Give a little”
page will be created shortly for the family by the school.
Announcements by
the Chairperson
Nil
Announcements by
the management
The Chief Executive advised that Rachael Horton has been
appointed the new Manager Regulatory Solutions and will be transferring from
her current role of Manager Business Excellence and Transformation.
Confirmation of minutes
Councillors Boag / Wright
That the
Minutes of the meeting held on 19 February 2019 were taken as a true and
accurate record of the meeting.
Carried
Regulatory Committee - 30 April
2019 - Open Minutes
Agenda
Items
1.    Earthquake-Prone
Buildings - Identification of Priority Buildings - Consultation
Type of Report:
Legal
Legal Reference:
Building Act
2004
Document ID:
726241
Reporting Officer/s &
Unit:
Malcolm Smith, Manager
Building Consents
1.1   Purpose
of Report
To advise Council of the
requirements under the Building Act 2004 in relation to the identification of
priority buildings under the earthquake-prone building legislation, and to seek
approval to release the draft Statement of Proposal for public submissions
prior to adoption by Council.
At the Meeting
The Manager Building Consents gave a broad overview
noting that Council is meeting legislative requirements to consult on this
matter. Council officers have initially identified priority areas of risk, and
the draft Statement of Proposal has been prepared for public consultation to
ensure that all priority areas have been included.
In response to questions from Councillors the
following points were clarified:
·
Maraenui and Greenmeadows are not identified as priority areas
as Council officers do not believe that any unreinforced masonry buildings
are located in those areas. It is anticipated that any areas that the public
believe have been incorrectly assessed or overlooked will be identified through
the consultation process.
·
No priority buildings have been identified at this stage. Once
the priority areas have been confirmed, the priority buildings within these will
be identified and building owners will be contacted at that time. Most
building owners will already know whether they are likely to be affected or
not.
·
Due to the 1931 Earthquake, Napier’s building stock is
relatively modern and a number of buildings have already been assessed and
upgraded.
·
Council officers will liaise with the Business Associations located
in the identified priority areas, the local branch of Engineering NZ will
engage with Historic Places and Art Deco Trust (if necessary) once the
priority buildings have been identified.
·
Owners of heritage buildings may be able to apply for some
dispensations under the Building Act.
Committee's recommendation
Councillors
Brosnan / Hague
That the Regulatory Committee:
a.     Approve
the release of the draft Statement of Proposal for public submissions.
Councillor Jeffery did not participate in the vote
due to a declared interest
Carried
2.    Street
Naming - 250 Guppy Road Taradale Napier
Type of Report:
Procedural
Legal Reference:
N/A
Document ID:
723801
Reporting Officer/s &
Unit:
Paul O'Shaughnessy, Team
Leader Resource Consents
2.1   Purpose
of Report
The purpose of this report is to
obtain Councils approval for one new street name to replace a previously
approved street name within the recently approved residential subdivisions at
250 Guppy Road. The street in question has already been subject to a previous
street name approval by Council (Chue Court), however a mistake by the
developer has led to a request for a re-naming to Gee Place.
At the Meeting
Councillors agreed that Gee Place was an appropriate
name for the street.
Committee's recommendation
Acting
Mayor White / Councillor Tapine
That the Regulatory Committee:
a.     Approve
one new street name at 250 Guppy Road as follows:
·      Gee
Place-250 Guppy Road
b.     That
a DECISION OF COUNCIL is required urgently as Council have recently
issued Section 224 certification for the subdivision at 250 Guppy Road and
the developers require certainty for the purposes of marketing, physical
street naming and property addressing.
Carried
Council Resolution
Councillors Brosnan / Hague
That Council:
a.     Approve
one new street name at 250 Guppy Road as follows:
·      Gee
Place-250 Guppy Road
Carried
The meeting closed at 3.11pm.
Approved and adopted as a true and accurate record of the
meeting.
Chairperson .............................................................................................................................
Date of approval ......................................................................................................................