Minutes of Clifton to Tangoio Coastal Hazards Strategy Joint Committee -
18 March 2019
Unconfirmed
MINUTES OF A
meeting of the Clifton to Tangoio Coastal Hazards Strategy Joint Committee
Date:                          Monday 18 March 2019
Time:                          10.00am
Venue:
Council Chamber
Hawke's Bay Regional Council
159 Dalton Street
NAPIER
Present:                     Cr Peter Beaven – HBRC - Chair
Cr Tony Jeffery
– NCC – Deputy Co Chair
Cr Tania Kerr _HDC
– Deputy Co-Chair
Cr
Paul Bailey – HBRC
Cr
Larry Dallimore – NCC
Cr Annette Brosnan
– NCC
Cr Malcolm Dixon
– HDC
Cr Ann Redstone –
HDC
Tania Hopmans –
Maungaharuru Tangitū Trust
Tania Huata –
Mana Ahuriri Trust
Alternates:                Mayor Bill Dalton – NCC
Cr Rod Heaps - HDC
In Attendance:          Simon
Bendall – Mitchell Daysh
Chris Dolley –
HBRC
Mark Clews –
HDC
Craig Goodier –
HBRC
Trudy Kilkolly
– HBRC
Drew Broadley –
HBRC
Brent Chamberlain
– HDC
Caroline Thomson
– NCC
Pieri Munro –
HBRC
A Roets –
Governance Administration Assistant
1.       Welcome/Apologies/Notices
The Chair welcomed
everyone to the first Hazards Committee meeting for 2019 and in light of
Friday’s events in Christchurch, the Chair advised that the meeting will
commence appropriately with a karakia.
Pieri Munro acknowledged
those whom had lost their lives in the event and those still recovering in
hospital and offered a karakia.  A moment of silence, out of respect and
solidarity towards the victims and their family, was given.
CLI103/19     Resolution:
That apologies
for Mayor Sandra Hazlehurst be accepted.
Kerr/Redstone
CARRIED
Secretarial
note; the absence of Peter Paku and Cr Neil Kirton was noted.
The Chair noted
that the Hauraki District Council are currently visiting Hawke’s Bay to
learn about the Strategy and other work. They will be joining for lunch for an
informal discussion, ahead of a field trip to the Cape Coast area, hosted by
the Chair, Cr Peter Beaven and Simon Bendall.
2.       Conflict
of Interest Declarations
There was no
Conflict of Interest Declarations.
3.       Confirmation of Minutes of the Clifton to
Tangoio Coastal Hazards Strategy Joint Committee held on 10 December 2018
CLI104/19
Resolution:
Minutes of the Clifton to Tangoio Coastal
Hazards Strategy Joint Committee held on Monday, 10 December 2018, a copy
having been circulated prior to the meeting, were taken as read and
confirmed.
Hopmans/Bailey
CARRIED
4.
Actions
from Previous Clifton to Tangoio Coastal Hazards Strategy Joint Committee
The Terms of Reference was adopted by
Napier City Council on 11 December 2018 and the Chair noted that the TOR have
now been adopted by all three Councils respectively.
An electronic link to the OECD case study
has been circulated to the Joint Committee Members.  A PDF-version will
be circulated once received from OECD.
CLI105/19
Resolution:
That the Clifton to Tangoio Coastal
Hazards Strategy Joint Committee receives and notes the “Actions
from previous Clifton to Tangoio Coastal Hazards Strategy Joint Committee
Meetings” report.
Brosnan/Redstone
CARRIED
5.
Call
for Items of Business Not on the Agenda
Recommendations
That the Clifton to Tangoio Coastal Hazards Strategy Joint Committee accepts the following “Minor Items of Business Not
on the Agenda” for discussion as Item
9.
Item
Topic
Raised
by
1.
Climate rally
Cr P Bailey
2.
Update on this
Committee’s work to all three Councils
Cr P Beaven
6.
Formal
Confirmation of Member Organisations’ Appointments and 2018 Terms of
Reference Adoption
The Formal confirmation of Member
organisations’ appointments were not included in the Agenda pack of the
TOR themselves.
Other than the outstanding appointment of
Napier City Council’s member to this Committee, all other matters have
been addressed and resolved.
CLI106/19
Resolution
1.     That the Clifton to Tangoio Coastal
Hazards Strategy Joint Committee receives the “Formal
Confirmation of Member Organisations’ Appointments and 2018 Terms of
Reference Adoption” report, noting those appointments are:
1.1.   Councillors Tania Kerr, Ann Redstone and Malcolm Dixon
representing Hastings District Council
1.2.   Councillors Peter Beaven, Paul Bailey and Neil Kirton representing
Hawke’s Bay Regional Council.
Bailey/Kerr
CARRIED
7.
Project
Manager's Update
Simon Bendall gave an update on the
report and highlighted two specific matters for consideration, being the
revised Stage 4 Project Plan and the Central Government Engagement.
Discussions traversed:
·
TAG had a meeting a month ago to check the
progress for this project.
·
The Design Workstream requires additional time
to complete due to the sheer complexity of these tasks and the resources
available to complete them.
·
Deliverables from the Design Workstream
include preliminary designs for each short term action of the recommended
pathways, a recommended order and priority of works, and updated costings.
·
The commencement of public consultation under
the LGA has been delayed from the first quarter of 2020 to the first quarter
of 2021.
·
The effect of this is that public consultation
on Strategy implementation will now align with public consultation on the
next review of the Council Long Term Plans.
·
Proposed that TAG bring back a report to the
Joint Committee to explore options for an aligned consultation process and in
how to manage risks e.g. consultation fatigue.
·
The Joint Committee has on a number of
occasions emphasised the need to engage with Central Government and this has
been in progress through TAG.
·
The project team have been invited to present
to the Community Resilience Working Group which is a cross-Agency group that
involves Treasury, Ministry for the Environment, Department of Internal
Affairs, Department of Conservation and others.
·
Also in discussions with LGNZ who are keen to
assist with local government issues more broadly in this space – will
keep committee updated as it progresses.
·
The Joint Committee has sent a letter inviting
Minister of Climate Change Hon. James Shaw to discuss how central government
can assist and engage in strategy development.
·
Contact has also been had with LGNZ President
Dave Cull – suggested that Mr Cull could attend the presentation with
James Shaw.
CLI107/19
Resolution:
That the Clifton to Tangoio Coastal Hazards Strategy Joint Committee
receives the “Project Manager’s Update”
report.
Redstone/Jeffery
CARRIED
8.
Current
Coastal Projects Update
The TAG group gave an update on the
following matters:
·
Whakarire Ave Revetment works:  Simon
Bendall reported that the Council has made an initial decision on how to
allocate the funding of this work and Napier City Council are now engaging
with those affected landowners.
·
Mark Clews reported that the Revetment works
at Clifton are essentially completed.
·
Port of Napier:  Chris Dolley gave a
comprehensive update at the last meeting on the consenting process and gave a
briefing around the discharge zone closer to the beach.  Craig Goodier
reported that the Port are engaging their pre-consent conditions for the wharf
and the dredging.  HBRC are engaging with the Planner to assist with a
consent for possibly dumping material in the 750m exclusion zone. HBRC has
completed a Bathometric survey and are progressing.
CLI108/19
Resolution:
That the Clifton to Tangoio Coastal Hazards Strategy Joint Committee
receives the “Current Coastal Project Update”
report.
Dallimore/Bailey
CARRIED
The Chair suggested that there is no need
for the Workshop on the funding workstream to be held as a publicly excluded
session. Following discussion, and by agreement with the Joint Committee, the
workshop was incorporated into the meeting.
10.
WORKSHOP
on funding workstream – NOTES
CLI109/19
The Chair introduced the item and noted that the aim of this work
is to develop an agreed funding model for Strategy implementation, ultimately
to be presented to the Partner Council’s for approval.
Brent
Chamberlain and Caroline Thomson gave a presentation on the Funding
Workstream highlighting:
·
A recap of 10 December meeting stated that
there were no guidance from Central Government on how to fund the
implementation of a strategy of this type.
·
Initially, three models were presented to the
Joint Committee i.e
o
Single Agency HBRC: HBRC to
rate/fund all implementation activity;
o
Single Agency TA: TA to rate/fund
all implementation activity; (which is now been discounted) and
o
A hybrid type model in which
rating/funding the activity shared between HBRC an TAs
·
At the last workshop, initial indications were
that 50% of the Joint Committee members favoured the hybrid model and 50%
favoured that HBRC to rate/fund all implementation activity. The committee
indicated support to further explore the Hybrid model.
Further discussion traversed:
·
Regional Council is the only Council that can
rate across both Napier and Hastings districts and there is strong support
that the public good be spread beyond the TA boundaries.
·
The Contributory Fund is recommended to be set
up and aims to grow a fund over time that can pay for Strategy
implementation.  It is ring fenced for transparency and reporting.
·
Legal advice obtained from Simpson Grierson
dated 20 December 2018 suggests that Councils can rate in advance for
activities with uncertain timing and details, providing that the community is
fully consulted on what steps will be taken in the event that the money is
not used for its intended purposed – e.g. a fund collected for a sea
wall which fails to gain resource consent.
·
A contributory fund has been generally
supported by all parties.
·
Collecting from non-ratepayer beneficiaries
was discussed a key component of the funding model.  The identified
organisations have yet to be approached and there is no legal mechanism to
enforce a contribution.
·
Based on the discussions today the TAG funding
subgroup would further refine the funding model and have the model externally
peer reviewed.
·
Proposed to have an update/progress on Central
Government funding.
Dixon/Hopmans
CARRIED
9.
Discussion
of Items Not on the Agenda
Item
Topic
Raised
by
1.
Climate rally:
·
Attended the school on Climate Change.
·
The region is contributing a lot dealing with Climate Change.
·
To Communicate this better in what the region is doing in
terms of Climate Change.
Cr P Bailey
2.
Update on this
Committee’s work to all three Councils (HBRC, NCC and HDC):
·
A one page update will be provided to each Council on the
progress on the Committee’s work herein.
Cr P Beaven
Closure:
There
being no further business the Chairman declared the meeting closed at 11.56am
on Monday, 18 March 2019.
Signed
as a true and correct record.
DATE: ................................................               CHAIRMAN:
...............................................