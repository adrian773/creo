Minutes of Environment and Services Committee - 19 June 2019
Unconfirmed
MINUTES OF A
meeting of the Environment and Services Committee
Date:                          Wednesday 19 June 2019
Time:                          11.00am
Venue:
Council Chamber
Hawke's Bay Regional Council
159 Dalton Street
NAPIER
Present:                     T Belford - Chairman
P Bailey
R Barker
P Beaven
A J Dick
R Graham
B Gregory
N Kirton
P Paku
F Wilson
In Attendance:          J Palmer – Chief Executive
I Maxwell –
Group Manager Integrated Catchment Management
T Skerman –
Group Manager Strategic Planning
C Dolley – Group
Manager Asset Management
P Munro – Te
Pou Whakarae Māori Partnerships
A Roets – Governance
Administration Assistant
M Mohi – Māori
Committee Chairman
L Lambert –
Group Manager Regulation
B Bisset – Extinction Rebellion Aotearoa
M Heaney – Manager
Client Services
C Lambourne – Project Manager, Power to the People
B Rangi – Representative from the Presbyterian Church
S Gilmer – FEMP
Project Coordinator
D Evans – Catchment
Manager Tukituki/Southern Coasts
B Powell – Catchment
Manager (Central)
N Heath – Catchment
Manager (Wairoa/Mohaka)
C Leckie – Manager
Catchment Services
1.       Welcome/Apologies/Notices
The Chair
welcomed everyone to the meeting and Pieri Munro opened with a karakia.
Resolution
ESC14/19     That the
apology for absence from Cr Debbie Hewitt be accepted.  The Chair advised
that Cr Hewitt will be joining the meeting via audio link for parts of the
Agenda.
Crs Rex Graham
and Rick Barker advised that they need to leave by 4.00pm.
Beaven/Barker
CARRIED
2.       Conflict
of Interest Declarations
There were no
Conflict of Interest declarations.
3.       Confirmation
of Minutes of the Environment and Services Committee meeting held on 10 April 2019
ESC15/19
Resolution
Minutes of the Environment and Services Committee meeting held on
Wednesday, 10 April 2019, a copy having been circulated prior to the meeting,
were taken as read and confirmed as a true and correct record.
Kirton/Bailey
CARRIED
4.
Follow-ups from Previous Environment and
Services Committee Meetings
The Executive Team assessment of
strategic elements of TLA wastewater management will be included in the
annual Compliance report as requested, and a request was made for the report
to also include an analysis of engagement scheduled between staff of
respective councils and the results of the regional Three Waters Review assessments
of the current state of infrastructure.
Clive River Dredging programme is in early
stages with $1m having been budgeted for the project.
MPI review of the NPS for Plantation
Forestry should be complete by December 2019, with changes to be made to the
NES mid 2020.
ESC16/19
Resolution
That the Environment and Services Committee receives
and notes the report “Follow-up Items from Previous Environment
& Services Committee Meetings”.
Beaven/Barker
CARRIED
5.
Call for Minor Items of Business Not on
the Agenda
Recommendation
That the Environment and
Services Committee accepts the following “Minor Items of Business Not on the Agenda” for discussion as item
18.
Topic
Raised
by
Managed Aquifer
recharge project
Cr P Bailey
Natural reforestation
in Wairoa
Cr R Graham & Cr P
Bailey
Future Farming Initiative
Cr T Belford
6.
Extinction Rebellion Aotearoa - Call to
Declare Climate and Ecological Emergency
Bruce Bisset spoke on behalf of the Extinction Rebellion Aotearoa,
with discussions traversing:
·
Greenhouse gas concentrations, global average
temperatures and sea levels are all rising sharply
·
Impacts of climate change and the need to
advocate to the Minister for the Environment to declare a State of Emergency
on Climate Change
·
Start acting now and invest in the research
and infrastructure required to remove carbon dioxide from the atmosphere with
the utmost urgency, and advocate to Central Government for same
·
Proposed to evaluate climate change hazards
and risks and make climate change adaption plans as climate change will be
much more severe in future
·
New Zealand will be unprepared for the impacts
of climate change
·
Recommendations proposed request that MfE
issues a climate change planning and adaptation guidance note explaining the
limitations of the IPCC climate change projections, creates a structure for
NZ climate science advice and updates its climate change website with
information on the potential for consequences of severe, abrupt climate
change for each region in NZ
·
Requests Central Government provides regional
councils with statutory powers to severely limit and adequately mitigate any
further increase in short and long-lived greenhouse gas emissions and to
fast-track initiatives to draw-down atmospheric carbon dioxide
·
Advocate for amendment of the RMA to include greenhouse
gas emissions as a consideration for consents granted under the Act
·
Significantly shift to fuel efficient rail for
shipment and promote public transport to further generate net carbon savings
ESC17/19
Resolution
That the
Environment and Services Committee receives and notes the “Extinction
Rebellion Aotearoa - Call to Declare Climate and Ecological Emergency”
Barker/Graham
CARRIED
7.
Climate Change Mitigation and
Adaptation - A Regional Response
Tom Skerman introduced the item, highlighting that this item was
prepared prior to receipt of the Extinction Rebellion
Aotearoa request. Discussions traversed:
·
Many work programmes across Council directly
and indirectly addressing climate change adaptation and mitigation
·
Some additional local government approaches
and initiatives for Council to consider
·
There have been a number of climate change
declarations by individual organisations, however no regional declaration to
date.
Rick Barker
left the meeting at 12.00pm.
·
Query in relation to council’s vehicle
fleet emissions and carbon emissions around councillor and staff air
travel.
·
Council’s organisational carbon
footprint numbers (Weaver report in 2017) are currently being updated through
an independent auditor and will be provided as part of future organisational
performance reports
·
In relation to the carbon footprint of tourism
to Hawke’s Bay, James advised that discussions will be entered into to
see if they have data which can be used to assess the contribution of tourism
to the region’s carbon footprint
Rick Barker
returned to the meeting at 12.07pm.
·
The Regional Council can influence local
authorities and where they locate residential and infrastructure development through
the Regional Policy Statement (RPS) and does so through its statutory advocacy
work by submitting on TA planning proposals
·
Acidification of oceans and the serious
impacts associated
Rick Barker
left the meeting at 12.19pm
·
Climate change demands urgent action to respond
to significant weather events, rising sea levels, coastal erosion and carbon
emissions
·
Staff need the time to do the real work rather
than writing reports that add no value
Rick Barker
re-joined the meeting at 12.31pm
·
The report highlighted the amount of work
already undertaken in this space
·
Need to give higher priority to climate change
adaptation, intensify efforts and identify further opportunities to improve the
environment, and to lead by example.
ESC18/19
Resolutions
1.      That the Environment and Services
Committee receives and considers the “Climate Change Mitigation and
Adaptation - A Regional Response” staff report.
2.      The Environment and Services Committee recommends that
Hawke’s Bay Regional Council:
2.1.      Declares a climate emergency, recognising global warming to be an
urgent and pervasive threat to human and ecological wellbeing.
2.2.      Commits to providing an annual progress report in relation to its
existing programme of work and additional future programmes relating to
climate change.
2.3.      Includes climate change as a primary factor for consideration in
its decision making processes.
2.4.      Commits to developing a comprehensive programme of work in
response to climate change, including regional leadership for climate change
awareness and action.
2.5.      Requests staff develop a programme of community engagement on
climate change mitigation and adaptation.
2.6.      Directs the Chief Executive to further reduce the Council’s
greenhouse gas emissions and report annually on progress within the annual
progress report.
2.7     Advocate
to the Ministry of Environment to include greenhouse gas emissions in the resource
consenting process under the Resource Management Act.
2.8     Makes
further submissions on the Zero Carbon Bill as appropriate.
Belford/Graham
For: Bailey, Graham, Beaven, Barker, Kirton,
Gregory, Belford
Against: Dick, Paku, Wilson
CARRIED
The meeting adjourned at 12.48pm and reconvened at 1.15pm.
The Chair advised that
many of the remaining Agenda items will be taken as read.
9.
National Compliance Monitoring and
Enforcement Report
Liz Lambert introduced item which provides a comprehensive picture
of Compliance, Monitoring and Enforcement activities across the Regional
sector.  Discussions traversed:
·
This is the first report that brings together
data on Compliance, Monitoring and Enforcement (CME) functions across New
Zealand.
·
room for improvement around HBRC’s
information management, particularly the outcomes of incident responses
·
This report will be used as a benchmark for
future updates
·
HBRC has some of the lowest staffing levels
per head of population involved in CME activities
·
Staffing levels tend to correspond to the
economy of the region, e.g. large dairying regions, or those with big
industrial economies
·
Council will have an ongoing need for additional
compliance, monitoring and enforcement staff as new consenting requirements
take effect with PC6 and TANK.
·
In terms of data information, the new IT
system (IRIS) for consents and compliance is now live and will provide better
data to report on and will largely address the improvements suggested in the
report
·
Currently recruiting for a Compliance Manager
and will have a fresh look at compliance practises through that process
·
The annual Compliance report will go to the
August Environment and Services committee meeting.
·
Three waters review has vastly improved
strategic discussions and engagement between HBRC and TAs in relation to
their infrastructure and meeting the consent requirements.
·
Councillor stated his opposition to
contracting Catalyst for this work for the record.
ESC19/19
Resolution
That the Environment and
Services Committee receives and notes the “National Compliance
Monitoring and Enforcement Report”.
Barker/Bailey
CARRIED
Cr Rick Barker left the meeting at 1.39pm.
8.
Flaxmere Solar Farm
Mark Heaney introduced Chris Lambourne, Power
to the People Project Manager and Berry Rangi, Cook Island Secretary for St
Andrew’s Church.
·
Proposal to provide partial funding towards a
total capital project of $2.7million for a solar farm in Hastings
·
Proposal is to build and operate a 1MW PV
Solar Farm on a 2 hectare site, with the proceeds from the sale of
electricity generated used to reduce the cost of retail power to 400
low-income homes in Flaxmere and Camberley (within the Hastings airshed)
·
Solar will assist in moving the Council from
the national current renewable energy level of 85% to the planned 100% in
line with the Council’s strategic goal of carbon neutrality by 2040
·
Seeking an in principle decision
·
Excerpt from the project feasibility study will
be distributed to the Committee.
ESC20/19
Resolutions
1.      That the Environment and Services Committee receives and considers
the “Flaxmere Solar Farm” staff report.
2.      The Environment and Services Committee recommends that
Hawke’s Bay Regional Council:
2.1       Agrees that the decisions to be made are not significant under the
criteria contained in Council’s adopted Significance and Engagement
Policy, and that Council can exercise its discretion and make decisions on
this issue without conferring directly with the community and persons likely
to be affected by or to have an interest in the decision.
2.2       Agrees in principle, and subject to acceptance by the Regional
Council of a full business case and debt security being procured, to extend
lending to ‘Power To The People’, to achieve the
objectives of the Heatsmart and the Sustainable Homes programmes.
Bailey/Wilson
CARRIED
10.
Update on Farm Environment Management
Plans
Liz Lambert invited Shane Gilmer, FEMP Project Coordinator who presented on the farm environment management plans to date in
the Tukituki catchment.  Discussions traversed:
·
PC6 deadline for
resource consents in the Papanui Stream, Mangaonuku
Stream and Kahahakuri Stream sub-catchments is
1 June 2020, with staff expecting around 340 new resource consents
required
·
Farm Plans consist of three parts – 1. identifying
environmental risk on the property, 2. mitigation of the risk and 3. an
action plan to achieve the mitigation.
·
First farm plans received in 2016
·
A FEMP auditing programme is being piloted, which
checks that all environmental risks have been identified and that the
mitigation measures are adequate
Fenton Wilson left at 2.06pm
·
2020 compliance deadlines for stock exclusion
and LUC reports where properties exceed the allowable limits
·
3 year review cycle coming up 2021 when 1100
FEMPs will need to be resubmitted including data to show progress toward
achieving compliance with limits
·
Currently finalising consent application forms
and information guidelines for a series of engagements with landowners in the
three affected sub-catchments early in the new financial year.
·
Still working with landowners with outstanding
FEMPs to get them completed and haven’t taken enforcement action
against them yet as they are making progress
Fenton Wilson returned to the
meeting at 2.15pm.
·
Compliance with meeting the deadline has not
been enforced, and the requirement for a resource consent will be the stage
at which that will happen.
ESC21/19
Resolution
That the Environment and
Services Committee receives and notes the “Update on Farm
Environment Management Plans” staff report.
Beaven/Belford
CARRIED
15.
Catchment Management and Erosion
Control Scheme Update
Iain Maxwell introduced Dean Evans, Nathan Heath and Brendan
Powell who presented on the key outputs achieved and work planned for
2019-20.  Discussions covered:
·
Brendan Powell (Central Catchment) noted key
points from the report including the formation of new teams and new office
locations, 64 approved Erosion Control Scheme (ECS) applications and $1.45
million committed for on-ground works, 28,000 Poplar and Willow poles sold to
protect 560ha of land
·
Around 252,000 ha of Hawke’s Bay hill country identified
through SEDNet modelling as being at high risk of erosion
·
$5M central government funding for work over the next 4 years
·
An erosion control plan takes between 32 and 80 hours of staff
time
·
Staff have been involved in the logistics of getting programmes
up and running
·
2019-20 Target for 70 Erosion Control Plans approved (35 per
Catchment Advisor)
·
Focus on sediment priority areas (catchments and farms) and
policy implementation, specifically around TANK
·
Future hgh demand for catchment issues beyond erosion
Northern
Catchments Focus
·
Nathan Heath advised the team focussed on engagement and
consultation with tangata whenua aspirations at a local level.
·
2019-20 target of 70 Erosion Control Plans
·
Wairoa community concerned not enough attention being given to
unique indicators and characteristics of the place and people, requiring a
different approach to making things happen.
·
Community concern around the state of the awa, the Wairoa
wastewater system upgrade and the legacy effectd of the Waihi dam, forestry
slash and streambank erosion threatening a number of urupa and marae
Southern
Catchments Focus
·
Dean Evans noted the target of 105 Erosion Control Plans for
2019-20.
·
Transition from backlog response to targeted approach with particular
focus on worst areas
·
Supporting Tukituki PC6 Policy implementation and working
collaboratively with the Regulation team on FEMP support for priority
sub-catchments
·
Assisting with delivery of key projects of CHB water security,
Lake Whatuma, Aramoana, Porangahau estuary and effective collaboration with
iwi, partners, stakeholders and rural professionals
·
Propose quarterly reporting in future, through the
organisational performance report including hectares of land under management
·
The Deed for the Whakaki Freshwater Improvement Fund was signed
this morning
·
Procurement team is going to manage procurement of plants for
the erosion control scheme
Cr Alan Dick left at 2.35pm.
·
Demand equally across the region, however majority of highly
erodible land is in the Wairoa area so resource allocation will continue to
be reviewed to be able to meet demand.
·
Dr Barry Lynch was successful getting funding from Central
Government to get a range of automated samplers to deploy throughout
catchments and a staff member to run them, with that info added to calibrate
SEDnet and make it more accurate
ESC22/19
Resolution
That the Environment and
Services Committee receives and notes the “Erosion Control Scheme
Update” staff report.
Bailey/Beaven
CARRIED
14.
Landscape Scale Ecological Restoration
on the Mahia Peninsula
Nathan Heath spoke about the current work on Mahia Peninsula and
how that might be extended in collaboration with the Mahia community.
Discussions traversed:
·
A variety of ideas (like identification,
prioritisation of key sites of significance) have been put forward as to how
a “wider vision for Te Mahia” might be realised.
·
Identify priorities to best integrate with
HBRC’s current actions
·
Opportunity to develop skills and training for
whanau to undertake work, for example planting, establishing nurseries, etc.
·
Once engagement and consultation with tangata
whenua done, Nathan will report back to this committee with a proposal
·
Predator Free Te Mahia achievable, challenge
is keeping it pest free.
ESC23/19
Resolution
That the Environment and Services Committee receives and considers the “Landscape
Scale Ecological Restoration on the Mahia Peninsula” staff
report.
Beaven/Wilson
CARRIED
11.
Update on Central Government Policy
Announcements
The item was taken as read.
·
a number of Central Government workstreams
underway in relation to resource management legislation and policy
·
The Essential Freshwater Reform project is
going to represent quite a significant change to Freshwater Plan Change
processes
·
None of the Government’s proposals can
be guaranteed until approved by Cabinet
·
Report on potential impacts on Council work
programmes will be presented at a future meeting
ESC24/19
Resolution
That the Environment and
Services Committee receives and notes the “Update on Central Government
Policy Announcements” staff report.
Beaven/Kirton
CARRIED
12.
Sustainable Homes Update
The item was taken as read.
ESC25/19
Resolution
That the Environment and
Services Committee receives and notes the “Sustainable Homes Update”
staff report.
Beaven/Kirton
CARRIED
13.
Waitangi Horseshoe Wetland Update
The item was taken as read.
·
Cr Paul Bailey proposed that a more suitable
name be chosen to suit tangata whenua. – provisional name proposed, but
have not been agreed by all parties and thus not included in the paper.
ESC26/19
Resolution
That the Environment and
Services Committee receives and notes the “Waitangi Horseshoe
Wetland Update” staff report.
Beaven/Kirton
CARRIED
16.
Erosion Control Grants Policy
Refinement
The item was taken as read.
ESC27/19
Resolution
That the Environment and
Services Committee receives and notes the “Erosion Control Grants
Policy Refinement” staff report.
Beaven/Kirton
CARRIED
17.
June 2019 Hotspots Update
The item was taken as read.
ESC28/19
Resolution
That the Environment and
Services Committee receives and notes the “June 2019 Hotspots
Update” staff report.
Beaven/Kirton
CARRIED
18.
Discussion of Minor Items Not on the
Agenda
Topic
Raised
by
Managed aquifer
recharge:
·
Requesting a report on the water security project and what
stage consideration of the managed aquifer recharge proposal is at
Cr P Bailey
Natural forestation in
Wairoa
Rex and Paul went to visit Jill Snelling’s property, and
met with several other landowners, notably the next door property on which
the landowner spends 4 hours a day on pest management with stunning
results..
Cr R Graham/
Cr P Bailey
Future Farming Trust meeting.
·
next Future Farming Trust meeting today at 5.30pm.
·
will discuss 12 candidates for the permanent trust.
·
will consider feedback from the wider working group to narrow
down to 8 candidates for interviews.
·
The draft Trust Deed does have provision for a councillor
appointed trustee (to be appointed through a council process).
Cr T Belford
The meeting adjourned at 3.12pm and re-convened at 3.28pm
19.
Potential Acquisition of Land at Lake
Whatuma
ESC29/19
Resolution
That Council excludes the public
from this section of the meeting, being Agenda Item 19 Potential Acquisition
of Land at Lake Whatuma with the general subject of the item to be considered
while the public is excluded; the reasons for passing the resolution and the
specific grounds under Section 48 (1) of the Local Government Official
Information and Meetings Act 1987 for the passing of this resolution being:
GENERAL SUBJECT OF THE ITEM TO BE
CONSIDERED
REASON FOR PASSING THIS RESOLUTION
GROUNDS UNDER SECTION 48(1) FOR THE
PASSING OF THE RESOLUTION
Potential Acquisition of Land at Lake
Whatuma
7(2)(i) That the public conduct of this
agenda item would be likely to result in the disclosure of information
where the withholding of the information is necessary to enable the local
authority holding the information to carry out, without prejudice or
disadvantage, negotiations (including commercial and industrial
negotiations).
The Council is specified, in the First Schedule
to this Act, as a body to which the Act applies.
Beaven/Kirton
CARRIED
20.
Forest Harvest Procurement,
Tūtira and Tangoio Forests
ESC30/19
Resolution:
That Council excludes the public
from this section of the meeting, being Agenda Item 20 Forest Harvest
Procurement, Tūtira and Tangoio Forests with the general subject of the
item to be considered while the public is excluded; the reasons for passing
the resolution and the specific grounds under Section 48 (1) of the Local
Government Official Information and Meetings Act 1987 for the passing of this
resolution being:
GENERAL SUBJECT OF THE ITEM TO BE
CONSIDERED
REASON FOR PASSING THIS RESOLUTION
GROUNDS UNDER SECTION 48(1) FOR THE
PASSING OF THE RESOLUTION
Forest Harvest Procurement, Tūtira
and Tangoio Forests
7(2)s7(2)(i) That the public conduct of
this agenda item would be likely to result in the disclosure of information
where the withholding of the information is necessary to enable the local
authority holding the information to carry out, without prejudice or
disadvantage, negotiations (including commercial and industrial
negotiations).
The Council is specified, in the First
Schedule to this Act, as a body to which the Act applies.
Beaven/Kirton
CARRIED
The meeting went into public excluded session at 3.28pm
and out of public excluded session at 4.35pm
Closure:
There
being no further business the Chairman declared the meeting closed at 4.35pm on
Wednesday, 19 June 2019.
Signed
as a true and correct record.
DATE: ................................................               CHAIRMAN:
...............................................