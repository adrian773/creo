FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

6 AUGUST 2019 

  MINUTES OF WAIROA DISTRICT COUNCIL 

FINANCE, AUDIT & RISK COMMITTEE MEETING 

HELD AT THE COUNCIL CHAMBER, WAIROA DISTRICT COUNCIL, CORONATION SQUARE, WAIROA 

ON TUESDAY, 6 AUGUST 2019 AT 1.30PM 

 

PRESENT: 

Cr  Denise  Eaglesome-Karekare 
(Chairperson), Mr Philip Jones 

(Deputy  Mayor),  Cr 

Jeremy  Harker 

IN ATTENDANCE:   Casey Hucker & Matthew Lawson (Solicitors, Lawson Robinson), S May (Tumu 
Whakarae  Chief  Executive  Officer),  G  Borg  (Pouwhakarae  –  Pūtea/Tautāwhi 
Rangapū  Group  Manager  Finance  and  Corporate  Support),  Stephen  Heath 
(Pouwhakarae  –  Hua  Pūmau  Hapori/Ratonga  Group  Manager  Community 
Assets  and  Services),  G  Waikawa  (Kaiurungi  Mana  Arahi  –  Governance 
Officer),  K  Stevenson  (Kaiurungi  Kore  Whakawhara/Zero  Harm  Officer),  A 
Atzwanger (Kaiurungi Whakawhanake Hapori Economic Development Officer) 

 

1 

KARAKIA 

Given by G Borg. 

2 

APOLOGY FOR ABSENCE 

APOLOGY 

COMMITTEE RESOLUTION  2019/61  

Moved: 
Seconded:  Cr Jeremy Harker 

Cr Denise Eaglesome-Karekare 

That the apology received from His Worship the Mayor be accepted and leave of absence granted. 
CARRIED 

 

3 

None 

DECLARATION OF CONFLICT OF INTEREST 

4 

CHAIRPERSON’S ANNOUNCEMENTS 

None. 

5 

LATE ITEMS OF URGENT BUSINESS 

Financial Performance to May 2019 

6 

PUBLIC PARTICIPATION 

A maximum of 30 minutes has been set aside for members of the public to speak on any 
item on the agenda. Up to 5 minutes per person is allowed.  As per Standing Order 14.14 

Page 1 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

6 AUGUST 2019 

requests to speak must be made to the meeting secretary at least one clear day before 
the meeting; however this requirement may be waived by the Chairperson. 

7 

MINUTES OF THE PREVIOUS MEETING 

COMMITTEE RESOLUTION  2019/62  

Moved: 
Seconded:  Cr Denise Eaglesome-Karekare 

Cr Jeremy Harker 

That the minutes of the Ordinary Meeting held on 2 July 2019 be confirmed. 

CARRIED 

 

8 

 

PUBLIC EXCLUDED ITEMS  

RESOLUTION TO EXCLUDE THE PUBLIC 

MOTION 

COMMITTEE RESOLUTION  2019/63  

Moved: 
Seconded:  Cr Denise Eaglesome-Karekare 

Cr Jeremy Harker 

That the public be excluded from the following parts of the proceedings of this meeting at 1.34pm. 

The  general  subject  matter  of  each  matter  to  be  considered  while  the  public  is  excluded,  the 
reason  for  passing  this  resolution  in  relation  to  each  matter,  and  the  specific  grounds  under 
section 48 of the Local Government Official Information and Meetings Act 1987 for the passing of 
this resolution are as follows: 

General subject of each matter 
to be considered 

9.1 - Risk Mitigation 

Reason for passing this 
resolution in relation to each 
matter 

s7(2)(g) - the withholding of the 
information is necessary to 
maintain legal professional 
privilege 

Ground(s) under section 48 for 
the passing of this resolution 

s48(1)(a)(i) - the public conduct 
of the relevant part of the 
proceedings of the meeting 
would be likely to result in the 
disclosure of information for 
which good reason for 
withholding would exist under 
section 6 or section 7 

CARRIED 

 

COMMITTEE RESOLUTION  2019/64  

Moved: 
Seconded:  Mr Philip Jones 

Cr Jeremy Harker 

That the Committee moves out of Closed Committee into Open Committee at 2.16pm. 

Page 2 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

6 AUGUST 2019 

 
 
ADJOURNED – 2.17PM 
RECONVENED – 2.22PM 
 

9 

GENERAL ITEMS 

9.1 

UPDATE ON RESPONSE TO AUDIT RECOMMENDATIONS 2017-18 

COMMITTEE RESOLUTION  2019/65  

Moved: 
Seconded:  Cr Denise Eaglesome-Karekare 

Cr Jeremy Harker 

That the Committee receive the report. 

CARRIED 

CARRIED 

 

9.2 

UPDATE  ON  PREPARATION  OF  THE  ANNUAL  REPORT  FOR  THE  YEAR  ENDING  30  JUNE 
2019. 

COMMITTEE RESOLUTION  2019/66  

Moved:  Mr Philip Jones 
Seconded:  Cr Jeremy Harker 

That the Committee receive the report. 

POINTS RAISED: 

Creditors 
Invoices 
 

9.3 

ESTIMATED RESERVES AND SPECIAL FUNDS 

COMMITTEE RESOLUTION  2019/67 

Moved: 
Seconded:  Cr Denise Eaglesome-Karekare 

Cr Jeremy Harker 

That the Committee receive the report. 

POINT RAISED: 

Reconciliation of Reserves 

CARRIED 

CARRIED 

Page 3 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

6 AUGUST 2019 

 
 

9.4 

HEALTH & SAFETY REPORT 

COMMITTEE RESOLUTION  2019/68  

Moved: 
Seconded:  Mr Philip Jones 

Cr Jeremy Harker 

That the Committee receive the report. 

CARRIED 

POINTS RAISED: 

Near misses, proactive advice of potential hazards and increasing awareness need to be aware of. 
Provide data of incidents, near misses, injuries, medical treatment injuries and serious events. 
 
 

9.5 

LATE ITEM - FINANCIAL PERFORMANCE TO 31 MAY 2019 

COMMITTEE RESOLUTION  2019/69 

Moved: 
Seconded:  Mr Philip Jones 

Cr Denise Eaglesome-Karekare 

That the Committee receive the report. 

CARRIED 

 
 

FAR WORK PLAN 

 

# 

1. 

Date 
Entered 

April 

2 
2019 

Action to be taken 

Responsible 

To be completed by 

Closed/Open 

Top five H&S risks to staff and 
what 
to 
minimise them. 

is  being  done 

Kevin 
Stevenson 

- 

To  be  included  in  Zero 
Harm Officer’s report 

14 May 2019 

Closed 

2. 

2 
2019 

April 

3rd quarter financial results 

Gary Borg 

11 June 2019 

Closed 

-  March 

financials 

to 

Council on 11 June 2019 

 

 

- 

April  financials  to  FAR 
committee  on  25  June 
2019 

Gary Borg 

25 June 2019 

Closed 

Page 4 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

6 AUGUST 2019 

3. 

2  April  20-
19 

Adopt  FAR  work  plan/TOR  as 
per 
FAR 
Committee minutes 

2019 

Feb 

26 

Steven May 

 

Ongoing 

 

 

- 

standing  agenda  item  for 
work plan 

 

16 July 2019 

Open 

4. 

April 

2 
2019 

Detailed  work  plan  on  how 
Non-conformances 
from  EY 
audit report will be addressed 

Steven 
May/Gary 
Borg 

6 August 2019 

Ongoing 

 

 

- 

establish  an  outstanding 
report  to  management 
register  with  a  regular 
update. 

 

5. 

May 

14 
2019 

Gary 
Borg/Stuart 
Mutch 

Interim  Summary  from  EY  on 
Annual  Report  process  and 
FY18/19  close  off.    Liaise  with 
Stuart  Mutch 
request 
summary. 

to 

-  update  of  where  processes 
for  reporting  of  reserves  both 
quarterly and annual report 

May 

14 
2019 

Full draft Annual report NLT 10 
Sept  2019  to  be  reviewed  by 
FAR  before 
to 
Auditors  

submitting 

Gary 
Borg/Steven 
May 

16 July 2019 

Open 

16 July 2019 

Ongoing 

 

 

 

 

 

 

6 Aug 2019 

Closed 

 

17 Sept 2019 

Open 

May 

Contract Management Review 

14 
2019 

Stephen 
Heath 

17 Sept 2019 

Open 

May 

14 
2019 

Financial 

Draft 
including summary reserves 

Statements 

Gary Borg 

17 Sept 2019 

Open 

6. 

7. 

8. 

9. 

2 July 2019 

Interim  report  on  risk  against 
Council risk policy 

CEO 

17 Sept 2019 

Open 

10. 

2 July 2019  Update  on  Long  Term  Plan 

2021-2031 (Project Review) 

Kimberley 
Tuapawa 

17 Sept 2019 

Open 

11 

6 Aug 2019  Reconciliation of reserves 

Gary Borg 

10 Dec 2019 

Open 

 
 

 

CARRIED 

Page 5 

FINANCE, AUDIT & RISK COMMITTEE MEETING MINUTES 

6 AUGUST 2019 

 
The closing karakia was given by G Borg. 
 
The Meeting closed at 3.06pm. 

 

The  minutes  of this  meeting  were  confirmed  at  the  Finance,  Audit  &  Risk  Committee  Meeting 
held on 17 September 2019. 

 

 

 

 

................................................... 

CHAIRPERSON 

 

Page 6 

