Minutes of Community Services Committee - 25 June 2019
Community Services Committee
Open Minutes
Meeting Date:
Tuesday
25 June 2019
Time:
3.08pm-3.35pm
Venue
Council
Chamber
Hawke's Bay Regional Council
159 Dalton Street
Napier
Present
Councillor Wright (In the
Chair), Acting Mayor White, Councillors Boag, Brosnan, Dallimore, Hague,
Jeffery, McGrath, Price, Tapine, Taylor and Wise
In
Attendance
Chief
Executive, Director Corporate Services, Director Community Services, Director
City Services, Director City Strategy, Manager Communications and Marketing,
Manager People and Capability, MTG Director, Manager Community Strategies,
Manager Environmental Solutions, Senior Advisor Policy, Communications
Specialist
Administration
Governance
Team
Community Services
Committee - 25 June 2019 - Open Minutes
Apologies
Councillors Wise / Tapine
That the
apology from Mayor Dalton be accepted.
Carried
Conflicts of
interest
Nil
Public forum
Bruce Carnegie – Napier Grey Power
Bruce spoke in support of the Draft Napier Positive Ageing
Strategy (Strategy), acknowledging the work of everyone involved in developing
the Strategy to date. He confirmed that the Strategy aligns with Napier Grey
Power’s priorities and noted the importance of the strategic link to the
Age Friendly Cities and Communities model.
Jill Fitzmaurice – Positive Ageing Strategy
Reference Group
Jill spoke in support of the Draft Napier Positive Ageing
Strategy (Strategy), having been involved in the development of the Strategy as
a member of the Positive Ageing Strategy Reference Group from its inception.
She noted that the word ‘Positive’ is the key to this initiative,
and spoke to the importance of implementing the Strategy to ensure that Napier
is equipped to successfully prepare and provide for our ageing population.
Announcements by
the Acting Mayor
Nil
Announcements by
the Chairperson
The Chair acknowledged the recent passing of Napier Civic
Award recipient, Kerry Duncan, and noted her tremendous service to the
community.
Announcements by
the management
Nil
Confirmation of minutes
Acting Mayor White / Councillor McGrath
That the
Minutes of the meeting held on 14 May 2019 were taken as a true and accurate
record of the meeting.
Carried
Community Services Committee - 25 June
2019 - Open Minutes
Agenda
Items
1.    Napier
Positive Ageing Strategy - Draft for Consultation
Type of Report:
Operational
Legal Reference:
N/A
Document ID:
757232
Reporting Officer/s &
Unit:
Michele Grigg, Senior Advisor
Policy
1.1   Purpose
of Report
This report summarises
development of the draft ‘Napier Positive Ageing Strategy – Te
Rautaki Tipu Ora o Ahuriri’ and seeks approval to release it for
community feedback prior to its finalisation for adoption by Council.
At the Meeting
The Chair acknowledged the work of staff, individuals
and agencies involved in developing the draft Strategy to date, and noted the
next stage is to seek community feedback through consultation.
In response to questions from Councillors, the
following points were clarified:
·
The consultation process will take place over four weeks,
commencing in July, with options for feedback being made available both
online and in writing.
·
It was noted that it will be important to address projected
figures for retiree homeownership as the number of over 65 year olds is set
to double over the length of this Strategy. Council officers noted that this
information will likely be drawn out during development of the implementation
plan, following adoption. A Councillor advised that a recent report
commissioned by the Retirement Villages Association of New Zealand predicted
that homeownership of people over the age of 65 will reduce to 50%.
·
An Advisory Group will be established once the Strategy is
adopted, comprised of members that would be able to take the lead on
different areas of the Strategy. It is intended that the Advisory Group will
be independently chaired and will work with the relevant agencies to deliver
action plans.
·
The implementation plan and drivers will come back through
Council for consideration.
·
The intention is to have an implementation plan in place by the
end of the year; however, this will be dependent on feedback received through
consultation and staff workloads, noting that officers will be required to
work around this year’s Election and the impact this has on the meeting
schedule.
·
It was noted that the development of the Strategy, together
with the Disability Strategy, has highlighted parallels to developing a Child
Friendly City framework and provided learnings to take forward into that
space. Officers are considering how this framework could be developed in
future.
Committee's recommendation
Councillors
Boag / Taylor
The Community Services Committee:
a.     Approve
the release of the draft Napier Positive Ageing Strategy – Te Rautaki
Tipu Ora o Ahuriri for community feedback.
Carried
2.    IRON
MĀORI EVENT -
ROAD CLOSURES
Type of Report:
Operational
Legal Reference:
N/A
Document ID:
755845
Reporting Officer/s &
Unit:
Sera Chambers, Transportation
Team Administrator
Kevin Murphy, Event Manager
2.1   Purpose
of Report
To seek approval for the course
change and road closures for the Iron Māori events to be held in 2019 and
in future years.
At the Meeting
It was noted that a number of participants in these
events have never taken part in events of this nature before which raises
safety concerns around navigating difficult roads etc. Council officers
confirmed that these concerns would be passed on to Council’s Event
Manager who will be working closely with event organisers.
In response to a question as to whether a trial period
had been discussed, Council officers confirmed that these events are
constantly reviewed. If the layout works well this year, officers have
authority to approve the course for the next event; however, if significant
concerns are raised this would come back to Council for their input.
Committee's recommendation
Councillors
Taylor / Price
The Community Services Committee:
a.     Approve
the course change and road closures for the Iron Māori event and other
events.
Carried
Community Services Committee - 25 June
2019 - Open Minutes
PUBLIC EXCLUDED
ITEMS
Acting Mayor White / Councillor Hague
That the public be excluded
from the following parts of the proceedings of this meeting, namely:
1.         Regional
Museum Research and Archives Centre
Carried
The general subject of each
matter to be considered while the public was excluded, the reasons for passing
this resolution in relation to each matter, and the specific grounds under
Section 48(1) of the Local Government Official Information and Meetings Act
1987 for the passing of this resolution were as follows:
General subject of each
matter to be considered.
Reason for passing this
resolution in relation to each matter.
Ground(s) under section
48(1) to the passing of this resolution.
1.  Regional Museum
Research and Archives Centre
7(2)(h) Enable the local authority to carry out,
without prejudice or disadvantage, commercial activities
48(1)A That the public conduct
of the whole or the relevant part of the proceedings of the meeting would be
likely to result in the disclosure of information for which good reason for
withholding would exist:
(i) Where the local authority is named or specified in Schedule 1 of this
Act, under Section 6 or 7  (except 7(2)(f)(i)) of the Local Government
Official Information and Meetings Act 1987.
The meeting moved into Committee
at 3.35pm.
Approved and adopted as a true and accurate record of the
meeting.
Chairperson .............................................................................................................................
Date of approval ......................................................................................................................