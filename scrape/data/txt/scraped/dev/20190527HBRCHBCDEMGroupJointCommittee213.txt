Minutes of HB Civil Defence Emergency Management Group - 27 May 2019
MINUTES OF A
meeting of the HB Civil Defence Emergency Management Group
Date:                          Monday 27 May 2019
Time:                          1.30pm
Venue:
Council Chamber
Hawke's Bay Regional Council
159 Dalton Street
NAPIER
Present:                     A Walker (Chair & CHBDC Mayor)
F White (NCC Acting Mayor)
R Graham (HBRC Chair)
S
Hazlehurst (HDC Mayor)
In Attendance:                      B Allan
M
Davidson (CHBDC CE)
W Jack (NCC CE)
S May (WDC CE)
J Palmer (HBRC CE)
I Macdonald –
Group Controller HB CDEM
A Hickey - MCDEM
L Lambert – HBRC
Group Manager Regulation
M Hayes-Jones –
HBRC Emergency Management Advisor
A Prins – HB CDEM Group Welfare Manager
A Roets
–Governance Administration Assistant
1.       Welcome/Apologies/Notices
A quorum was not
established and therefore the meeting lapsed at 1.30pm.
The Chair
reminded members that the quorum is all five members of the Joint Committee,
and that if a member is unable to attend they need to delegate to their
‘deputy’ to attend on their behalf.
Despite the meeting
not being legally constituted, the Chair requested that the Committee receive
briefings on the agenda items – recorded following.
4.
Action Items from Previous HB CDEM
Group Joint Committee Meetings
·
Lifelines Action plan is being developed and
will be reported to the next Joint Committee meeting
·
Work programme for earthquake prone buildings
will be organised by Ian Macdonald with each council individually including in
terms of risk reduction and a snapshot of current state will be reported to
the next Joint Committee meeting
6.
Group Plan Review
Ian Macdonald provided an update of the
work already undertaken with discussions highlighting:
·
Group Plan due for renewal as originally
approved in June 2014
·
Current strategic approach is viewed as still
being fit for purpose and is not creating any implementation issues
·
2019 review commenced with commissioning of HB
CDEM Group Risk Profile Review and HB CDEM Group Capability Assessment Report
which form the foundation of the review
·
Gap analysis will be carried out once risk
analysis and capability assessments have been completed
·
The detailed work program will be reported to the
next Joint Committee meeting
·
Number of key areas to be examined include
Group Structure, changes to the Act, changes to strengthen recovery
positions, changes to response framework and roles of councils and partners
in a response, changes in the Risk Profile and long term reduction
provisions.
·
Some external influences/constraints to
completing the review include Emergency Management System Reform Programme,
Review of the National Plan and Coordinated Incident Management System (CIMS)
Review.
·
CDEM Coordinating Executive Group has endorsed
the approach outlined
·
tangata whenua involvement includes Community
resilience planning, three tangata whenua actively involved and advise on
kaupapa Māori,
papakainga inclusion in the Resilience Plans process and focus on work to
determine “how best” to incorporate Māori perspectives within the CDEM world
·
CDEM provides input to HPUDS through Liquefaction
maps and advocating for consideration to be given to the Risk Reduction
framework
·
Work is being undertaken on updating the
Regional Policy Statement to incorporate the natural hazards elements of the
Resource Management Act
·
suggested that the Committee brainstorms
strengths and weaknesses of the Group and what the new Plan will look like at
the August meeting, after the gap analysis has been done
·
Group Structure and funding to include
Governance responsibilities and delegations including financial statements
and performance objectives for both regional and local councils
7.
Hawke’s Bay Civil Defence
Centres Concept
Discussions on the Civil Defence Centre
(CDC) project traversed:
·
No Group strategy or formal plan for use of
CDCs
·
Best practice is that people either shelter in
place or evacuate to friends and family if they can
·
Potential political risks as one of the
outcomes is to remove all signage from buildings and location of CDCs from
websites
·
Many pre-identified facilities located in
evacuation/hazard zones which can be confusing and put lives at risk and important
that communities are educated adequately about this project
·
Further work required to plan for mass
evacuations and large scale facilities to provide basic health care
·
Communities are effectively managing their own
community led responses – and may establish their own community led
centre which CDEM can support if needed
·
The final strategy and plan will be presented to
the Coordinating Executive Group.
·
current recommendation is to
“retire” Civil Defence Management and replace with Emergency
Management – changing the “brand”
8.
Group Work Programme Progress Update
Group Work Programme discussions
traversed:
·
areas of focus and risks identified include rebuilding
of the Hastings Emergency Management Centre, Civil Defence Centres, Community
Resilience and Education, Hazard Research (Landslides).
·
Group budget at end of 2018 underspent across
all projects due to staff vacancies not being filled immediately as well as
budget sequencing.
·
2018-19 quarterly expenditure and budget
update will be provided to the August JC meeting
·
Resourcing for community resilience projects
is tight
·
processes to follow to keep public safe around
the coastal areas of Hawke’s Bay following the Cape Kidnappers
landslides and references made in the Group Plan specifically around
Readiness and how Emergency Management deals with Tourists
·
Mass Public Alerting system work stream
includes several interrelated projects including regional warning systems,
mass public alerting systems (e.g. sirens), incident management team activation
9.
Volunteer Management Plan
The item highlights:
·
Volunteer Strategy has been adopted by the
Coordinating Executive Group and 5-year implementation plan summarises
“how” the strategy will be achieved
·
Potential for some expenditure to be directed
to implementation of the Volunteer Technical Advisory Group (VTAG) across
Hawke’s Bay volunteer groups to increase efficiency and capability of
an integrated volunteer response
·
The Plan allows for the volunteer base to be
developed in an effective and targeted way and have a collaborative approach
with CDEM partners and NGOs
·
Recruiting volunteers will be achieved using
several avenues
·
Essential that volunteers are retained to make
best of the investment in equipment, training, experience and personal development
opportunities
·
VTAG has been established with several
organisations and agencies who use volunteers for Emergency Management
10.
Risk Reduction
Current situation and exploration of strategies
and options addressing long term risk reduction and the impacts of natural
hazards for Hawke’s Bay discusions traversed:
·
Long term hazard risk reduction strategies and
plans need to be developed
·
There is an increased knowledge and more
robust information on hazards and impacts
·
Sustainable long term (over a period of 100
years) decisions on land use and development help reduce the risks associated
with natural hazards
·
Risk profile is changing – economic and
social impacts increasing
·
Council and agency approaches should not
conflict with Central Government Policy
·
A more cooperative and coordinated approach
suggested – similar to Clifton to Tangoio Coastal Hazards Strategy 2120
·
Possibly start with a Regional Policy
Statement change to provide some tools underpinned by a risk based approach
to determine what is acceptable and what not
·
Potentially looking at one document or
combined plan for all TAs and Agencies.
·
Next step is initial discussions with CEs, TAs
and Agencies.
11.
Group Manager’s General Update
Ian Macdonald’s update covered:
·
CDEM Duty Managers Management system, mobile
emergency alerts test feedback
·
resilience fund application outcomes
·
Regional Group exercise in October 2019
·
reporting to Councils on Group activities and
distribution of Joint Committee Minutes
12.
MCDEM
update
Andrew Hickey provided a brief overview of
national level MCDEM work covering:
·
Focus on implementing Emergency Management
System Reform which will possibly result in changes to the Civil Defence
Management Act
·
Budget announcement 30 May will provide some
direction from the Ministry
·
National Disaster Resilience Strategy approved
by Cabinet on 9 April
NZ fly in
teams
·      NZ fly-in Teams (a team of up to 10 people) have been renamed to
Emergency Management Assistance Teams or NZ-EMAT, first round of appointments
have been made and initial training will commence soon.
Revision of
CIMS
·      A revision of the Coordinated Incident Management System (CIMS)
will be signed of by government shortly.
Controller and Recovery Managers Development Programme
·
A draft programme outline for the new Controller
and Recovery Managers Development Programme has been developed, consisting of
an on-line component and two face-to-face Tiers.  Tier 1 focus is on
common capability for response and recovery management and Tier 2 focus is on
response and recovery leadership capability.
Training and induction of
elected members post elections will be discussed at the next Joint Committee
meeting.
Closure:
There
being no further business the Chair declared the meeting closed on Monday, 27
May 2019 at 3.27pm.
Signed
as a true and correct record.
DATE: ................................................               CHAIRMAN:
...............................................