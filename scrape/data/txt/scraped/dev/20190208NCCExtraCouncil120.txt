Minutes of Extraordinary Meeting of Council - 8 February 2019
Extraordinary Meeting of Council
Open Minutes
Meeting Date:
Friday
8 February 2019
Time:
10.30am
Venue
Council
Chambers
Hawke's Bay Regional Council
159 Dalton Street
Napier
Present
Deputy Mayor White (In the
Chair), Councillors Boag, Brosnan, Dallimore, Hague, Jeffery, McGrath, Price,
and Tapine
In
Attendance
Chief
Executive, Director Corporate Services, Director Infrastructure Services, Director
City Services, Director City Strategy, Manager Communications and Marketing,
Manager Building Consents
Helen
Rice – Managing Director, Rice Speir
Amy
Davidson – Senior Associate, Rice Speir
Administration
Governance
Team
Extraordinary Meeting
of Council - 08 February 2019 - Open Minutes
Apologies
Apologies
Council resolution
Councillors
Boag / Brosnan
That the apologies from Mayor Dalton, Cr Taylor, Cr Wise
and Cr Wright be accepted.
Carried
Conflicts of interest
Nil
Public forum
Nil
Announcements by
the Deputy Mayor
Nil
Announcements by
the management
Nil
Extraordinary Meeting of Council - 08
February 2019 - Open Minutes
PUBLIC EXCLUDED
ITEMS
Council resolution
Councillors Hague / Price
That the public and all staff
other than
·
Members of the Senior Leadership Team
·
Manager Building Consents
·
Governance Team (for the purposes of meeting administration
·
Helen Rice and Amy Davidson of Rice Speir
be excluded from the
following parts of the proceedings of this meeting.
Carried
Agenda
Items
1.         Delegation
to Chief Executive
The general subject of each
matter to be considered while the public was excluded, the reasons for passing
this resolution in relation to each matter, and the specific grounds under
Section 48(1) of the Local Government Official Information and Meetings Act
1987 for the passing of this resolution were as follows:
General subject of each
matter to be considered.
Reason for passing this
resolution in relation to each matter.
That the public conduct of
the whole or the relevant part of the proceedings of the meeting would be
likely to result in the disclosure of information where the withholding of
the information is necessary to:
Ground(s) under section
48(1) to the passing of this resolution.
48(1)(a) That the public
conduct of the whole or the relevant part of the proceedings of the meeting
would be likely to result in the disclosure of information for which good
reason for withholding would exist:
Agenda
Items
1.  Delegation to Chief
Executive
7(2)(g) Maintain legal professional privilege
7(2)(i) Enable the local authority to carry on,
without prejudice or disadvantage, negotiations (including commercial and
industrial negotiations)
48(1)A That the public conduct
of the whole or the relevant part of the proceedings of the meeting would be
likely to result in the disclosure of information for which good reason for
withholding would exist:
(i) Where the local authority is named or specified in Schedule 1 of this
Act, under Section 6 or 7  (except 7(2)(f)(i)) of the Local Government
Official Information and Meetings Act 1987.
The meeting moved into committee
at 10.31am
Approved and adopted as a true and accurate record of the
meeting.
Chairperson .............................................................................................................................
Date of approval ......................................................................................................................