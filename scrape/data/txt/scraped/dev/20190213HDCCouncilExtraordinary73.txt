Minutes of Extraordinary Council Meeting - 13 February 2019
Hastings District Council
Civic Administration Building
Lyndon Road East, Hastings 4156
Phone:  (06) 871
5000
Fax:
(06) 871 5100
www.hastingsdc.govt.nz
OPEN
M I N U T E S
Council - Extraordinary
Raureka Parks Reserve Management Plan
Meeting
Date:
Wednesday, 13 February 2019
CG-14-42-00036                                                                         1
Minutes
of a Meeting of the Council - Extraordinary held on
13
February 2019 at 9.00am
Table of Contents
Item                                                                                    Page No.
1.         Apologies  1
2.         Hearing
of Submissions to Raureka Parks Reserve Management Plan   2
2.         Hearing
of Submissions to Raureka Parks Reserve Management Plan (contd) 4
CG-14-42-00036                                                                         1
HASTINGS DISTRICT COUNCIL
MINUTES
OF A MEETING OF THE COUNCIL HELD IN Council Chamber, Ground Floor, Civic
Administration Building, Lyndon Road East, Hastings ON  Wednesday,
13 February 2019 AT 9.00am
Present:                          Mayor Hazlehurst (Chair)
Councillors
Barber, Dixon, Heaps, Kerr, Lawson, Nixon, O’Keefe, Poulain, Redstone,
Schollum, and Watkins.
IN ATTENDANCE:             Chief
Executive (Mr N Bickle)
Group Manager: Asset Management (Mr C Thew)
Group Manager:
Community Facilities & Programmes (Mrs A Banks)
Parks and
Property Services Manager (Mr C Hosford)
Parks Planning
and Development Manager (Ms R Stuart)
Parks Asset
Planner (Ms J Leaf)
Manager: Democracy and Governance Services (Mrs J Evans)
Committee Secretary (Mrs C Hunt)
AS required:
Submitters
Speaking:
Neil Thorsen – Endangered Species
Foundation (24), Kath Purchas and Liv Flynn – HB Bird & Wildlife
Rescue (20); David Renouf (4); Barry and Bernice Koenders (23) Wayne and
Judith Taylor (29; Wendy King (6); Carol McMillan – Girl Guiding (18);
Phillipa Cook (3) and Murray Richardson – Ellar Trust (26)
Members of the public were present in the gallery.
A full list of people who made submissions is attached.
1.
Apologies
Councillor
Dixon/Councillor Heaps
That apologies for absence  be
received from Councillor Travers, Harvey and Lyons be accepted.
CARRIED
Suspension of Standing Orders
Mayor
Hazlehurst/Councillor Redstone
That in accordance with paragraph 13.3
of  Standing Orders Her Worship the Mayor exercised her discretion to
waive the need for Councillors to stand to speak during a Council meeting.
CARRIED
2.
Hearing of Submissions to Raureka Parks Reserve
Management Plan
(Document
18/1303)
Mayor Hazlehurst welcomed submitters to the meeting and advised
that due to the level of public interest in regard to the Raureka Parks that
submissions would be considered at a full Council meeting, rather than the
Reserve Management Subcommittee.
HEARING OF
VERBAL SUBMISSIONS
Neil Thorsen Endangered Species
Foundation (24) spoke to his submission and
highlighted the following points:
·
That water be supplied to each of the parks
for human and animals to drink.
·
That trees and shrubs be planted to provide
food and shelter for fledging birds and other native fauna such as lizards
and insects.
·
That consideration be given to becoming
“predator free”.
·
Plant native trees in park corridors.
Mr Thorsen was happy to meet with Councillors
and discuss the work of the Endangered Species Foundation.  He was
currently working for the Hawke’s Bay Regional Council to produce a
list of endangered species.
Kath Purchas and Liv Flynn – HB Bird & Wildlife Rescue
(20) spoke to their
submission and highlighted the following points:
·
Provide water fountains for people and dogs in
each of the parks as NCC have done along the Marine Parade.
·
Cohesive strategy be considered  on
growing and supporting biodiversity in parks.
·
Plant native plants along north and south
boundaries.
·
Native plants would provide food and shelter
for species.
·
Currently almost no shelter for fledging birds
from predators.
·
Water sources for birds to drink and bath in
be provided.
·
Referred to paragraph 7.82 regarding enhancing
biodiversity.
·
More vegetation into park will bring native
and birds back to park areas.
David Renouf (4) spoke to his
submission and highlighted the following points:
·
Due to safety concerns opposed the use of
motorised scooters in parks due to the pathway width.
·
Paths too narrow for skate boarders – they
have a skate park to use.
·
Opposed vehicles on the park.
·
Requested an actual maintenance programme with
check lists.
·
Displayed at the meeting was a map  (CG-14-39-00012)
of Hastings from 1938 when streams ran through the City.
·
Displayed a map of Hastings showing the Makirikiri
Stream going through Hastings City (CG-14-39-00012) .
Bernice Koenders (23) advised that she
had presented to Council on 31 January 2019 with a
Petition of 252 signatories objecting to any part of Ebbett Park being sold.
Mrs Koenders advised that having read the
Officer’s report and recommendation she was satisfied with the outcome.
Wayne Taylor (29) spoke to his submission regarding the vesting of Ebbett Park under
the Reserves Act and was happy with the Officer’s recommendation to
delete the references in the Plan to not vest a portion.
Mr Taylor was also concerned with the terminology
“landlocked” as there was adequate access from Oliphant Road.
Wendy King (6) spoke to her submission
and highlighted the following points for St Leonards Park:
·
Supported native tree planting
·
Have walkway around the perimeter of the park.
·
Play area for under 6 year olds
·
Have a BBQ and seating included for St
Leonards Park for sporting and family activities.
·
Homeless use the park – nowhere to wash
Carol McMillan – Girl Guiding (18) spoke to her submission and highlighted
the following points:
·
Safety issue - Request carpark by Girl Guide
Hall.
·
Existing lighting was not enough.
·
Trees on path break branches and need to be
cleared away.
·
Equipment in play area for older children.
·
Suggest carpark could be locked at certain
time at night.
Murray Richardson – Ellar Trust (26) spoke to his submission and highlighted
the following points:
·
Concern about destruction and removal of trees
will affect birdlife.
·
In Western area the palm trees should be
removed.
·
Palm trees harbour rats and pigeons which are
a pest and create mess below the trees.
·
Plant native tree species eg titoki tree.
·
Concerned with appearance/maintenance of west
end entrance way off to Oliphant Road.
Sharnita Raheke – Raureka
Community Trust (32) spoke
to her submission and highlighted the following points:
·
Repair existing tennis and basketball courts.
·
Restore the old pavilion or replacement
facility erected.
·
Support the provision of well site furniture
in the Park.
·
Provision of drinking fountains also
essential.
·
Provision of proper carparking by Arahura Hall
accessed from Gordon Road.
·
Bollards and signage are deterrent for
vehicles accessing park.
·
Have native species planted within the Park.
·
More emphasis on the cultural values of Ebbett
Park.
·
Would like some carvings at the entrance of
Ebbett Park.
·
Important to have provision for both walking
and cycling in the Park.
·
Essential to have toilet facilities provided.
·
Important that Council consults and negotiates
with the landowners surrounding Ebbett Park for the provision of appropriate
fencing.
·
Encourage the purchase of additional land for
future developments of the Parks.
·
Very supportive of the Plan.
Phillipa Cook (3) spoke to her submission and highlighted the following points:
·
Provide a fence the dog area for off lead at
the west end of Ebbett Park.
·
If in the park dogs should be on leads.
·
Do not want fencing around the children play
areas.
·
Provision of more park benches and
installation of toilets may bring undesirables into the park.
This concluded the verbal presentations
on submissions.
Councillor Nixon withdrew from the meeting at 10.40am
______________________
The meeting adjourned at 10.40am for morning tea
and reconvened at 11.05am
_______________________
2.
Hearing of
Submissions to Raureka Parks Reserve Management Plan (cont’d)
The Parks Asset Planner, Ms Leaf
displayed a powerpoint presentation (CFM-17-66-19-178) providing an overview of the consultation and
submissions to the Raureka Parks Reserve Management Plan.
As an administering body under the Reserves Act 1977, the Hastings
District Council was required to prepare Reserve Management Plans for the
reserves under its management.  The Plans identify issues, objectives
and policies for the use, development, management and protection of the
reserves within the District.
Thirty three written submissions, including a petition with 252
signatories, were received relating to the Raureka Parks (Ebbett Park, St
Leonards Park and Whenua Takoha Reserve).  The petition had been
formally received at the Council meeting on 31 January 2019.
Issues raised for Ebbett Park included:
•
Poor passive surveillance “don’t feel safe”
•
Lacks amenity (furniture, toilet, car parking)
•
Aging buildings without lease or licence
•
Aging trees and “hidey-holes”
•
Not vested under the Reserves Act 1977
Issues raised for St Leonards Park included:
•
Old playground with poor play value along
busy Southampton
•
Lack of public toilet
•
Lack of seating and picnic tables
•
Lack of youth facilities
•
Poor accessibility
•
Not vested under the Reserves Act 1977
Issues raised for Whenua Takoha Park
included:
•
Lack of picnic tables and seating
•
Space lacks youth oriented activity and
seating
•
Desire for BBQ & drinking fountain
•
Lacks shade
•
Formally Name Whenua Takoha Park
•
Not vested under the Reserves Act 1977
Ms Leaf advised that
submissions had been received on Sections 1.3 Dogs; 2.2 Buildings and
Structures; 2.5 Playgrounds; 2.6 Informal Sport & Recreation Facilities; 2.7
Toilets; 2.8 Signs; 2.9 Park Furniture; 2.10 Vehicle Parking; 3.0 Natural
Values and Biodiversity; 4.1 Cultural and Heritage Significance; 5.6 &
5.8 Title and Reserve Classification and Sale of Parkland; 5.7 Future Park
Acquisition and 5.11 Safety and Vandalism which had required further
analysis.
The meeting then addressed submissions requiring
minor amendments and the Officer Recommendations regarding the Remedies.
Westend Tennis Club (#1 )
The Council agreed with the
Officer’s recommendation that page 18 of
the Plan be amended by adding the following wording:
The Westend Tennis Club (WTC) building is located at the
far northwest end of Ebbett Park. This building and courts have been managed
by the WTC. Beginning in 2015 the Club renewed its efforts to increase
numbers as well as seek incorporation. Two new posts were concreted in to
make the courts playable for competition and coaching, a push-button lock was
installed on the gate, and roof and courts cleaned. The inability to get
sustained membership and sufficient volunteer resources led to disaffiliation
with Tennis New Zealand.
WTC initiated the Raureka community usage and agreed to a
set of protocols by which the complex would be used and maintained by the two
parties ensuring continued tennis access and focus on the grounds whilst
allowing other extensions for a return to netball/basketball. Tennis coaching
has been provided and the building used for rangatahi services.
With no current lease and a change in usage the site has
returned to Council ownership. WTC would like to continue a foothold with
tennis continuing to be a part of what is on offer at the west end.
Vania Sillick (# 9) and Wayne &
Judith Taylor (#29) submitted on the use of the term “landlocked”.
Landlocked – noting parking is interior park
and limited road frontage – not completely landlocked.
The Council agreed with the Officer’s recommendations that the following three sections of the Plan be amended to read:
Section
1.1   Park & Reserve Use
Ebbett Park is
largely enclosed by residential housing with limited road frontage.
Section 2.3   Fences
& Walls
The majority
of Ebbett Park is enclosed by residential housing with limited road frontage
Section 5.8   Sale
of Parkland
The northwest
corner of Ebbett Park is enclosed by residential housing with limited road
frontage which creates a number of safety and surveillance issues.
REMEDY 1: SECTION 1.3 DOGS
Ms Leaf advised
that creating a permanent and dedicated dog off-lead area in Ebbett Park
would require full public consultation via an amendment to the bylaws.
The Council
agreed with the Officer’s recommendation
that a new Policy 1.3.4 be added to Section 1.3 as follows:
Support an
amendment to the Council bylaw regarding the provision of dog exercise areas
within Ebbett Park; and consult the community on the provision of a permanent
fenced dog off-lead in this park.
REMEDY 2: SECTION 2.2 BUILDINGS &
STRUCTURES
Ms Leaf advised
that an assessment of the Clubrooms had found that it was in very poor
condition.  The tennis courts and building are now under Council
ownership and options available would be to lease them to an organisation who
had the ability to make the improvements; Council undertake the improvements
at ratepayer cost or remove the facilities.
Officers
recommend that Council work alongside community groups such as (but not
limited to) Raureka Community Trust, Girl Guiding NZ and the community regarding
the long term future use of both of these buildings.
The Council agreed that no change be
made to Section 2.2 of the Plan and it remain as below.
The tennis
club building is in poor condition and unless a group is found to undertake
the necessary building improvement work it will be removed as per Council
policy.  Similarly, the courts need a lot of maintenance and an
organisation to activate the space for wider community use.
REMEDY 3 – SECTION 2.5
PLAYGROUNDS
Ms Leaf advised in regard to the
submission of Wendy King (#6) for play items for children under 6 years of age that
Council’s Play Strategy defined local level playground and
provided guidelines for the development.  A range of
ages could be supported within a playground.
The Council agreed with the Officer’s
recommendation:
That sufficient funds are included in
the Long Term Plan to
provide adequate new and renewed playgrounds in all of these parks,
consistent with their size, location, and classification.  No change is
therefore recommended to be made to the Plan.
REMEDY 4 – SECTION 2.6 –
INFORMAL SPORT AND RECREATION FACILITIES
The Council agreed with the Officer’s recommendation
that no changes be made to Section 2.6 of the Draft Plan, but recommend
that a surface treatment  be included in the Whenua Takoha basketball
half-court to minimise noise.
REMEDY 5 – SECTION 2.7 TOILETS
Ms Leaf advised that one unisex toilet
would require an additional $140,000 being included in the budget, with
additional funding required for ongoing cleaning and maintenance.
The Council agreed with the Officer’s recommendation
that no change be made to Section 2.7 of the Plan in that toilets be
provided in Ebbett and St Leonards Park, but not Whenua Takoha Park.
REMEDY 6 – SECTION 2.8 SIGNS
Ms Leaf advised that Interpretative
signage at Ebbett and St Leonards Park would be similar to the Frimley and
Havelock Village Green signs and would capture Ebbett bequest as well as
Makirikiri Stream.   She also recommended a solid plaque at the
entrance of Ebbett Park being installed.
The Council agreed with the Officer’s recommendation
that Section 4.0 of the Action Plan be amended to update Item 7
(Ebbett Park), and that an additional $2,000 be added to the budget for
Historical and Interpretative Signage and Commemorative Plaque.
Remedy 7 – Section 2.9 Park
Furniture
The Council agreed with the Officer’s
recommendation that Section 2.9.2 of the Plan be
amended to include the word barbeque to the list of furniture and that a
barbecue and shelter be added to the Action Plan and $30,000 be added to the Budget
for St. Leonards Park.
Remedy 8 – Section 2.10 Vehicle
Parking
The
Council agreed with the Officer’s recommendations
that the intention to provide a carpark in Ebbett Park be retained and that
no change is made to Section 2.10.5 of the Plan and that the submission regarding
permeable construction of a carpark at St. Leonards Park be forwarded to the
Woodturner’s Guild.
Section 9 – Section 4.0 Natural
Values and Biodiversity
Ms Leaf confirmed that the Oleander trees
should be removed as per submissions.
The Council agreed with the Officer’s
recommendations that an additional $5,000 be included
for a succession planting plan that is appropriate for the Raureka Parks and
adds to the long-term biodiversity of the Park and that $5,000 also be included in the plan per annum to carry out the
succession planting and budget corrected to reflect the per annum.
Section 10 – Section 4.0 Social
and Cultural Values
The Council agreed with the Officer’s recommendations
that $10,000 be included in the Plan to instigate a
community-led design process for Ebbett Park and that $20,000 be set aside
towards the creation of the final product(s) and fundraising to supplement
this.
Section 11 – Sections 5.6 Title
and Reserve Classification and 5.8. Sale of Parkland
Ms Leaf acknowledged the strong community
commitment to Ebbett Park and its  protection and ongoing
management.  Sections 5.6 and 5.8 be amended with revised wording.
The Council agreed with the Officer’s
recommendations that the submissions be allowed, and
that the whole park be vested under the Reserves Act 1977.  This would
ensure the intent of the gift of land was upheld and parkland was retained
for future generations to enjoy and the following sections amended.
5.6    Title and Reserve Classification
Council will vest all of Ebbett Park
under the Reserves Act 1977 to recognise the intent of the Deed of Trust 1927
and generous gift of land from Eliza Maud Ebbett and George Ebbett to the Borough
of Hastings.
5.6.2 Declare the Raureka Parks: Whenua Takoha Park, St. Leonards
Park, and Ebbett Park as Reserves under the Reserves Act 1977.
5.8    Sale of Park Land
5.8.4 Meet the provisions of the relevant
legislation and District Wide Reserve Management Plan prior to the revocation
or sale of any part of the Raureka Parks (non-vested parks require that
Council have 75% majority support)
(NB: This is the standard Policy in the Draft
Reserves Management Plan.)
Remedy 12 – Section 5.7
Furniture Reserve Acquisition
The Council agreed to support
the provision to acquire properties in the
Reserve Management Plan, but not make financial allowance of $500,000 as
recommended by Officers.
It was
considered that if the opportunity arose in the future to acquire properties
it would be addressed by the Council of the day.
Remedy 13 – Section 5.11 Safety
and Vandalism
The Council agreed that no
changes be made to the Plan.
Ms Leaf advised that the concept plans
for the Raureka Parks (Ebbett Park, St Leonards Park and Whenua Takoha
Reserve) included an amount of $1,387,725. The
proposed spend per park/reserve was:
Ebbett Park                                       $488,725
St. Leonards Park                            $822,000
Whenua Takoha Reserve                $77,000
As a result of submissions and
unprogrammed capital expenditure there would be a shortfall and a submission
would be made to the Annual Plan, plus fundraising by the community.  A
copy of the budget implications was tabled at the meeting (CG-14-42-00041).
It was noted that maintenance for new
items had not been included in the budget.  In regard to the barbecue
usage, Ms Leaf advised that the communities where barbecues were located at
parks were very well patronised.
Councillor Nixon rejoined the meeting
at 11.58pm
Ms Raheke from the Raureka Community
Trust had confirmed that the Whenua Takoha Park was used a lot and a
lot of events were held in the park included three “Movie in the Park”
events.  It was one of the main meeting places for the community.
Whenua Takoha Park Action Plan included:
·
BBQ and Shelter
·
New and additional park furniture (picnic tables, seats, and
bins)
·
Water fountain
·
Basketball half-court
·
Trees
Councillor O’Keefe withdrew from
the meeting at 11.50am
St Leonard’s Park
Ms Leaf advised that the playground at St
Leonards Park was scheduled to be upgraded to a “key urban”
playground.  The design and development of new
playgrounds also took into consideration equipment at nearby schools to
minimise duplication.  Due to location and
benefits to community a significant upgrade was included.
Although concern had
been expressed by submitters regarding families, who may be homeless, it was
not proposed to include shower facilities.  Officers would investigate
the possibility of  the showers in the sports facilities being used,
when there were no sports on.
St Leonards Action Plan
·
Upgrade playground with saleyard theme and fencing
·
Internal carpark
·
Park furniture (picnic tables, seats, and bins)
·
Interpretive Signage
·
Toilet and drinking fountain
·
Pathway network form Southampton and Townsend to Francis Hicks
·
Light pathway from Southampton to Francis Hicks
·
Landscape planting
·
Perimeter Fitness Trail plus equipment
Councillor O’Keefe rejoined the
meeting at 12.05pm
Ebbett Park
It was noted that there was an
expectation from some of the community that Council would take over the
improvements of the Westend Tennis Building.  The Westend Tennis Club
had not had discussions regarding co-sharing with other Tennis Clubs.
Mayor Hazlehurst and Councillor Dixon
withdrew from the meeting at 12.10pm.
Deputy Mayor Kerr assumed the role of
Chair
There was $155,000 in the budget for
approximately 20 car parks which would be sufficient funding.
Main thoroughfare link widen it and put
signs up “share with care” and operate at walking speed.
Children’s playgrounds not fenced.
Councillors Barber and Nixon left the
meeting at 12.20pm
_________________________
The meeting adjourned for lunch at 12.20pm
and reconvened at 1.05pm
_________________________
Deputy Mayor Kerr reconvened the
meeting at 1.05pm.
If there was a major change to the
building use it could trigger the need for community consultation.  A
change in ownership of the building from Girl Guiding or other would also
create a need for more inspections and bring the building up to the new
standard.
Councillor Nixon rejoined the meeting
at 1.10pm.
Ebbett Park Action Plan
Upgrade lighting on
central pathway Gordon Road to Oliphant
Park furniture
(picnic tables, seats, and bins)
Renew playground into
more central location and fence
Increase dog off-lead
hours and dog bag dispenser
Internal carpark
Toilet and drinking
fountain
Historical and
Interpretive signage
Enhanced landscape
planting
Develop perimeter
footpath & improve existing pathway
Develop petanque
court
There was further discussion on the
acquisition of properties included within the Reserve Management Plan which
were considered nice to have but unbudgeted.
Councillor Dixon rejoined the meeting
at 1.25pm and Mayor Hazlehurst reassumed the Chair.
The meeting proceeded to address the
recommendations.
·
It was noted that all Bylaws would be reviewed
in 2020 and determination would be whether there would be a fenced enclosure
for dogs.
·
The amount of $2,600 per barbeque per annum
was required to for daily cleaning and inspection in the summer and few times
in winter.
·
Vehicle parking – lighting will be
updated to LEDs (Light-emitting diode) and will be brighter.  Consider
additional lighting.
Councillor O’Keefe left the
meeting at 2.20pm.
Councillor Kerr/Councillor Redstone
A)     That the report
of the Parks Asset Planner titled “Hearing of Submissions
to Raureka Parks Reserve Management Plan” dated 13/02/2019
be received.
B)     That the allocation of funds required to complete the unprogrammed
capital works programme identified in the Action Plan form part of the
2019/20 Annual Plan priority considerations; and the Long Term Plan.
REMEDY 0: MINOR AMENDMENT  STATUS OF WESTEND TENNIS
C)     That
the submission of Westend Tennis Club (Submission #1) be allowed insofar as
the wording on page 18 of the Plan is amended as below.
The Westend Tennis Club (WTC) building is located at the
far northwest end of Ebbett Park. This building and courts have been managed
by the WTC. Beginning in 2015 the Club renewed its efforts to increase
numbers as well as seek incorporation. Two new posts were concreted in to
make the courts playable for competition and coaching, a push-button lock was
installed on the gate, and roof and courts cleaned. The inability to get
sustained membership and sufficient volunteer resources led to disaffiliation
with Tennis New Zealand.
WTC initiated the Raureka community usage and agreed to a
set of protocols by which the complex would be used and maintained by the two
parties ensuring continued tennis access and focus on the grounds whilst
allowing other extensions for a return to netball/basketball. Tennis coaching
has been provided and the building used for rangatahi services.
With no current lease and a change in usage the site has
returned to Council ownership. WTC would like to continue a foothold with
tennis continuing to be a part of what is on offer at the west end.
REMEDY 0: MINOR AMENDMENT  USE OF WORD
‘LANDLOCKED’
D)     That
the submission of Vania Sillick (Submission #9) and Wayne & Judith Taylor
(Submission #29) be allowed insofar as the following sections be amended by
deleting the words shown as struck out and added words shown in red
italics:
Section 1.1      Park
& Reserve Use
Ebbett Park
is largely land-locked enclosed
by residential housing with limited road frontage.
Section 2.3      Fences
& Walls
The majority
of Ebbett Park is landlocked enclosed by
residential housing with limited road frontage
Section 5.8      Sale
of Parkland
The
northwest corner of Ebbett Park is landlocked enclosed by residential housing
with limited road frontage which creates a number of safety
and surveillance issues.
REMEDY 1: SECTION 1.3 Dogs
E)     That
the submission of Phillipa Cook (Submission 3) be allowed insofar as the
following new policy is included in the Plan:
1.3.4      Support of an
amendment to the Council bylaw regarding the provision of dog exercise areas
within Ebbett Park; and consult the community on the provision of a permanent
fenced dog off-lead in this park.
F)      That
the submission of Rosalind Moore (Submission 2), John Roberts (Submission
14), Andrea Rooderkerk (Submission #21), the Ellar Trust (Submission 26), and
Raureka Community Trust (Submission 32) be allowed insofar that Section 1.3
is retained.
G)     That
the submission of Raureka Community Trust (Submission 32) requesting a dog
doo bin at Whenua Takoha Reserve be disallowed with the reason being that a
rubbish bin is already provided.
REMEDY 2: SECTION 2.2 BUILDINGS & STRUCTURES
H)     That
the submission of Westend Tennis Club (Submission 1) requesting the fences
and gates at the tennis courts be replaced be disallowed.
I)       That
the submission from Vania Sillick (Submission 9) be disallowed and the
submissions of John Roberts (Submission 14) and the Raureka Community Trust
(Submission 32) be allowed insofar as the policies in Section 2.2 are
retained in the Plan.
REMEDY 3: SECTION 2.5 PLAYGROUNDS
J)      That
the submissions of Wendy King (Submission 6) and Hamish Dufty (Submission 10)
be allowed insofar as that a local level playground as proposed in the plan
is an upgrade of what currently exists.
K)     That
the submission of Raureka Community Trust (Submission 3) be disallowed insofar
that the Ebbett Park playground upgrade is not  planned on being a key
urban or destination level playground.
REMEDY 4: SECTION 2.6 INFORMAL SPORT AND RECREATION FACILITIES
L)      That
the submission of Darbara Singh et.al. (Submission 8) be allowed insofar that
fitness
equipment in Policy 2.6 is retained in the Plan.
M)     That
the submission of Rosalind Moore (Submission 2) be allowed insofar that the
petanque court remains in the Ebbett Park Action Plan.
N)     That
the submission of Andrea Rooderkerk (Submission 21) be allowed insofar as
Policy 2.6.8 be retained.
O)     That
the submission of Raureka Community Trust (Submission 32) requesting a
surface treatment that minimises ball bounce noise on the planned Whenua
Takoha half-court be included in the design be allowed.
REMEDY 5: Section 2.7 Toilets
P)     That
the submission of Denise Bromby (Submission 13) requesting a toilet at Whenua
Takoha Reserve be disallowed.
REMEDY 6: Section 2.8 Signs
Q)     That
the submission of David Renouf (Submission 4) and the Ebbett Family
(Submission 33) be allowed insofar as Policy 2.8.4 is retained and that
Section 4.0 Action Plan be amended as follows:
7 Historical
and Interpretive Signage and Commemorative Plaque
REMEDY 7: SECTION 2.9 PARK FURNITURE
R)     That
the submission form Wendy King (Submission 6) and Linda Johnson (Submission
15) be allowed insofar that a barbeque is added to the St. Leonards’
Concept and Action Plans.
S)     That
the submission of Darbara Singh et.al. (Submission 8) be allowed insofar as
the sheltered seating be included in the Concept Plan.
T)     That
the submissions from Kath Purchas (Submission 20, Neil Thorsen (Submission
#24), and Raureka Community Trust (Submission 32) be allowed insofar as the
Policy 2.9.2 is retained in the Plan and that drinking fountains with dog
drinking attachment be included in the Concept Plans.
REMEDY 8: SECTION 2.10 VEHICLE PARKING
U)     That
the submission of Rosalind Moor (Submission 2), Girl Guiding NZ (Submission
18), and Raureka Community Trust (Submission 32) be allowed insofar as Policy
2.10.5 is retained in the Plan.
V)     That
the submission of the Ebbett Family (Submission 33) be allowed insofar as
lockable gates to Ebbett Park carpark be included in the Concept Plan.
W)    That
the submission of David Renouf (Submission 4) and Vania Sillick (Submission
9) be disallowed.
X)     That
the submission of Linda Johnson (Submission 15) be allowed insofar that her
submission regarding permeable construction of a carpark be given to the
Woodturner’s Guild.
REMEDY 9: SECTION 3.0 NATURAL VALUES
Y)     That
the submissions of Wendy King (Submission 6), Rosalind Moore (Submission 2),
Kath Purchas (Submission 30), Neil Thorsen (Submission 24), Andrea Rooderkerk
(Submission 21), Raureka Community Trust (Submission 32), the Ellar Trust
(Submission 26), and the Ebbett Family (Submission 33) be allowed insofar as
a staged succession planting plan be developed to ensure that the Raureka
Parks contribute to Hastings biodiversity while maintaining the character of
each park.
REMEDY 10: SECTION 4.0 SOCIAL AND CULTURAL VALUES
Z)      That
the submission of Raureka Community Trust (Submission 32) and the Ebbett
Family (Submission 33) be allowed in so far as gates at the entrance on
Gordon Road and design elements within the park should be progressed via a
community-led design process.
REMEDY 11: SECTION 5.6 TITLE AND RESERVE CLASSIFICATION AND 5.8
SALE OF PARKLAND
AA)   That the
submission of Kevin Naylor (Submission 17) be disallowed insofar that Council
is not confiscating land.
BB)  That the
submission of Peter Culloty (Submission 27) be disallowed insofar that there
are changes to Policies 5.6 and 5.8 as identified below
CC)  That the
submission by petition of 252 signatories (Submission 23) and the submissions
of David Renouf (Submission 4), Bernard & Bernice Koenders (Submission
23), Alistair & Jean Chalmers (Submission
28), Wayne & Judith Taylor (Submission 29), and Jenny Ross (Submission
30), Janet Wilson (Submission 5), Vania Sillick (Submission 8), Zoe Libby
(Submission 11), Gill Libby (Submission 12), Mahmood Nasir (Submission 19)
and Andrea Rooderkerk (Submission 21), Ellar Trust (Submission 26), Joanna Lynley-Richardson (Submission 31), The Raureka
Community Trust (Submission 32), and the Ebbett Family (Submission 33)
be allowed insofar that Section 5.6 and 5.8 be amended by deleting the words
shown as struck out and added as shown in red italics:
5.6         Title and
Reserve Classification
Council
will vest the southern and main portion of Ebbett Park leaving the west
end as fee simple.  This allows for flexibility in the future use of
this park all of Ebbett Park under the
Reserves Act 1977 to recognise the intent of the Deed of Trust 1927 and
generous gift of land from Eliza Maud Ebbett and George Ebbett to the Borough
of Hastings.
5.6.2      Declare the Raureka Parks:
Whenua Takoha Park, St. Leonards Park, and Ebbett Park (with the exception
of the northwest end of Ebbett Park) as Reserves under the Reserves Act
1977.
5.8         Sale of Park
Land
The northwest corner of Ebbett Park is landlocked which creates a
number of safety and surveillance issues.  If more beneficial community
uses are considered appropriate in the future, Council will follow the
relevant consultative process.  If thorough consideration determines the
sale of this land appropriate, any funds from the sale of land should be
redirected into increasing and opening up the more popular southern end of
the park.
5.8.2   To consider the disposal of a part of the
landlocked west end of Ebbett Park in the future.
5.8.4   Meet the provisions of the relevant legislation
and District Wide Reserve Management Plan prior to the revocation or sale of
any part of the Raureka Parks (non-vested parks require that Council have 75%
majority support)
5.8.5   Require any funds from a future sale to be redirected
into increasing and opening up the south end of Ebbett Park.
REMEDY 12: SECTION 5.7 FUTURE RESERVE ACQUISITION
DD)  That
submission Andrea Rooderkerk (Submission 21) be allowed insofar that Council
will consider opportunities for reserve acquisition when they arise.
REMEDY 13: SECTION 5.11 SAFETY & VANDALISM
EE)   That the
submission of David Renouf (Submission 4) be disallowed and the submission of
Raureka Community Trust (Submission 32) be allowed in that Policy 1.5.4 is
retained.
·
With the reasons for these
decisions being that the objective of the decision will contribute to meeting
the current and future needs of communities for good quality local
infrastructure in a way that is most cost-effective
for households and business by:
·
The provision of good quality
recreation open spaces
FF)   That
Council adopt the Raureka Parks Reserve Management Plan, as amended above by
submissions.
CARRIED
_______________________
The meeting closed at 2.50pm
Confirmed:
Chairman:
Date:
LIST
OF SUBMITTERS TO EXTRAORDINARY COUNCIL MEETING
RAUREKA
PARKS RESERVE MANAGEMENT PLAN
13
FEBRUARY 2019
Sub No.
Submitter
Trim Ref
1
Westend Tennis (Ian Purdon)
CFM-17-66-19-143
2
Rosalind Moore
CFM-17-66-19-145
3
Phillipa Cook
CFM-17-66-19-146
4
David Renouf
CFM-17-66-19-147
5
Janet Wilson
CFM-17-66-19-148
6
Wendy King
CFM-17-66-19-149
7
Tania Mitchell
CFM-17-66-19-150
8
Darbara Dingh
CFM-17-66-19-151
9
Vania Sillick
CFM-17-66-19-152
10
Hamish Dufty
CFM-17-66-19-153
11
Zoe Libby
CFM-17-66-19-154
12
Gill Libby
CFM-17-66-19-155
13
Denise Bromby
CFM-17-66-19-156
14
John Roberts
CFM-17-66-19-157
15
Linda Johnson
CFM-17-66-19-158
16
Richard Karn
CFM-17-66-19-159
17
Kevin M Naylor
CFM-17-66-19-160
18
Carol McMillan - Girl Guiding NZ
CFM-17-66-19-161
19
Mahmood Nasir
CFM-17-66-19-162
20
Kath Purchas
CFM-17-66-19-164
21
Andrea Rooderkerk
CFM-17-66-19-165
22
Powerco
CFM-17-66-19-143
23
Petitioner Bernice Koenders notes and Deed of Trust
presented at Council 31 January 2019
CG-14-1-01153
24
Neil Thorsen
CFM-17-66-19-166
25
Bernice Koenders
CFM-17-66-19-167
26
Ellar Trust (Linley-Richardson Family) Murray
Richardson
CFM-17-66-19-174
27
Peter Culloty
CFM-17-66-19-173
28
Alistair John and Jean Pringle Chalmers
CFM-17-66-19-172
29
Wayne Robert and Judith Yvonne Taylor
CFM-17-66-19-171
30
Jenny Ross
CFM-17-66-19-170
31
Joanna Linley-Richardson
CFM-17-66-19-169
32
Raureka Community Trust
CFM-17-66-19-168
33
Ebbett Family (Ron Ebbett)
CFM-17-66-19-175